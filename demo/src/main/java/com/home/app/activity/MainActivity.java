package com.home.app.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.pms.sdk.common.security.TlsSocketFactory;
import com.pms.sdk.common.util.CLog;
import com.pmsdemo.R;
import com.pms.sdk.PMS;
import com.pms.sdk.PMSPopup;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.api.request.*;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.Prefs;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import androidx.core.app.ActivityCompat;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.TlsVersion;

public class MainActivity extends Activity {

    private transient PMS pms = null;
    private PMSPopup pmsPopup = null;

    private Context mContext = null;

    private EditText mEdtCustId = null;

    private Button mBtnDeviceCert = null;
    private Button mBtnLoginPms = null;
    private Button mBtnNewMsg = null;
    private Button mBtnReadMsg = null;
    private Button mBtnClickMsg = null;
    private Button mBtnSetConfig = null;
    private Button mBtnLogoutPms = null;

    private TextView mTxtResult = null;

    private Boolean mConfigState = false;
    private transient OnClickListener onClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            int id = v.getId();
            mTxtResult.setText("loading...");

            switch (id) {
                case R.id.btn_device_cert:
                    pms.setCustId(mEdtCustId.getText().toString());
                    /**
                     * deviceCert (앱이 실행 되는 시점에서 호출 해주시면 됩니다.) 첫번째 파라미터는 CRM데이터 연동을 위한 파라미터로 null로 념겨주시면 됩니다.
                     */
                    new DeviceCert(mContext).request(null, new APICallback() {
                        @Override
                        public void response(String arg0, final JSONObject arg1) {
                            CLog.d("deviceCert is ended");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run()
                                {
                                    mTxtResult.setText(arg1.toString());
                                }
                            });
                        }
                    });
                    break;

                case R.id.btn_login_pms:
                    /**
                     * 로그인(cust_id저장)을 수행합니다. 사용자 정보를 원치 않으시면 null 등록
                     */

                    JSONObject userData = new JSONObject();
                    try {
                        userData.put("custName", "1111");
                        userData.put("phoneNumber", "2222");
                        userData.put("birthday", "3333");
                        userData.put("location1", "4444");
                        userData.put("location2", "5555");
                        userData.put("gender", "M");
                        userData.put("data1", "7777");
                        userData.put("data2", "8888");
                        userData.put("data3", "9999");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    new LoginPms(mContext).request(mEdtCustId.getText().toString(), userData, new APICallback() {
                        @Override
                        public void response(String arg0, final JSONObject arg1) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run()
                                {
                                    mTxtResult.setText(arg1.toString());
                                }
                            });
                        }
                    });
                    break;

                case R.id.btn_new_msg:
                    // 현재 가지고 있는 메시지의 max user msg id를 가져올 수 있습니다.
                    String req = new Prefs(mContext).getString(PMS.PREF_MAX_USER_MSG_ID);

                    /**
                     * 서버에서 메시지를 가져 와서 SQLite에 저장하는 request입니다. callback에서 pms.selectMsgList(1, 9999)과 같이 cursor를 가져와 쓰시거나, newMsg완료시점에 broadcasting을
                     * 하기 때문에, PMS.RECEIVER_REQUERY로 receiver를 받으셔서 메시지를 가져오셔도 됩니다. 파라미터 관련해서는 api문서를 참조하시면 되겠니다.
                     */
                    new NewMsg(mContext).request(PMS.TYPE_NEXT, "-1", "-1", "1", "30", new APICallback() {

                        @Override
                        public void response(String arg0, final JSONObject arg1) {
                            // 메시지를 cursor형태로 가져올수 있습니다.
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run()
                                {
                                    Cursor c = pms.selectMsgList(1, 9999);
                                    mTxtResult.setText("msgListSize:" + c.getCount() + "\n\n" + arg1.toString());
                                }
                            });
                        }
                    });
                    break;

                case R.id.btn_read_msg:
                    Cursor c = pms.selectMsgList(1, 10);
                    c.moveToLast();

                    if (c.getCount() > 0) {
                        Msg msg = new Msg(c);

                        JSONArray jsar = new JSONArray();
                        jsar.put(PMSUtil.getReadParam(msg.msgId));

                        /**
                         * 메세지 읽을 처리 Class 입니다.
                         */
                        new ReadMsg(mContext).request(jsar, new APICallback() {
                            @Override
                            public void response(String arg0, final JSONObject arg1) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run()
                                    {
                                        mTxtResult.setText(arg1.toString());
                                    }
                                });
                            }
                        });
                    } else {
                        mTxtResult.setText("매세지가 없습니다.");
                    }

                    break;

                case R.id.btn_click_msg:
                    /**
                     * 메시지 클릭정보를 저장하는 Class 입니다.
                     */
                    new ClickMsg(mContext).request(null, new APICallback() {
                        @Override
                        public void response(String arg0, final JSONObject arg1) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run()
                                {
                                    mTxtResult.setText(arg1.toString());
                                }
                            });
                        }
                    });
                    break;

                case R.id.btn_set_config:
                    /**
                     * msg & noti flag 값을 저장하는 Class 입니다.
                     */
                    if (mConfigState) {
                        mConfigState = false;
                        new SetConfig(mContext).request("Y", "Y", new APICallback() {
                            @Override
                            public void response(String arg0, final JSONObject arg1) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run()
                                    {
                                        mTxtResult.setText(mConfigState.toString()+" "+arg1.toString());
                                    }
                                });

                            }
                        });
                    } else {
                        mConfigState = true;
                        new SetConfig(mContext).request("N", "N", new APICallback() {
                            @Override
                            public void response(String arg0, final JSONObject arg1) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run()
                                    {
                                        mTxtResult.setText(mConfigState.toString()+" "+arg1.toString());
                                    }
                                });


                            }
                        });
                    }
                    break;

                case R.id.btn_logout_pms:
                    /**
                     * Logout 을 실행하는 Class 입니다.
                     */
                    new LogoutPms(mContext).request(new APICallback() {
                        @Override
                        public void response(String arg0, final JSONObject arg1) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run()
                                {
                                    mTxtResult.setText(arg1.toString());
                                }
                            });

                        }
                    });
                    break;

                case R.id.btn_private_start:
                    pms.startMQTTService(mContext);
                    break;

                case R.id.btn_private_end:
                    pms.stopMQTTService(mContext);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;
        setPMSSetting();
        setPMSPopupSetting();
//        ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.READ_PHONE_STATE,
//                Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);

        mEdtCustId = (EditText) findViewById(R.id.edt_cust_id);

        mBtnDeviceCert = (Button) findViewById(R.id.btn_device_cert);
        mBtnLoginPms = (Button) findViewById(R.id.btn_login_pms);
        mBtnNewMsg = (Button) findViewById(R.id.btn_new_msg);
        mBtnReadMsg = (Button) findViewById(R.id.btn_read_msg);
        mBtnClickMsg = (Button) findViewById(R.id.btn_click_msg);
        mBtnSetConfig = (Button) findViewById(R.id.btn_set_config);
        mBtnLogoutPms = (Button) findViewById(R.id.btn_logout_pms);

        mTxtResult = (TextView) findViewById(R.id.txt_result);

        mBtnDeviceCert.setOnClickListener(onClickListener);
        mBtnLoginPms.setOnClickListener(onClickListener);
        mBtnNewMsg.setOnClickListener(onClickListener);
        mBtnReadMsg.setOnClickListener(onClickListener);
        mBtnClickMsg.setOnClickListener(onClickListener);
        mBtnSetConfig.setOnClickListener(onClickListener);
        mBtnLogoutPms.setOnClickListener(onClickListener);

        ((Button) findViewById(R.id.btn_private_start)).setOnClickListener(onClickListener);
        ((Button) findViewById(R.id.btn_private_end)).setOnClickListener(onClickListener);

        mEdtCustId.setText(pms.getCustId());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PMS.clear();
    }

    private void setPMSSetting() {
        // pms 기본 셋팅입니다.
//        pms = PMS.getInstance(mContext, "997255892867");
        pms = PMS.getInstance(mContext, "997255892867");
        pms.setPopupSetting(true, "HOME");
        pms.setNotiIcon(R.drawable.ic_launcher);
        pms.setNotiReceiver("com.pms.home.push.notification");
        pms.setPushSilentReceiverClass("com.home.app.push.PushSilentReceiver");
        pms.setDebugTAG("HOME");
        pms.setDebugMode(true);
        pms.setRingMode(true);
        pms.setVibeMode(true);
        pms.setScreenWakeup(true);
        pms.setNotiOrPopup(false);
        pms.setPopupNoti(true);
        pms.setIsPopupActivity(true);
        pms.setUseBigText(true);
        pms.startMQTTService(mContext); // Private 서버 사용시 주석 해제
        pms.setNotificationStackable(true);
        PMSUtil.setMqttWakeupTime(mContext, 60000);
        PMSUtil.setMQTTAlarmFlag(mContext, true);

        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
        {
            builder.sslSocketFactory(pms.getSSLSocketFactoryForLollipop(), pms.getX509TrustManagerForTrustAllOfCertifications());
        }
    }

    private void setPMSPopupSetting() {
        // pms popup 기본 셋팅입니다.
        pmsPopup = PMS.getPopUpInstance();
        pmsPopup.setXmlAndDefaultFlag(false);

        setImage();
//         setXML();

        pmsPopup.commit();
    }

    private void setXML() {
        // xml 파일로 팝업창을 생성시
        pmsPopup.setLayoutXMLTextResId("pms_text_popup");
        pmsPopup.setLayoutXMLRichResId("pms_rich_popup");
//         pmsPopup.setTextBottomBtnClickListener(btnEvent, btnEvent1);
//         pmsPopup.setRichBottomBtnClickListener(btnEvent);
    }

    private void setImage() {
        // 이미지 파일로 팝업창을 생성시
        // pmsPopup.setTextBottomBtnClickListener(btnEvent, btnEvent1);
        // pmsPopup.setRichBottomBtnClickListener(btnEvent);

        // 이미지 파일로 적용시 셋팅 예시
         pmsPopup.setPopUpBackColor(128, 128, 128, 200);
        pmsPopup.setPopupBackImgResource("pms_bg_popup");

        // Top Layout Setting
        pmsPopup.setTopLayoutFlag(true);
        pmsPopup.setTopBackColor(56, 98, 196, 255);
        pmsPopup.setTopTitleType("image");
         pmsPopup.setTopTitleTextColor(255, 255, 255);
         pmsPopup.setTopTitleTextSize(25);
         pmsPopup.setTopTitleName("NS SHOP");
        pmsPopup.setTopTitleImgResource("pms_img_logo");

        // Content Layout Setting
        // pmsPopup.setContentBackImgResource(mCon, this.getPackageName(), "myfav_titleback");
        pmsPopup.setContentBackColor(255, 255, 255, 255);
        pmsPopup.setContentTextColor(0, 0, 0);
        // pmsPopup.setContentTextSize(20);

        // Bottom Button Layout Setting
        pmsPopup.setBottomTextBtnCount(2);
        pmsPopup.setBottomRichBtnCount(1);
        pmsPopup.setBottomBackColor(224, 224, 224, 255);
        // pmsPopup.setBottomBackImgResource("myfav_titleback");

        pmsPopup.setBottomTextViewFlag(true);
        pmsPopup.setBottomTextBtnCount(2);
        pmsPopup.setBottomRichBtnCount(1);
        pmsPopup.setBottomBtnTextName("닫  기", "자세히 보기");
        pmsPopup.setBottomBtnTextColor(255, 255, 255);
        // pmsPopup.setBottomBtnTextSize(10);
        // pmsPopup.setBottomBtnImageResource("mypage_stamp_confirmbtn", "mypage_stamp_cancelbtn");
        pmsPopup.setBottomBtnImageResource("pms_btn_text_close_off", "pms_btn_text_detail_off");
        // pmsPopup.setTextBottomBtnClickListener(btnEvent, btnEvent1);
        // pmsPopup.setRichBottomBtnClickListener(btnEvent);
    }
}
