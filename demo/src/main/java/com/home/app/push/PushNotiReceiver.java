package com.home.app.push;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.home.app.activity.MainActivity;
import com.pms.sdk.common.util.CLog;

/**
 * Notibar Click 시 실행되는 class 
 *
 */
public class PushNotiReceiver extends BroadcastReceiver {
	
	@Override
	public void onReceive(Context context, Intent intent) {
		CLog.i("onReceive");
		Intent i = new Intent(context, MainActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		i.putExtras(intent.getExtras());
		
		context.startActivity(i);
	}
}