package com.pms.sdk.api;

import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.NoCache;
import com.android.volley.toolbox.StringRequest;
import com.pms.sdk.common.security.SelfSignedSocketFactory;
import com.pms.sdk.common.security.TlsSocketFactory;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class QueueManager {
    private final int THREAD_POOL_COUNT = 6;

    private static class QueueManagerHolder {
        private static final QueueManager INSTANCE = new QueueManager();
    }

    public static QueueManager getInstance() {
        return QueueManagerHolder.INSTANCE;
    }

    private RequestQueue requestQueue = null;

    private QueueManager() {
        if (requestQueue == null) {
            final TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException
                        {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException
                        {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };
            SSLSocketFactory sslSocketFactory = null;
            try
            {
                sslSocketFactory = new TlsSocketFactory(trustAllCerts, new java.security.SecureRandom());
            }
            catch (KeyManagementException e)
            {
                e.printStackTrace();
            }
            catch (NoSuchAlgorithmException e)
            {
                e.printStackTrace();
            }
            Network network = new BasicNetwork(new HurlStack(null, sslSocketFactory));
            this.requestQueue = new RequestQueue(new NoCache(), network, THREAD_POOL_COUNT);
            this.requestQueue.start();
        }

    }

    public void addRequestQueue(StringRequest stringRequest) {
        if (requestQueue != null) {
            this.requestQueue.add(stringRequest);
        }
    }

    public RequestQueue getRequestQueue () {
        return requestQueue;
    }

    @Deprecated
    public void stop() {
        if (requestQueue != null) {
            this.requestQueue.stop();
            requestQueue = null;
        }
    }
}
