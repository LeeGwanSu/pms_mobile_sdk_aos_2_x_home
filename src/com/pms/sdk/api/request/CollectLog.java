package com.pms.sdk.api.request;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.Cursor;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.bean.Logs;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.db.PMSDB;

public class CollectLog implements IPMSConsts
{
	private Context context;
	private APIManager apiManager;
	private PMSDB pmsdb;
	private Prefs prefs;

	public CollectLog(Context context) {
		this.context = context;
		this.apiManager = APIManager.getInstance(context);
		this.pmsdb = apiManager.getPmsDb();
		this.prefs = apiManager.getPrefs();
	}

	public JSONObject getParam (String date) {
		try {
			HashMap<String, Long> hMap = new HashMap<String, Long>();
			JSONObject mainJobj = new JSONObject();
			String name = "";
			long dateCount = DateUtil.diffOfDate(date, DateUtil.getNowDateMo());
			String tempDate = date;
			Boolean dateState = false;

			for (int i = 0; i < 2; i++) {
				JSONObject jobj = new JSONObject();
				JSONObject jobjSub = new JSONObject();
				JSONArray jarry = new JSONArray();
				StringBuilder builder = new StringBuilder(1024);
				StringBuilder subBuilder = new StringBuilder(1024);
				String afterTime = "000000";
				String afterSubTime = "00";
				String afterFlag = "-1";
				int count = 0;
				Cursor c = null;

				for (int j = 0; j < dateCount; j++) {
					if (i == 0) {
						c = pmsdb.selectLog(Logs.TYPE_A, tempDate);
						name = "apiLogs";
					} else if (i == 1) {
						c = pmsdb.selectLog(Logs.TYPE_P, tempDate);
						name = "privateLogs";
					}

					while (c.moveToNext()) {
						dateState = true;
						Logs logs = new Logs(c);
						if (Logs.TYPE_A.equals(logs.logFlag) && FLAG_Y.equals(prefs.getString(PREF_API_LOG_FLAG))) {
							jobjSub.put("time", logs.time);
							jobjSub.put("api", logs.api);
							jobjSub.put("param", logs.param);
							jobjSub.put("result", logs.result);
							jarry.put(jobjSub);
							jobjSub = new JSONObject();
						} else if (Logs.TYPE_P.equals(logs.logFlag) && FLAG_Y.equals(prefs.getString(PREF_PRIVATE_LOG_FLAG))) {
							String DBtime = logs.time;
							String subTime = DBtime.substring(0, 2);
							long diffTime = DateUtil.diffOfTime(afterTime, DBtime, tempDate);

							if (subTime.equals(afterSubTime)) {
								builder.append(logs.privateLog);
								if (afterFlag.equals("-1") == false) {
									setHashMapDate(hMap, diffTime, afterFlag);
								}
							} else {
								String value = builder.toString();
								if (value.length() >= 10) {
									value = value.substring((value.length() - 10), value.length());
								}

								setHashMapDate(hMap, diffTime, afterFlag);

								jobjSub.put(afterSubTime, value);
								builder.delete(0, builder.length());
								builder.append(logs.privateLog);
								subBuilder.append(getMaxTime(hMap) + "|");
								hMap.clear();
							}

							if ((c.getCount() - 1) == count) {
								String value = builder.toString();
								if (value.length() >= 10) {
									value = value.substring((value.length() - 10), value.length());
								}

								subBuilder.append(getMaxTime(hMap) + "|");
								jobjSub.put(afterSubTime, value);
								jobjSub.put("S", subBuilder.toString().substring(0, subBuilder.length() - 1));
							}

							afterTime = DBtime;
							afterSubTime = subTime;
							afterFlag = logs.privateLog;
							count++;
						}
					}

					if (i == 0) {
						jobj.put(tempDate, jarry);
						tempDate = DateUtil.getTomorrowDate(tempDate);
					} else if (i == 1) {
						jobj.put(tempDate, jobjSub);
						jobjSub = new JSONObject();
						tempDate = DateUtil.getTomorrowDate(tempDate);
					}
				}
				mainJobj.put(name, jobj);
				tempDate = date;
			}

			if (dateState == false) {
				mainJobj = null;
			}
			return mainJobj;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void request (String date, final APICallback apiCallback) {
		try {
			JSONObject paramJsOb = getParam(date);
			if (paramJsOb == null) {
				return;
			}

			apiManager.call(API_COLLECT_LOG, paramJsOb, new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean requiredResultProc (JSONObject json) {
		try {
			String date = prefs.getString(PREF_YESTERDAY);
			long dateCount = DateUtil.diffOfDate(date, DateUtil.getNowDateMo());
			for (int i = 0; i < dateCount; i++) {
				pmsdb.deleteLog(date);
				date = DateUtil.getTomorrowDate(date);
			}

			prefs.putString(PREF_YESTERDAY, DateUtil.getNowDateMo());
			prefs.putBoolean(PREF_ONEDAY_LOG, true);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@SuppressWarnings("rawtypes")
	private String getMaxTime (HashMap<String, Long> map) {
		long time = 0;
		String maxvaluekey = "";
		Set key = map.keySet();
		for (Iterator iterator = key.iterator(); iterator.hasNext();) {
			String keyName = (String) iterator.next();
			long valueName = map.get(keyName);

			if (time <= valueName) {
				time = valueName;
				maxvaluekey = keyName;
			}
		}
		return maxvaluekey;
	}

	private void setHashMapDate (HashMap<String, Long> map, long time, String value) {
		if (map.containsKey(value)) {
			long hashTime = Long.valueOf(map.get(value));
			time = (time >= hashTime) ? time : hashTime;
		}
		map.put(value, time);
	}
}