package com.pms.sdk.api.request;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager;
import com.pms.sdk.api.APIManager.APICallback;

public class ClickMsg implements IPMSConsts
{
	private Context context;
	private APIManager apiManager;

	public ClickMsg(Context context) {
		this.context = context;
		this.apiManager = APIManager.getInstance(context);
	}

	/**
	 * get param
	 * 
	 * @return
	 */
	public JSONObject getParam (JSONArray clicks) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("clicks", clicks);

			return jobj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 
	 * @param clicks
	 * @param apiCallback
	 */
	public void request (JSONArray clicks, final APICallback apiCallback) {
		try {
			apiManager.call(API_CLICK_MSG, getParam(clicks), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		return true;
	}
}
