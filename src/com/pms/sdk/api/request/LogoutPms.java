package com.pms.sdk.api.request;

import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.db.PMSDB;

public class LogoutPms implements IPMSConsts {
	private Context context;
	private APIManager apiManager;
	private Prefs prefs;
	private PMSDB pmsDb;

	public LogoutPms(Context context) {
		this.context = context;
		this.apiManager = APIManager.getInstance(context);
		this.prefs = apiManager.getPrefs();
		this.pmsDb = apiManager.getPmsDb();
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (final APICallback apiCallback) {
		try {
			// PMSUtil.setEncKey(mContext, "");
			PMSUtil.setCustId(context, "");
			prefs.putString(PREF_LOGINED_CUST_ID, "");
			prefs.putString(IPMSConsts.PREF_MAX_USER_MSG_ID, "-1");
			pmsDb.deleteAll();

			apiManager.call(API_LOGOUT_PMS, new JSONObject(), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		return true;
	}
}
