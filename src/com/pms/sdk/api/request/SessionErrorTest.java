package com.pms.sdk.api.request;

import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager;
import com.pms.sdk.api.APIManager.APICallback;

public class SessionErrorTest implements IPMSConsts
{
	private Context context;
	private APIManager apiManager;
	public SessionErrorTest(Context context) {
		this.context = context;
		this.apiManager = APIManager.getInstance(context);
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (final APICallback apiCallback) {
		try {
			apiManager.call("sessionErrorTest.m", new JSONObject(), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					requiredResultProc(json);
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		return true;
	}
}
