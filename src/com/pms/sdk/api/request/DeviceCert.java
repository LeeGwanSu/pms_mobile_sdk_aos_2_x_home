package com.pms.sdk.api.request;

import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.FCMRequestToken;
import com.pms.sdk.push.mqtt.MQTTService;
import com.pms.sdk.push.mqtt.RestartReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;

public class DeviceCert implements IPMSConsts{
    private Context context;
    private APIManager apiManager;
    private Prefs prefs;
    private PMSDB pmsDb;

    public DeviceCert(Context context)
    {
        this.context = context;
        this.apiManager = APIManager.getInstance(context);
        this.prefs = apiManager.getPrefs();
        this.pmsDb = apiManager.getPmsDb();
    }

    /**
     * get param
     *
     * @return
     */
    public JSONObject getParam(JSONObject userData) {
        JSONObject jobj;

        try {
            jobj = new JSONObject();

            // new version
            jobj.put("appKey", PMSUtil.getApplicationKey(context));
            jobj.put("uuid", PMSUtil.getUUID(context));
            jobj.put("pushToken", PMSUtil.getGCMToken(context));
            jobj.put("custId", PMSUtil.getCustId(context));
            jobj.put("appVer", PhoneState.getAppVersion(context));
            jobj.put("os", "A");
            jobj.put("osVer", PhoneState.getOsVersion());
            jobj.put("device", PhoneState.getDeviceName());
            jobj.put("sessCnt", "1");

            if (userData != null) {
                jobj.put("userData", userData);
            }

            return jobj;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * request
     *
     * @param apiCallback
     */
    public void request(final JSONObject userData, final APICallback apiCallback)
    {
        boolean isNExistsUUID = PhoneState.createDeviceToken(context);
        CLog.i("Device Cert request get UUID : " + (isNExistsUUID == true ? "Created" : "Exists"));

        String custId = PMSUtil.getCustId(context);

        if (!custId.equals(prefs.getString(PREF_LOGINED_CUST_ID))) {
            // 기존의 custId와 auth할려는 custId가 다르다면, DB초기화
            CLog.i("DeviceCert:new user");
            pmsDb.deleteAll();
        }

        new FCMRequestToken(context, PMSUtil.getGCMProjectId(context), new FCMRequestToken.Callback() {
            @Override
            public void callback(boolean isSuccess, String message)
            {
                CLog.i("FCMRequestToken isSuccess? "+isSuccess+" message : "+message);
                if (StringUtil.isEmpty(PMSUtil.getGCMToken(context)) || NO_TOKEN.equals(PMSUtil.getGCMToken(context))) {
                    prefs.putString(KEY_GCM_TOKEN, NO_TOKEN);
                }

                try {
                    apiManager.call(API_DEVICE_CERT, getParam(userData), new APICallback() {
                        @Override
                        public void response(String code, JSONObject json) {
                            if (CODE_SUCCESS.equals(code)) {
                                PMSUtil.setDeviceCertStatus(context, DEVICECERT_COMPLETE);
                                requiredResultProc(json);
                                if (apiCallback != null) {
                                    apiCallback.response(code, json);
                                }
                            }
                        }
                    });
                } catch (Exception e) {
                    CLog.e(e.getMessage());
                    if (apiCallback != null) {
                        apiCallback.response(CODE_UNKNOWN, null);
                    }
                }
            }
        }).execute();
    }

    /**
     * required result proccess
     *
     * @param json
     */
    private boolean requiredResultProc(JSONObject json) {
        try {
            PMSUtil.setAppUserId(context, json.getString("appUserId"));
            PMSUtil.setEncKey(context, json.getString("encKey"));

            // set msg flag
            prefs.putString(PREF_API_LOG_FLAG, json.getString("collectApiLogFlag"));
            prefs.putString(PREF_PRIVATE_LOG_FLAG, json.getString("collectPrivateLogFlag"));
            String custId = PMSUtil.getCustId(context);
            if (!StringUtil.isEmpty(custId)) {
                prefs.putString(PREF_LOGINED_CUST_ID, PMSUtil.getCustId(context));
            }

            // set badge
//            Badge.getInstance(mContext).updateBadge(json.getString("newMsgCnt"));

            // set config flag
//            if ((prefs.getString(PREF_MSG_FLAG).equals(json.getString("msgFlag")) == false)
//                    || (prefs.getString(PREF_NOTI_FLAG).equals(json.getString("notiFlag")) == false)) {
//                new SetConfig(mContext).request(prefs.getString(PREF_MSG_FLAG), prefs.getString(PREF_NOTI_FLAG), null);
//            }
            prefs.putString(IPMSConsts.PREF_MSG_FLAG, json.getString("msgFlag"));
            prefs.putString(IPMSConsts.PREF_NOTI_FLAG, json.getString("notiFlag"));

            // readMsg
            final JSONArray readArray = PMSUtil.arrayFromPrefs(context, PREF_READ_LIST);
            if (readArray.length() > 0) {
                // call readMsg
                new ReadMsg(context).request(readArray, new APICallback() {
                    @Override
                    public void response(String code, JSONObject json) {
                        if (CODE_SUCCESS.equals(code)) {
                            // delete readMsg
                            prefs.putString(PREF_READ_LIST, "");
                        }

                        if (CODE_PARSING_JSON_ERROR.equals(code)) {
                            for (int i = 0; i < readArray.length(); i++) {
                                try {
                                    if ((readArray.get(i) instanceof JSONObject) == false) {
                                        readArray.put(i, PMSUtil.getReadParam(readArray.getString(i)));
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            new ReadMsg(context).request(readArray, new APICallback() {
                                @Override
                                public void response(String code, JSONObject json) {
                                    if (CODE_SUCCESS.equals(code)) {
                                        prefs.putString(PREF_READ_LIST, "");
                                    }
                                }
                            });
                        }
                    }
                });
            } else {
                CLog.i("readArray is null");
            }

            // clickMsg
            JSONArray clickArray = PMSUtil.arrayFromPrefs(context, PREF_CLICK_LIST);
            if (clickArray.length() > 0) {
                // call clickMsg
                new ClickMsg(context).request(clickArray, new APICallback() {
                    @Override
                    public void response(String code, JSONObject json) {
                        if (CODE_SUCCESS.equals(code)) {
                            // delete clickMsg
                            prefs.putString(PREF_CLICK_LIST, "");
                        }
                    }
                });
            } else {
                CLog.i("clickArray is null");
            }

            // mqttFlag Y/N
            if (FLAG_Y.equals(PMSUtil.getMQTTFlag(context))) {
                String flag = json.getString("privateFlag");
                prefs.putString(PREF_PRIVATE_FLAG, flag);
                // test, mqtt service 동작 주기 확인 test!!!
                if (FLAG_Y.equals(flag)) {
//                if (true) {
                    String protocol = json.getString("privateProtocol");
                    String protocolTemp = "";
                    try {
                        URI uri = new URI(PMSUtil.getMQTTServerUrl(context));
                        if (protocol.equals(PROTOCOL_TCP)) {
                            protocolTemp = protocol.toLowerCase() + "cp";
                        } else if (protocol.equals(PROTOCOL_SSL)) {
                            protocolTemp = protocol.toLowerCase() + "sl";
                        }

                        if (uri.getScheme().equals(protocolTemp)) {
                            prefs.putString(PREF_PRIVATE_PROTOCOL, protocol);
                        } else {
                            prefs.putString(PREF_PRIVATE_PROTOCOL, protocol);
                            context.stopService(new Intent(context, MQTTService.class));
                        }
                    } catch (NullPointerException e) {
                        prefs.putString(PREF_PRIVATE_PROTOCOL, protocol);
                    }
                    int device = Build.VERSION.SDK_INT;
                    CLog.d("Device build version: " + device + ", Release version: " + Build.VERSION.RELEASE);
                    if (device < Build.VERSION_CODES.M && device < 23)
                    {
                        Intent intent = new Intent(context, RestartReceiver.class);
                        intent.setAction(ACTION_START);
                        context.sendBroadcast(intent);
                    }
                } else {
                    context.stopService(new Intent(context, MQTTService.class));
                }
            }

            // LogFlag Y/N
            if ((FLAG_N.equals(prefs.getString(PREF_API_LOG_FLAG)) && FLAG_N.equals(prefs.getString(PREF_PRIVATE_LOG_FLAG))) == false) {
                setCollectLog();
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void setCollectLog() {
        boolean isAfter = false;
        String beforeDate = prefs.getString(PREF_YESTERDAY);
        String today = DateUtil.getNowDateMo();

        try {
            isAfter = DateUtil.isDateAfter(beforeDate, today);
        } catch (Exception e) {
            isAfter = false;
        }

        if (isAfter) {
            prefs.putBoolean(PREF_ONEDAY_LOG, false);
        }

        if (prefs.getBoolean(PREF_ONEDAY_LOG) == false) {
            if (beforeDate.equals("")) {
                beforeDate = DateUtil.getNowDateMo();
                prefs.putString(PREF_YESTERDAY, beforeDate);
            }
            new CollectLog(context).request(beforeDate, null);
        }
    }
}
