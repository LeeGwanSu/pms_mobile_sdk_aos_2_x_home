package com.pms.sdk.api.request;

import android.content.Context;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.db.PMSDB;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class NewMsg implements IPMSConsts {

    private Context context;
    private APIManager apiManager;
    private Prefs prefs;
    private PMSDB pmsDb;
    private String pType;
    private String pReqUserMsgId;
    private String pMsgGrpCode;
    private String pPageNum;
    private String pPageSize;
    private APICallback pApiCallback;
    private boolean isComplete;

    public NewMsg(Context context) {
        this.context = context;
        this.apiManager = APIManager.getInstance(context);
        this.prefs = apiManager.getPrefs();
        this.pmsDb = apiManager.getPmsDb();
    }

    /**
     * get param
     *
     * @return
     */
    public JSONObject getParam(String type, String reqUserMsgId, String msgGrpCode, String pageNum, String pageSize) {
        JSONObject jobj;

        try {
            jobj = new JSONObject();

            jobj.put("type", type);
            jobj.put("reqUserMsgId", reqUserMsgId);
            jobj.put("msgGrpCd", msgGrpCode);

            JSONObject page = new JSONObject();
            page.put("page", pageNum);
            page.put("row", pageSize);

            jobj.put("pageInfo", page);

            return jobj;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * request
     *
     * @param apiCallback
     */
    public void request(String type, String reqUserMsgId, String msgGrpCode, String pageNum, String pageSize, final APICallback apiCallback) {

        isComplete = false;

        if (reqUserMsgId == null || "".equals(reqUserMsgId) || "-1".equals(reqUserMsgId) ) {
            reqUserMsgId = "-1";
            type = TYPE_PREV;
        }
        
        pType = type;
        pReqUserMsgId = reqUserMsgId;
        pMsgGrpCode = msgGrpCode;
        pPageNum = pageNum;
        pPageSize = pageSize;
        pApiCallback = apiCallback;

        try {
            apiManager.call(IPMSConsts.API_NEW_MSG, getParam(type, reqUserMsgId, msgGrpCode, pageNum, pageSize), new APICallback() {
                @Override
                public void response(String code, JSONObject json) {
                    if (CODE_SUCCESS.equals(code)) {
                        requiredResultProc(json);
                    } else {
                        isComplete = true;
                    }

                    /**
                     * 20.06.08 손보광
                     * CursorWindowAllocationException 발생해서 루프 다돌고 동작하도록 변경
                     */
                    try
                    {
                        pmsDb.deleteExpireMsg();
                        pmsDb.deleteEmptyMsgGrp();
                    }
                    catch (Exception e)
                    { }

                    if (apiCallback != null && isComplete) {
                        apiCallback.response(code, json);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * required result proccess
     *
     * @param json
     */
    private boolean requiredResultProc(final JSONObject json) {
        try {
            // recent msg map
            Map<String, Msg> newMsgList = new HashMap<String, Msg>();

            prefs.putString(IPMSConsts.PREF_MAX_USER_MSG_ID, json.getString("maxUserMsgId"));

            // get msg list (json array)
            JSONArray msgList = json.getJSONArray("msgs");

            int msgListSize = msgList.length();
            CLog.i("msgListSize=" + msgListSize);
            if (msgListSize < 1) {
                isComplete = true;
                return true;
            }

            for (int i = 0; i < msgListSize; i++) {
                Msg msg = new Msg(msgList.getJSONObject(i));
                if (Long.parseLong(msg.expireDate) / 1000000 < Long.parseLong(DateUtil.getNowDate()) / 1000000) {
                    continue;
                }

                Msg exMsg = pmsDb.selectMsgWhereUserMsgId(msg.userMsgId);
                if (exMsg != null) {
                    // update msg (already exist)
//					if (Msg.TYPE_H.equals(msg.msgType) || Msg.TYPE_L.equals(msg.msgType)) {
//						// rich
//						msg.msgGrpCd = "-1";
//					}
                    pmsDb.updateMsg(msg);
                } else {
                    // insert msg (no exist)
                    /**
                     * 2013.07.23 msgType이 H일 경우 msgCode를 음수로 저장
                     *//*
                    if (Msg.TYPE_H.equals(msg.msgType) || Msg.TYPE_L.equals(msg.msgType)) {
						int minMsgCode = mDB.selectMinMsgCode();
						minMsgCode = minMsgCode > 0 ? 0 : minMsgCode;
						msg.msgGrpCd = String.valueOf(minMsgCode - 1);
						// rich 메시지인경우 바로 msggrp을 만들어 줌
						mDB.insertMsgGrp(msg);
					}*/
                    pmsDb.insertMsg(msg);
                }

//				if ((!Msg.TYPE_H.equals(msg.msgType) || !Msg.TYPE_L.equals(msg.msgType))&&
                if ((!newMsgList.containsKey(msg.msgGrpCd) || (newMsgList.containsKey(msg.msgGrpCd)
                                && Long.parseLong(newMsgList.get(msg.msgGrpCd).regDate) <= Long.parseLong(msg.regDate) && Long.parseLong(newMsgList
                                .get(msg.msgGrpCd).msgId) < Long.parseLong(msg.msgId)))) {
                    // rich메시지가 아니면서,
                    // 같은 msgCode를 가진 msg가 map에 존재하지 않거나,
                    // 같은 msgCode를 가진 msg가 map에 존재하면서, regDate가 같거나 크고, msgId가 큰 경우
                    newMsgList.put(msg.msgGrpCd, msg);
                }
            }

            // set msg grp (recent)
            Iterator<String> it = newMsgList.keySet().iterator();
            while (it.hasNext()) {
                String recentMsgCode = it.next();

                // set recent msg
                Msg recentMsg = newMsgList.get(recentMsgCode);
                if (pmsDb.selectMsgGrp(recentMsgCode) != null) {
                    // update msg grp (already exist)
                    pmsDb.updateRecentMsgGrp(recentMsg);
                } else {
                    // insert msg grp (no exist)
                    pmsDb.insertMsgGrp(recentMsg);
                }
            }

            try {
                if (msgListSize >= Integer.parseInt(pPageSize)) {
                    request(pType, pReqUserMsgId, pMsgGrpCode, (Integer.parseInt(pPageNum) + 1) + "", pPageSize, pApiCallback);
                } else {
                    isComplete = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {

            // requery
//            mContext.sendBroadcast(new Intent(mContext, Badge.class).setAction(IPMSConsts.RECEIVER_REQUERY));
        }
    }
}
