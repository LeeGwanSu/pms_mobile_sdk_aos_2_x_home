package com.pms.sdk.api.request;

import org.json.JSONObject;

import android.content.Context;
import android.text.TextUtils;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.db.PMSDB;

public class SetConfig implements IPMSConsts
{
	private Context context;
	private APIManager apiManager;
	private Prefs prefs;
	private PMSDB pmsDb;


	public SetConfig(Context context) {
		this.context = context;
		this.apiManager = APIManager.getInstance(context);
		this.prefs = apiManager.getPrefs();
		this.pmsDb = apiManager.getPmsDb();
	}

	/**
	 * get param
	 * 
	 * @param msgFlag
	 * @param notiFlag
	 * @return
	 */
	// public JSONObject getParam (String msgFlag, String notiFlag, String orderFlag) {
	public JSONObject getParam (String msgFlag, String notiFlag) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("msgFlag", TextUtils.isEmpty(msgFlag) ? FLAG_Y : msgFlag);
			jobj.put("notiFlag", TextUtils.isEmpty(notiFlag) ? FLAG_Y : notiFlag);
			// jobj.put("orderFlag", orderFlag);
			return jobj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	// public void request (String msgFlag, String notiFlag, String orderFlag, final APICallback apiCallback) {
	public void request (String msgFlag, String notiFlag, final APICallback apiCallback) {
		try {
			// apiManager.call(API_SET_CONFIG, getParam(msgFlag, notiFlag, orderFlag), new APICallback() {
			apiManager.call(API_SET_CONFIG, getParam(msgFlag, notiFlag), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		try {
			prefs.putString(PREF_MSG_FLAG, json.getString("msgFlag"));
			prefs.putString(PREF_NOTI_FLAG, json.getString("notiFlag"));
			// mPrefs.putString(PREF_ORDER_FLAG, json.getString("orderFlag"));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
