package com.pms.sdk.api.request;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;

public class LoginPms implements IPMSConsts{
	private Context context;
	private APIManager apiManager;
	private Prefs prefs;
	private PMSDB pmsDb;

	public LoginPms(Context context) {
		this.context = context;
		this.apiManager = APIManager.getInstance(context);
		this.prefs = apiManager.getPrefs();
		this.pmsDb = apiManager.getPmsDb();
	}

	/**
	 * get param
	 * 
	 * @return
	 */
	public JSONObject getParam (String custId, JSONObject userData) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("custId", custId);

			if (userData != null) {
				jobj.put("userData", userData);
			}

			return jobj;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param custId
	 * @param userData
	 * @param apiCallback
	 */
	public void request (String custId, final JSONObject userData, final APICallback apiCallback) {
		try {
			prefs.putString(PREF_CUST_ID, custId);
			if (!custId.equals(prefs.getString(PREF_LOGINED_CUST_ID))) {
				// 기존의 custId와 auth할려는 custId가 다르다면, DB초기화
				CLog.i("LoginPms:new user");
				prefs.putString(IPMSConsts.PREF_MAX_USER_MSG_ID, "-1");
				pmsDb.deleteAll();
			}

			apiManager.call(API_LOGIN_PMS, getParam(custId, userData), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		String custId = PMSUtil.getCustId(context);
		if (!StringUtil.isEmpty(custId)) {
			prefs.putString(PREF_LOGINED_CUST_ID, PMSUtil.getCustId(context));
		}
		return true;
	}
}
