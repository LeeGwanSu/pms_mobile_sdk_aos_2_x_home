package com.pms.sdk.api.request;

import android.content.Context;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.db.PMSDB;

import org.json.JSONArray;
import org.json.JSONObject;

public class ReadMsg implements IPMSConsts {
	private Context context;
	private APIManager apiManager;
	private Prefs prefs;
	private PMSDB pmsDb;

	public ReadMsg(Context context) {
		this.context = context;
		this.apiManager = APIManager.getInstance(context);
		this.prefs = apiManager.getPrefs();
		this.pmsDb = apiManager.getPmsDb();
	}

	/**
	 * get param
	 * 
	 * @return
	 */
	public JSONObject getParam (String msgGrpCd, String firstUserMsgId, String lastUserMsgId) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("msgGrpCd", msgGrpCd);
			jobj.put("firstUserMsgId", firstUserMsgId);
			jobj.put("lastUserMsgId", lastUserMsgId);

			return jobj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * get Param
	 * 
	 * @param reads
	 * @return
	 */
	public JSONObject getParam (JSONArray reads) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("reads", reads);

			return jobj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * get Param
	 * 
	 * @param reads
	 * @return
	 */
	public JSONObject getParamUserId (JSONArray reads) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("userMsgIds", reads);

			return jobj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (final String msgGrpCd, final String firstUserMsgId, final String lastUserMsgId, final APICallback apiCallback) {
		try {
			apiManager.call(API_READ_MSG, getParam(msgGrpCd, firstUserMsgId, lastUserMsgId), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					PMSDB db = PMSDB.getInstance(context);
					db.updateReadMsg(msgGrpCd, firstUserMsgId, lastUserMsgId);
					db.updateNewMsgCnt();

					requiredResultProc(json);
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * request
	 * 
	 * @param reads
	 * @param apiCallback
	 */
	public void request (final JSONArray reads, final APICallback apiCallback) {
		try {
			apiManager.call(API_READ_MSG, getParam(reads), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						PMSDB db = PMSDB.getInstance(context);

						for (int i = 0; i < reads.length(); i++) {
							try {
								db.updateReadMsgWhereMsgId(reads.getJSONObject(i).getString("msgId"));
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						db.updateNewMsgCnt();

						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * request
	 * 
	 * @param reads
	 * @param apiCallback
	 */
	public void requestUserId (final JSONArray reads, final APICallback apiCallback) {
		try {
			apiManager.call(API_READ_MSG, getParamUserId(reads), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						PMSDB db = PMSDB.getInstance(context);

						for (int i = 0; i < reads.length(); i++) {
							try {
								db.updateReadMsgWhereUserMsgId(reads.getString(i));
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						db.updateNewMsgCnt();

						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		// requery
//		mContext.sendBroadcast(new Intent(mContext, Badge.class).setAction(IPMSConsts.RECEIVER_REQUERY));
		return true;
	}
}
