package com.pms.sdk.api.request;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.Prefs;

public class GetSignKey implements IPMSConsts
{
	private Context context;
	private APIManager apiManager;
	private Prefs prefs;

	public GetSignKey(Context context) {
		this.context = context;
		this.apiManager = APIManager.getInstance(context);
		this.prefs = apiManager.getPrefs();
	}

	/**
	 * get param
	 * 
	 * @param appKey
	 * @return
	 */
	public JSONObject getParam (String appKey) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("appKey", PMSUtil.getApplicationKey(context));
			return jobj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (String appKey, final APICallback apiCallback) {
		try {
			apiManager.call(API_GET_SIGNKEY, getParam(appKey), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		try {
			prefs.putString(PREF_SSL_SIGN_KEY, json.getString("signKey"));
			prefs.putString(PREF_SSL_SIGN_PASS, json.getString("signPw"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return true;
	}
}
