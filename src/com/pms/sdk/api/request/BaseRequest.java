package com.pms.sdk.api.request;

import android.content.Context;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.db.PMSDB;
@Deprecated
public class BaseRequest implements IPMSConsts {

	protected Context mContext;
	protected APIManager apiManager;
	protected PMSDB mDB;
	protected Prefs mPrefs;

	protected boolean isComplete;

	/**
	 * OKHTTP 사용으로 OOM 현상 방지하기 위해 APIManager를 싱글톤으로 사용
	 * @param context
	 */

	public BaseRequest(Context context) {
		this.mContext = context;
//		this.apiManager = new APIManager(mContext);
		this.mPrefs = new Prefs(context);
		this.mDB = PMSDB.getInstance(context);
	}
}
