package com.pms.sdk.api;

import android.content.Context;
import android.os.SystemClock;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.request.DeviceCert;
import com.pms.sdk.bean.Logs;
import com.pms.sdk.common.compress.CZip;
import com.pms.sdk.common.security.SA2Dec;
import com.pms.sdk.common.security.SA2Enc;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * APIManager
 *
 * @author erzisk
 * @since 2013.05.09
 */
public class APIManager implements IPMSConsts {

    private static APIManager instance;
    private final Context mContext;
    private final Prefs prefs;
    private QueueManager queueManager;
    private JSONObject tempParams;
    private String authStatus;
    private final Logs mLogs;
    private final PMSDB pmsDb;
    private Boolean mbApiState = false;
    private SA2Dec decrypt;
    private SA2Enc encrypt;

    public static APIManager getInstance(Context context)
    {
        if(instance == null)
        {
            instance = new APIManager(context);
        }
        return instance;
    }

    private APIManager(Context context) {
        this.mContext = context;
        this.prefs = new Prefs(mContext);
        this.mLogs = new Logs();
        this.pmsDb = PMSDB.getInstance(mContext);
        this.queueManager = QueueManager.getInstance();
        this.decrypt = new SA2Dec();
        this.encrypt = new SA2Enc();
    }

    public synchronized void call(final String url, final JSONObject params, final APICallback apiCallback) throws Exception {
        CLog.i("API:request=" + url);
        try {
            // save call log
            if (url.equals(API_COLLECT_LOG)) {
                mbApiState = false;
            } else {
                mbApiState = true;
            }

            if (mbApiState) {
                mLogs.date = DateUtil.getNowDateMo();
                mLogs.time = DateUtil.getNowTime();
                mLogs.logFlag = Logs.TYPE_A;
                mLogs.api = url.substring(0, url.length() - 2);
                mLogs.param = params.toString();
            }

            // check network available
            if (!PhoneState.getWifiState(mContext) && !PhoneState.get3GState(mContext)) {
                throw new APIException(CODE_CONNECTION_ERROR, "network not available");
            }

            // check url is null
            if (url == null || "".equals(url)) {
                throw new APIException(CODE_URL_IS_NULL, "url is null");
            }
            String apiServerUrl = PMSUtil.getServerUrl(mContext);
            CLog.d(" SERVER_URL + url = " + apiServerUrl + url);

            // DeviceCert Check
            authStatus = PMSUtil.getDeviceCertStatus(mContext);
            CLog.d(" DeviceCert Status -> " + authStatus);
            if (!DEVICECERT_COMPLETE.equals(authStatus) && (!API_DEVICE_CERT.equals(url) && !API_COLLECT_LOG.equals(url))) {
                PMSUtil.setDeviceCertStatus(mContext, DEVICECERT_PROGRESS);
                new DeviceCert(mContext).request(null, new APICallback() {
                    @Override
                    public void response(String code, JSONObject json) {
                        try {
                            if (CODE_SUCCESS.equals(code)) {
                                PMSUtil.setDeviceCertStatus(mContext, DEVICECERT_COMPLETE);
                                call(url, params, apiCallback);
                            } else {
                                PMSUtil.setDeviceCertStatus(mContext, DEVICECERT_FAIL);
                                if (apiCallback != null) {
                                    apiCallback.response(code, json);
                                }
                            }
                        } catch (Exception e) {
                            CLog.e(e.getMessage());
                        }
                    }
                });
                return;
            }

            // check param is null
            if (params == null || "".equals(params.toString())) {
                throw new APIException(CODE_PARAMS_IS_NULL, "params are null");
            }
            CLog.d(" params(json) = " + params.toString());

            // encrypt
            final String encryptedParam = encrypt(url, params.toString());
            CLog.d(" encryptedParam:" + encryptedParam);

            // call http
            tempParams = params;

            //Check TLS Supports
//            if (Build.VERSION.SDK_INT < 21)
//            {
//                try {
//                    ProviderInstaller.installIfNeededAsync(mContext, new ProviderInstaller.ProviderInstallListener() {
//                        @Override
//                        public void onProviderInstalled()
//                        {
//                            CLog.i("TLS Provider is installed");
//                        }
//
//                        @Override
//                        public void onProviderInstallFailed(int i, Intent intent) {
//                            CLog.i("TLS Provider install is failed");
//                        }
//                    });
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
            if(encryptedParam!=null)
            {
                StringRequest stringRequest = new StringRequest(Request.Method.POST, apiServerUrl + url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String object) {
                        // response result
                        proccessResult(url, object, apiCallback);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError ve) {
                        // error result
                        try {
                            JSONObject errResult = new JSONObject();
                            errResult.put(KEY_API_CODE, CODE_CONNECTION_ERROR);
                            errResult.put(KEY_API_MSG, ve.getMessage());
                            proccessResult(url, errResult.toString(), apiCallback);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError
                    {
                        // set params
                        Map<String, String> map = new HashMap<String, String>();
                        map.put(KEY_API_DEFAULT, encryptedParam);
                        CLog.d(" params(map):" + map.toString());
                        return map;
                    }
                };
                stringRequest.setShouldCache(false);
                stringRequest.setTag("PMS-Base-Msg-api");
                stringRequest.addMarker("PMS-Base-Msg-api");
                queueManager.addRequestQueue(stringRequest);
            }
            else
            {
                String code = CODE_PARAMS_IS_NULL;
                String msg = "encryptedParam is null";

                JSONObject errResult = new JSONObject();
                errResult.put(KEY_API_CODE, code);
                errResult.put(KEY_API_MSG, msg);

                CLog.e("code="+code+" / message="+msg);

                if (apiCallback != null) {
                    apiCallback.response(code, errResult);
                }
            }

        } catch (Exception e) {
            String code = e instanceof APIException ? ((APIException) e).getCode() : CODE_CONNECTION_ERROR;
            String msg = e instanceof APIException ? ((APIException) e).getMsg() : e.toString();

            JSONObject errResult = new JSONObject();
            errResult.put(KEY_API_CODE, code);
            errResult.put(KEY_API_MSG, msg);

            CLog.e("code="+code+" / message="+msg);

            if (apiCallback != null) {
                apiCallback.response(code, errResult);
            }
        }
    }

    /**
     * processResult
     *
     * @param url
     * @param object
     * @param apiCallback
     */
    private boolean proccessResult(final String url, final String object, final APICallback apiCallback) {
        String code = null;
        String msg = null;
        JSONObject result = null;

        try {
            // check result string
            if (object == null || "".equals(object)) {
                throw new APIException(CODE_NOT_RESPONSE, "not response");
            }
            CLog.d(" result:" + object);

            // decrypt
            String decryptedString = decrypt(url, object.toString());
            CLog.d(" result decrypted String:" + decryptedString);

            if(decryptedString!=null)
            {
                if (mbApiState) {
                    mLogs.result = decryptedString;
                    if (FLAG_Y.equals(prefs.getString(PREF_API_LOG_FLAG))) {
                        pmsDb.insertLog(mLogs);
                    }
                }

                // check live session
                long nowTime = SystemClock.currentThreadTimeMillis();

                if (url.indexOf(API_DEVICE_CERT) < 0) {
                    // session time out
                    long lastApiTime = Long.parseLong(prefs.getString(PREF_LAST_API_TIME));
                    CLog.i(" diffTime:" + (nowTime - lastApiTime));
                    if (lastApiTime + EXPIRE_RETAINED_TIME < nowTime) {
                        throw new APIException(CODE_WRONG_SESSION, "expired session");
                    }
                }
                prefs.putString(PREF_LAST_API_TIME, nowTime + "");

                // set result
                result = new JSONObject(decryptedString);
                code = result.getString(KEY_API_CODE);
                msg = result.getString(KEY_API_MSG);

                // check result code
                if (CODE_SUCCESS.equals(code)) {
                    // success
                    CLog.i("API:success");
                } else {
                    // error
                    throw new APIException(code, msg);
                }

                if (apiCallback != null) {
                    apiCallback.response(code, result);
                }
            }
            else
            {
                code = CODE_DECRPYT_FAIL;
                msg = "decryptedString is null";

                JSONObject errResult = new JSONObject();
                errResult.put(KEY_API_CODE, code);
                errResult.put(KEY_API_MSG, msg);

                CLog.e("code="+code+" / message="+msg);

                if (apiCallback != null) {
                    apiCallback.response(code, errResult);
                }
            }

        } catch (Exception e) {
            CLog.e(e.getMessage());
            // e.printStackTrace();
            if (e instanceof APIException && CODE_WRONG_SESSION.equals(((APIException) e).getCode())) {
                // wrong session

                PMSUtil.setDeviceCertStatus(mContext, DEVICECERT_PENDING);
                new DeviceCert(mContext).request(null, new APICallback() {
                    @Override
                    public void response(String code, JSONObject json) {
                        try {
                            if (CODE_SUCCESS.equals(code)) {
                                PMSUtil.setDeviceCertStatus(mContext, DEVICECERT_COMPLETE);
                                call(url, tempParams, apiCallback);
                            }
                        } catch (Exception e) {
                            CLog.e(e.getMessage());
                            // e.printStackTrace();
                        }
                    }
                });
            } else {
                if (apiCallback != null) {
                    apiCallback.response(code, result);
                }
            }
            return false;
        }
        return true;
    }

    /**
     * encrypt
     *
     * @param url
     * @param params
     * @return
     * @throws JSONException
     */
    private String encrypt(String url, String params) throws Exception {
        if (isDefaultEncKey(url)) {
            return encrypt.encode(params, DEFAULT_ENC_KEY);
        } else {
            JSONObject encryptedJobj = new JSONObject();
            String appUserId = PMSUtil.getAppUserId(mContext);
            String encKey = PMSUtil.getEncKey(mContext);
            if (StringUtil.isEmptyArr(new String[]{appUserId, encKey})) {
                throw new APIException(CODE_SESSION_EXPIRED, "appUserId or encKey is null, have to call deviceCert before");
            } else {
                encryptedJobj.put(KEY_APP_USER_ID, appUserId);
                encryptedJobj.put(KEY_ENC_PARAM, encrypt.encode(params, encKey));
                return encrypt.encode(encryptedJobj.toString(), DEFAULT_ENC_KEY);
            }
        }
    }

    /**
     * decrypt
     *
     * @param url
     * @param object
     * @return
     * @throws Exception
     */
    private String decrypt(String url, String object) throws Exception {
        String result;
        try {
            result = decrypt.decrypt(object, getEncKey(url));
            if (result == null) {
                result = object;
            }
        } catch (Exception e) {
            // wrong session인 경우 암호화지 않은 상태에서 데이터가 넘어온다.
            result = object;
        }
        return result;
    }

    /**
     * get enc key
     *
     * @param url
     * @return
     */
    private String getEncKey(String url) {
        if (isDefaultEncKey(url)) {
            return DEFAULT_ENC_KEY;
        } else {
            return PMSUtil.getEncKey(mContext);
        }
    }

    /*
     * is default encrypt key
     */
    private boolean isDefaultEncKey(String url) {
        if (url.indexOf(API_DEVICE_CERT) > -1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * https
     *
     * @param url
     * @return
     */
    private boolean isHttps(String url) {
        return false;
    }

    /**
     * is multipart
     *
     * @param url
     * @return
     */
    private boolean isMultipart(String url) {
        return false;
    }

    /**
     * api result callback
     *
     * @author erzisk
     */
    public interface APICallback {
        void response(String code, JSONObject json);
    }

    public Prefs getPrefs()
    {
        return prefs;
    }

    public PMSDB getPmsDb()
    {
        return pmsDb;
    }

}
