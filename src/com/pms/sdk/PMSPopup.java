package com.pms.sdk;

import java.io.Serializable;

import org.json.JSONObject;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;

import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.PMSPopupUtil;
import com.pms.sdk.common.util.PMSPopupUtil.btnEventListener;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.push.PushReceiver;

/**
 * @since 2013.12.11
 * @author Yang
 * @description pms popup ui setting
 * @version
 * 
 */
public class PMSPopup implements IPMSConsts, Serializable {

	private static final long serialVersionUID = 1L;

	private static PMSPopup instance = null;

	private static Boolean mPopupState = false;

	private static String mstrPackName = "";

	private static String mstrTitleName = "";

	public PMSPopupUtil mPmsPopUtil = null;

	public PMSPopup(Context context) {
		InitSetting(context);
	}

	public static PMSPopup getInstance (Context context, String packagename, Boolean popupState, String titleName) {
		mPopupState = popupState;
		mstrPackName = packagename;
		mstrTitleName = titleName;

		if (instance == null) {
			instance = new PMSPopup(context);
		}
		return instance;
	}

	public static PMSPopup getInstance (Context context) {
		if (instance == null) {
			instance = new PMSPopup(context);
		}
		return instance;
	}

	private void InitSetting (Context context) {
		mPmsPopUtil = new PMSPopupUtil();
		mPmsPopUtil.setParmSetting(context);
		mPmsPopUtil.setPackageName(mstrPackName);

		if (mPopupState == true) {
			setXmlAndDefaultFlag(!mPopupState);
			setDefaultPopup();
		}
	}

	// ////////////////////////////////////////POPUP LAYOUT SETTING ////////////////////////////////////////////////////////
	/**
	 * Set Push Popup Background Color & Alpha
	 * 
	 * @param red
	 *        0 ~ 255 (255 : Max)
	 * @param green
	 *        0 ~ 255 (255 : Max)
	 * @param blue
	 *        0 ~ 255 (255 : Max)
	 * @param alpha
	 *        0 ~ 255 (255 : Max)
	 */
	public void setPopUpBackColor (int red, int green, int blue, int alpha) {
		mPmsPopUtil.setPopUpBackColor(red, green, blue, alpha);
	}

	/**
	 * Set Push PopUp Content Layout Background Image
	 *
	 *        Image file name
	 */
	public void setPopupBackImgResource (String filename) {
		mPmsPopUtil.setPopupBackImgResource(filename);
	}

	// ////////////////////////////////////////TOP LAYOUT SETTING //////////////////////////////////////////////////////////
	/**
	 * Set Push Popup Top Layout ON & OFF
	 * 
	 * @param flag
	 *        ON & OFF (true & false)
	 */
	public void setTopLayoutFlag (Boolean flag) {
		mPmsPopUtil.setTopLayoutFlag(flag);
	}

	/**
	 * Set Push Popup Top Layout Background Color & Alpha
	 * 
	 * @param red
	 *        0 ~ 255 (255 : Max)
	 * @param green
	 *        0 ~ 255 (255 : Max)
	 * @param blue
	 *        0 ~ 255 (255 : Max)
	 * @param alpha
	 *        0 ~ 255 (255 : Max)
	 */
	public void setTopBackColor (int red, int green, int blue, int alpha) {
		mPmsPopUtil.setTopBackColor(red, green, blue, alpha);
	}

	/**
	 * Set Push PopUp Top Layout Background Image
	 *
	 *        Image file name
	 */
	public void setTopBackImgResource (String filename) {
		mPmsPopUtil.setTopBackImgResource(filename);
	}

	/**
	 * Set Push Popup Top Layout Title Name Type
	 * 
	 * @param type
	 *        'text' & 'image'
	 */
	public void setTopTitleType (String type) {
		mPmsPopUtil.setTopTitleType(type);
	}

	/**
	 * Set Push PopUp Top Layout Title Name Image
	 *
	 *        Image file name
	 */
	public void setTopTitleImgResource (String filename) {
		mPmsPopUtil.setTopTitleImgResource(filename);
	}

	/**
	 * Set Push Popup Top Layout Title Name Text
	 *
	 *        Title Name
	 */
	public void setTopTitleName (String name) {
		mPmsPopUtil.setTopTitleName(name);
	}

	/**
	 * Set Push Popup Top Title Text Color
	 * 
	 * @param red
	 *        0 ~ 255 (255 : Max)
	 * @param green
	 *        0 ~ 255 (255 : Max)
	 * @param blue
	 *        0 ~ 255 (255 : Max)
	 */
	public void setTopTitleTextColor (int red, int green, int blue) {
		mPmsPopUtil.setTopTitleTextColor(red, green, blue);
	}

	/**
	 * Set Push Popup Top Title Text Size
	 * 
	 * @param size
	 *        Font Size (Default : 25)
	 */
	public void setTopTitleTextSize (int size) {
		mPmsPopUtil.setTopTitleTextSize(size);
	}

	// ////////////////////////////////////////CONTENT LAYOUT SETTING //////////////////////////////////////////////////////////
	/**
	 * Set Push Popup Content Layout Background Color & Alpha
	 * 
	 * @param red
	 *        0 ~ 255 (255 : Max)
	 * @param green
	 *        0 ~ 255 (255 : Max)
	 * @param blue
	 *        0 ~ 255 (255 : Max)
	 * @param alpha
	 *        0 ~ 255 (255 : Max)
	 */
	public void setContentBackColor (int red, int green, int blue, int alpha) {
		mPmsPopUtil.setContentBackColor(red, green, blue, alpha);
	}

	/**
	 * Set Push PopUp Content Layout Background Image
	 *
	 *        Image file name
	 */
	public void setContentBackImgResource (String filename) {
		mPmsPopUtil.setContentBackImgResource(filename);
	}

	/**
	 * Set Push Popup Content Text Color
	 * 
	 * @param red
	 *        0 ~ 255 (255 : Max)
	 * @param green
	 *        0 ~ 255 (255 : Max)
	 * @param blue
	 *        0 ~ 255 (255 : Max)
	 */
	public void setContentTextColor (int red, int green, int blue) {
		mPmsPopUtil.setContentTextColor(red, green, blue);
	}

	/**
	 * Set Push Popup Content Text Size
	 * 
	 * @param size
	 *        Font Size (Default : 15)
	 */
	public void setContentTextSize (int size) {
		mPmsPopUtil.setContentTextSize(size);
	}

	// //////////////////////////////////////// BOTTOM LAYOUT SETTING //////////////////////////////////////////////////////////
	/**
	 * Set Push Popup Bottom Layout Background Image
	 * 
	 * @param filename
	 *        image file name
	 */
	public void setBottomBackImgResource (String filename) {
		mPmsPopUtil.setBottomBackImgResource(filename);
	}

	/**
	 * Set Push Popup Bottom Layout Background Color & Alpha
	 * 
	 * @param red
	 *        0 ~ 255 (255 : Max)
	 * @param green
	 *        0 ~ 255 (255 : Max)
	 * @param blue
	 *        0 ~ 255 (255 : Max)
	 * @param alpha
	 *        0 ~ 255 (255 : Max)
	 */
	public void setBottomBackColor (int red, int green, int blue, int alpha) {
		mPmsPopUtil.setBottomBackColor(red, green, blue, alpha);
	}

	/**
	 * Set Push PopUp Bottom Text Button Count
	 * 
	 * @param count
	 *        Button Count (MAX : 2)
	 */
	public void setBottomTextBtnCount (int count) {
		mPmsPopUtil.setBottomTextBtnCount(count);
	}

	/**
	 * Set Push PopUp Bottom Rich Button Count
	 * 
	 * @param count
	 *        Button Count (MAX : 2)
	 */
	public void setBottomRichBtnCount (int count) {
		mPmsPopUtil.setBottomRichBtnCount(count);
	}

	/**
	 * Set Push PopUp Bottom Button Image
	 * 
	 * @param args
	 *        array image file name
	 */
	public void setBottomBtnImageResource (String... args) {
		mPmsPopUtil.setBottomBtnImageResource(args);
	}

	/**
	 * Set Push Popup Bottom Button Name ON & OFF
	 * 
	 * @param state
	 *        ON & OFF (true & false)
	 */
	public void setBottomTextViewFlag (Boolean state) {
		mPmsPopUtil.setBottomTextViewFlag(state);
	}

	/**
	 * Set Push Popup Bottom Button Text Name
	 * 
	 * @param name
	 *        array button name
	 */
	public void setBottomBtnTextName (String... name) {
		mPmsPopUtil.setBottomBtnTextName(name);
	}

	/**
	 * Set Push Popup Bottom Button Text Color
	 * 
	 * @param red
	 *        0 ~ 255 (255 : Max)
	 * @param green
	 *        0 ~ 255 (255 : Max)
	 * @param blue
	 *        0 ~ 255 (255 : Max)
	 */
	public void setBottomBtnTextColor (int red, int green, int blue) {
		mPmsPopUtil.setBottomBtnTextColor(red, green, blue);
	}

	/**
	 * Set Push Popup Bottom Button Text Size
	 * 
	 * @param size
	 *        Font Size (Default : 15)
	 */
	public void setBottomBtnTextSize (int size) {
		mPmsPopUtil.setBottomBtnTextSize(size);
	}

	/**
	 * Set Push Popup Bottom Button OnclickListener
	 * 
	 * @param ocl
	 *        set array OnclickListener
	 */
	public void setTextBottomBtnClickListener (btnEventListener... ocl) {
		mPmsPopUtil.setTextBottomBtnClickListener((Object[]) ocl);
	}

	/**
	 * Set Push Popup Bottom Button OnclickListener
	 * 
	 * @param ocl
	 *        set array OnclickListener
	 */
	public void setRichBottomBtnClickListener (btnEventListener... ocl) {
		mPmsPopUtil.setRichBottomBtnClickListener((Object[]) ocl);
	}

	// ////////////////////////////////////////XML LAYOUT SETTING //////////////////////////////////////////////////////////
	/**
	 * Set Push Popup XML Text Layout
	 * 
	 * @param filename
	 *        XML File Name
	 */
	public void setLayoutXMLTextResId (String filename) {
		mPmsPopUtil.setLayoutXMLTextResId(filename);
	}

	/**
	 * Set Push Popup XML Rich Layout
	 * 
	 * @param filename
	 *        XML File Name
	 */
	public void setLayoutXMLRichResId (String filename) {
		mPmsPopUtil.setLayoutXMLRichResId(filename);
	}

	/**
	 * Set XML Text Push Button Type
	 * 
	 * @param viewtype
	 *        set array Resource Type
	 */
	public void setXMLTextButtonType (String... viewtype) {
		mPmsPopUtil.setXMLTextButtonType(viewtype);
	}

	/**
	 * Set XML Text Push Button Tag Name
	 * 
	 * @param name
	 *        set array Resource Tag Name
	 */
	public void setXMLTextButtonTagName (String... name) {
		mPmsPopUtil.setXMLTextButtonTagName(name);
	}

	/**
	 * Set XML Rich Push Button Type
	 * 
	 * @param viewtype
	 *        set array Resource Type
	 */
	public void setXMLRichButtonType (String... viewtype) {
		mPmsPopUtil.setXMLRichButtonType(viewtype);
	}

	/**
	 * Set XML Rich Push Button Tag Name
	 * 
	 * @param name
	 *        set array Resource Tag Name
	 */
	public void setXMLRichButtonTagName (String... name) {
		mPmsPopUtil.setXMLRichButtonTagName(name);
	}

	/**
	 * Set XML File ON & OFF
	 * 
	 * @param flag
	 *        ON & OFF (true & false)
	 */
	public void setXmlAndDefaultFlag (Boolean flag) {
		mPmsPopUtil.setXmlAndDefaultFlag(flag);
	}

	// ////////////////////////////////////////OTHER SETTING //////////////////////////////////////////////////////////
	/**
	 * Get Push Popup Activity
	 * 
	 * @return Popup Activity
	 */
	public Activity getActivity () {
		return mPmsPopUtil.getActivity();
	}

	public void commit () {
		mPmsPopUtil.saveMapData();
	}

	/**
	 * Push Popup Default Setting
	 *
	 *        Popup Title Name
	 */
	public void setDefaultPopup () {
		setPopUpBackColor(128, 128, 128, 200);
		setTopLayoutFlag(true);
		setTopTitleType("text");
		setTopTitleTextColor(255, 255, 255);
		setTopTitleName(mstrTitleName);
		setContentTextColor(255, 255, 255);
		setBottomTextBtnCount(2);
		setBottomRichBtnCount(1);
		setBottomTextViewFlag(false);
		setBottomBtnTextName("닫기", "자세히 보기");
		// setBottomBtnTextName("Close", "More");
		setTextBottomBtnClickListener(btnEvent, btnEvent1);
		setRichBottomBtnClickListener(btnEvent);
		commit();
	}

	/**
	 * noti receiver
	 */
	public void startNotiReceiver () {
		JSONObject read = new JSONObject();
		try {
			read.put("msgId", mPmsPopUtil.getMsgId());
			read.put("workday", DateUtil.getNowDate());
		} catch (Exception e) {
			e.printStackTrace();
		}
		CLog.d(String.format("msgId:%s", mPmsPopUtil.getMsgId()));

		PMSUtil.arrayToPrefs(getActivity(), PREF_READ_LIST, read);
		Prefs prefs = new Prefs(getActivity());
		Intent innerIntent = null;
		String receiverClass = prefs.getString(PREF_NOTI_RECEIVER_CLASS);
		String receiverAction = prefs.getString(PREF_NOTI_RECEIVER);
		receiverAction = receiverAction != null ? receiverAction : "com.pms.sdk.notification";

		if (receiverClass != null) {
			try {
				Class<?> cls = Class.forName(receiverClass);
				innerIntent = new Intent(getActivity().getApplicationContext(), cls).putExtras(getActivity().getIntent().getExtras());
				innerIntent.setAction(receiverAction);
			} catch (ClassNotFoundException e) {
				CLog.e(e.getMessage());
			}
		}

		if (innerIntent == null) {
			CLog.d("innerIntent == null");
			// setting push info to intent
			innerIntent = new Intent(receiverAction).putExtras(getActivity().getIntent().getExtras());
		}

		innerIntent.putExtras(getActivity().getIntent().getExtras());
		getActivity().sendBroadcast(innerIntent);

		hideNotification(getActivity());
	}

	/**
	 * hide notification
	 */
	private void hideNotification (Context context) {
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(PushReceiver.NOTIFICATION_ID);
	}

	private btnEventListener btnEvent = new btnEventListener() {

		private static final long serialVersionUID = 1L;

		public void response () {
			getActivity().finish();
		}
	};

	private btnEventListener btnEvent1 = new btnEventListener() {

		private static final long serialVersionUID = 1L;

		public void response () {
			startNotiReceiver();
			getActivity().finish();
		}
	};
}