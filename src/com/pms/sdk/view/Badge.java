package com.pms.sdk.view;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.widget.TextView;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.db.PMSDB;

/**
 * @since 2013.01.04
 * @author erzisk
 * @description badge<br>
 * <br>
 *              badge refresh가 필요한 시점<br>
 * 
 *              - new msg api를 콜한 후에<br>
 *              - msg grp를 터치해서 msg fragment로 넘어간 후에 (msg grp를 터치했다는 것은 해당 msg grp의 new msg cnt가 0이 되므로 필요)<br>
 *              - push를 받았을 때<br>
 */
public class Badge implements IPMSConsts {

	private Context mContext;
	private PMSDB mDB;

	private List<TextView> mLstBadge = new ArrayList<TextView>();

	public Badge(Context c) {
		this.mContext = c;
		mDB = PMSDB.getInstance(mContext);
//		mContext.registerReceiver(refreshReceiver, new IntentFilter(RECEIVER_PUSH));
//		mContext.registerReceiver(refreshReceiver, new IntentFilter(RECEIVER_REQUERY));
	}

	private static Badge instance;

	public static Badge getInstance (Context c) {
		if (instance == null) {
			instance = new Badge(c);
		}

		return instance;
	}

	public void unregisterReceiver () {
		try
		{
			mContext.unregisterReceiver(refreshReceiver);
		}
		catch (Exception e)
		{

		}
		instance.clear();
	}

	public void clear () {
		mLstBadge.clear();
	}

	public void addBadge (Context c, int id) {
//		addBadge((TextView) ((Activity) c).findViewById(id));
	}

	public void addBadge (TextView badge) {
		if (!mLstBadge.contains(badge)) {
			mLstBadge.add(badge);
		}
//		updateBadge();
	}

	private BroadcastReceiver refreshReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive (Context context, Intent intent) {
			CLog.i("refreshReceiver:onReceive()");
//			updateBadge();
		}
	};

	public void updateBadge () {
		int newMsgCnt = mDB.selectNewMsgCnt();

		for (TextView badge : mLstBadge) {
			try {
				CLog.i("updateBadge:newMsgCnt=" + newMsgCnt);
				badge.setText(newMsgCnt + "");
			} catch (Exception e) {
				CLog.e("updateBadge:invalid badge (" + e.getMessage() + ")");
				mLstBadge.remove(badge);
			}
		}
	}

	public void updateBadge (String str) {
		for (TextView badge : mLstBadge) {
			try {
				CLog.i("updateBadge:str=" + str);
				badge.setText(str);
			} catch (Exception e) {
				CLog.e("updateBadge:invalid badge (" + e.getMessage() + ")");
				mLstBadge.remove(badge);
			}
		}
	}
}
