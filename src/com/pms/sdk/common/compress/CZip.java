package com.pms.sdk.common.compress;

import android.os.Build;

import com.pms.sdk.common.util.CLog;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class CZip {

	public synchronized byte[] zipStringToBytes (String input, String charSet)
    {
        try
        {
            int bufferSize;
            if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1)
            {
                bufferSize = 10;
            }
            else
            {
                bufferSize = 100;
            }

            ByteArrayOutputStream zipByteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gzipOutputStream = new GZIPOutputStream(zipByteArrayOutputStream);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(gzipOutputStream, bufferSize);

            bufferedOutputStream.write(input.getBytes(charSet));


            if(bufferedOutputStream!=null)
            {
                bufferedOutputStream.close();
                bufferedOutputStream = null;
            }
            if(gzipOutputStream!=null)
            {
                gzipOutputStream.close();
                gzipOutputStream = null;
            }
            if(zipByteArrayOutputStream !=null)
            {
                zipByteArrayOutputStream.close();
            }
            final byte[] output = zipByteArrayOutputStream.toByteArray();
            zipByteArrayOutputStream = null;
            return output;
        }catch (OutOfMemoryError | IOException e)
        {
            CLog.e(e.toString());
            return null;
        }
	}

	public byte[] zipStringToBytes (String input)
    {
		return zipStringToBytes(input, "UTF-8");
	}

	public synchronized String unzipStringFromBytes (byte[] bytes)
	{
	    try
        {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
            GZIPInputStream gzipInputStream = new GZIPInputStream(byteArrayInputStream);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(gzipInputStream);
            ByteArrayOutputStream unzipByteArrayOutputStream = new ByteArrayOutputStream();

            int bufferSize;
            if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1)
            {
                bufferSize = 10;
            }
            else
            {
                bufferSize = 100;
            }

            byte[] buffer = new byte[bufferSize];
            int length;
            while ((length = bufferedInputStream.read(buffer)) > 0)
            {
                unzipByteArrayOutputStream.write(buffer, 0, length);
            }
            if(unzipByteArrayOutputStream !=null)
            {
                unzipByteArrayOutputStream.close();
            }
            if(bufferedInputStream!=null)
            {
                bufferedInputStream.close();
                bufferedInputStream = null;
            }
            if(gzipInputStream!=null)
            {
                gzipInputStream.close();
                gzipInputStream = null;
            }
            if(byteArrayInputStream!=null)
            {
                byteArrayInputStream.close();
                byteArrayInputStream = null;
            }

            final String output = unzipByteArrayOutputStream.toString();
            unzipByteArrayOutputStream = null;
            return output;
        }catch (OutOfMemoryError | IOException e)
        {
            CLog.e(e.toString());
            return null;
        }
	}

	public static boolean unzip (String srcPath, String dstPath) {

		if (srcPath == null || dstPath == null) {
			return false;
		}

		ZipInputStream zis = null;
		FileOutputStream fos = null;
		ZipEntry entry = null;
		int bytes_read;
		byte[] buf = new byte[4096];

		try {
			File dir = new File(dstPath);
			if (!dir.exists())
				dir.mkdirs();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {

			zis = new ZipInputStream(new FileInputStream(srcPath));
			boolean esc = false;

			while (true) {

				entry = zis.getNextEntry();
				String nm = entry.getName();
				fos = new FileOutputStream(dstPath + "/" + nm);

				while ((bytes_read = zis.read(buf)) != -1) {
					fos.write(buf, 0, bytes_read);
				}

				fos.close();

				if (esc)
					break;
				else if (zis.available() != 1)
					esc = true;

			}

		} catch (Exception e) {

			e.printStackTrace();

			return false;
		} finally {

			try {
				zis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			;
		}

		return true;
	}

}
