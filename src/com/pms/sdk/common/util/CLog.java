package com.pms.sdk.common.util;

import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Formatter;

/**
 * @since 2013.11.01
 * @author Yang
 * @description log (custom)
 */
public class CLog {
	public static boolean isPrintLog = true; // true : 로그 출력 false : 로그 미출력
	public static boolean isFileLog = true; // true : 로그 파일 저장 false : 로그 저장하지 않음

	private static final int LOG_D = 0;
	private static final int LOG_I = 1;
	private static final int LOG_W = 2;
	private static final int LOG_E = 3;

	public static final int DEBUG_LEVEL = LOG_D;

	private static String TAG = "PMS";

	public static final byte[] LOG_SPLIT_ITEM = { 0x2C }; // 저장 항목 분리자(,)
	public static final byte[] LOG_SPLIT_LINE = { 0x0D }; // 라인 분리자(\n)

	private static class ReusableFormatter {
		private Formatter formatter;
		private StringBuilder builder;

		public ReusableFormatter() {
			builder = new StringBuilder();
			formatter = new Formatter(builder);
		}

		public String format (String msg, Object... args) {
			formatter.format(msg, args);
			String s = builder.toString();
			builder.setLength(0);
			return s;
		}
	}

	private static final ThreadLocal<ReusableFormatter> thread_local_formatter = new ThreadLocal<ReusableFormatter>() {
		@SuppressWarnings("unused")
		protected ReusableFormatter initiaValue () {
			return new ReusableFormatter();
		}
	};

	public static void setTagName (String tagName) {
		CLog.TAG = tagName;
	}
	
	public static void setDebugFile (boolean debugFile) {
		CLog.isFileLog = debugFile;
	}

	public static void setDebugMode (boolean debugMode) {
		CLog.isPrintLog = debugMode;
	}

	public static String format (String msg, Object... args) {
		ReusableFormatter formatter = thread_local_formatter.get();
		return formatter.format(msg, args);
	}

	private static void printLog (int logLevel, String log) {
		if (!isPrintLog)
			return; // 로그 미출력
//		try {
//			writeToFile(TAG, log);
//		} catch (Exception e) {
//			Log.e(TAG, e.getMessage());
//		}
		
		switch (DEBUG_LEVEL) {
			case LOG_D:
				if (logLevel == LOG_D)
					Log.d(TAG, log);
			case LOG_I:
				if (logLevel == LOG_I)
					Log.i(TAG, log);
			case LOG_W:
				if (logLevel == LOG_W)
					Log.w(TAG, log);
			case LOG_E:
				if (logLevel == LOG_E)
					Log.e(TAG, log);
				break;
		}
	}

	private static void printLog (String tag, int logLevel, String log) {
		if (!isPrintLog)
			return; // 로그 미출력

//		try {
//			writeToFile(tag, log);
//		} catch (Exception e) {
//			Log.e(TAG, e.getMessage());
//		}
		
		switch (DEBUG_LEVEL) {
			case LOG_D:
				if (logLevel == LOG_D)
					Log.d(tag, log);
			case LOG_I:
				if (logLevel == LOG_I)
					Log.i(tag, log);
			case LOG_W:
				if (logLevel == LOG_W)
					Log.w(tag, log);
			case LOG_E:
				if (logLevel == LOG_E)
					Log.e(tag, log);
				break;
		}
	}

	private static void writeToFile (String tag, String logTxt) throws Exception {
		if (!isFileLog)
			return;
		
		new File("/sdcard/pms_file").mkdir();
		
		File file = new File("/sdcard/pms_file", "humuson_Log.txt");
		if (!file.exists()) {
			file.createNewFile();
		} else {
			long fileSize = file.length();
			if (fileSize >= 10000000) { // 10MB
				file.delete();
				file.createNewFile();
			}
		}

		FileWriter fw = new FileWriter(file, true);
		BufferedWriter bufferWritter = new BufferedWriter(fw);
		bufferWritter.write(new String(logTxt.getBytes("UTF-8"), "UTF-8"));
		bufferWritter.close();
	}
	
	private static final String getFileName (int a_nDepth) {
		try {
			StackTraceElement oStack = Thread.currentThread().getStackTrace()[a_nDepth];
			return oStack.getFileName();
		} catch (Throwable e) {

		}
		return "FFF";
	}

	private static final String getLineNumber (int a_nDepth) {
		try {
			StackTraceElement oStack = Thread.currentThread().getStackTrace()[a_nDepth];
			return String.valueOf(oStack.getLineNumber());
		} catch (Throwable e) {

		}
		return "LLL";
	}

	private static final String getFileLine (int a_nDepth) {
		++a_nDepth;
		return getFileName(a_nDepth) + ":" + getLineNumber(a_nDepth);
	}

	public static final void d (String s) {
		printLog(LOG_D, getFileLine(4) + "> " + s);
	}

	public static final void i (String s) {
		printLog(LOG_I, getFileLine(4) + "> " + s);
	}

	public static final void w (String s) {
		printLog(LOG_W, getFileLine(4) + "> " + s);
	}

	public static final void e (String s) {
		printLog(LOG_E, getFileLine(4) + "> " + s);
	}

	public static final void d (String tag, String s) {
		printLog(tag, LOG_D, getFileLine(4) + "> " + s);
	}

	public static final void i (String tag, String s) {
		printLog(tag, LOG_I, getFileLine(4) + "> " + s);
	}

	public static final void w (String tag, String s) {
		printLog(tag, LOG_W, getFileLine(4) + "> " + s);
	}

	public static final void e (String tag, String s) {
		printLog(tag, LOG_E, getFileLine(4) + "> " + s);
	}

	public static final void logE (Throwable e) {
		printLog(LOG_E, getFileLine(4) + "> " + e.toString());
	}

	// int 를 1byte 로 변환한다.
	public static byte[] writeByteOne (int val) throws Exception {
		byte[] bytes = new byte[1];

		bytes[0] = (byte) val;

		return bytes;
	}

	/**
	 * int를 byte[4] 로 변환
	 * 
	 * @param val
	 * @return
	 * @throws Exception
	 */
	public static byte[] writeByteInt (int val) throws Exception {
		byte[] bytes = new byte[4];

		bytes[0] = (byte) (0xff & (val >> 24));
		bytes[1] = (byte) (0xff & (val >> 16));
		bytes[2] = (byte) (0xff & (val >> 8));
		bytes[3] = (byte) (0xff & val);

		return bytes;
	}

	// a 에 b 를 추가한다.
	public static byte[] addBytes (byte[] a, byte[] b) throws Exception {
		int asize = 0;
		int bsize = 0;
		if (a != null)
			asize = a.length;
		if (b != null)
			bsize = b.length;

		byte[] bytesum = new byte[asize + bsize];
		if (a != null)
			System.arraycopy(a, 0, bytesum, 0, asize);
		if (b != null)
			System.arraycopy(b, 0, bytesum, asize, bsize);

		return bytesum;

	}

	/**
	 * 로그 메시지에 기타 정보를 포함하여 반환한다.
	 * 
	 * @param msg
	 *        실제 로그 메시지
	 * @return
	 */
	protected static String getFullMsg (String msg) {
		if (msg == null)
			msg = "";
		StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[4]; // 이 getFullMsg 메서드가 변형될 경우, 4라는 숫자는 바뀌어야할 수도 있다.
		String className = stackTraceElement.getClassName();
		className = className.substring(className.lastIndexOf(".") + 1);
		String methodName = stackTraceElement.getMethodName();
		msg = className + "." + methodName + ": " + msg;
		return msg;
	}
}
