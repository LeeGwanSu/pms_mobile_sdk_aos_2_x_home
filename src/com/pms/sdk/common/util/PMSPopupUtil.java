package com.pms.sdk.common.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Externalizable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pms.sdk.IPMSConsts;

/**
 * 
 * @author Yang
 * @since 2013.12.11
 */
public class PMSPopupUtil implements IPMSConsts, Externalizable {

	private static final long serialVersionUID = 1L;

	private Context mContext = null;

	private String mStrPackageName = "";

	private transient SharedPreferences mPref = null;

	public HashMap<String, Object> mResData = new HashMap<String, Object>();

	public void setParmSetting (Context context) {
		mContext = context;
		mPref = mContext.getSharedPreferences("popupUI", Activity.MODE_PRIVATE);
	}

	public void setPackageName (String name) {
		Editor edit = mPref.edit();
		mStrPackageName = name;

		edit.putString("packagename", name);
		edit.commit();
	}

	public String getPackageName () {
		return mPref.getString("packagename", "");
	}

	public void saveMapData () {
		writeOnPreference();
	}

	public HashMap<String, Object> getSaveMapData () {
		readOnPreference();
		return mResData;
	}

	@Override
	public void readExternal (ObjectInput objectinput) throws IOException, ClassNotFoundException {
		SerializableClass sc = (SerializableClass) objectinput.readObject();
		mResData = sc.mResData;
	}

	public void readOnPreference()
	{
		Prefs prefs = new Prefs(mContext);
		String serializableObjectString = prefs.getString(IPMSConsts.PREF_POPUP_SETTING);
		byte[] bytes = Base64.decode(serializableObjectString,0);
		SerializableClass object = null;
		try {
			ObjectInputStream objectInputStream = new ObjectInputStream( new ByteArrayInputStream(bytes) );
			object = (SerializableClass) objectInputStream.readObject();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (ClassCastException e) {
			e.printStackTrace();
		}
		mResData = object.mResData;
	}

	@Override
	public void writeExternal (ObjectOutput objectoutput) throws IOException {
		objectoutput.writeObject(new SerializableClass(mResData));
	}

	public void writeOnPreference()
	{
		String encoded = null;
		try {
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
			objectOutputStream.writeObject(new SerializableClass(mResData));
			objectOutputStream.close();
			encoded = new String(Base64.encodeToString(byteArrayOutputStream.toByteArray(),0));
			Prefs prefs = new Prefs(mContext);
			prefs.putString(PREF_POPUP_SETTING, encoded);
		} catch (IOException e) {
			CLog.e(e.getMessage());
		}
	}

	// ////////////////////////////////////////POPUP LAYOUT SETTING ////////////////////////////////////////////////////////
	/**
	 * Set Push Popup Background Color & Alpha
	 * 
	 * @param red
	 *        0 ~ 255 (255 : Max)
	 * @param green
	 *        0 ~ 255 (255 : Max)
	 * @param blue
	 *        0 ~ 255 (255 : Max)
	 * @param alpha
	 *        0 ~ 255 (255 : Max)
	 */
	public void setPopUpBackColor (int red, int green, int blue, int alpha) {
		String argb = "#" + StringUtil.intToHex(alpha) + StringUtil.intToHex(red) + StringUtil.intToHex(green) + StringUtil.intToHex(blue);
		mResData.put(POPUP_BACKGROUND_COLOR, argb);
	}

	public String getPopUpBackColor () {
		return (String) mResData.get(POPUP_BACKGROUND_COLOR);
	}

	/**
	 * Set Push PopUp Content Layout Background Image
	 *
	 *        Image file name
	 */
	public void setPopupBackImgResource (String filename) {
		int resid = mContext.getResources().getIdentifier(filename, "drawable", mStrPackageName);
		mResData.put(POPUP_BACKGROUND_RES_ID, resid);
	}

	public int getPopupBackImgResource () {
		return (Integer) mResData.get(POPUP_BACKGROUND_RES_ID);
	}

	// ////////////////////////////////////////TOP LAYOUT SETTING //////////////////////////////////////////////////////////
	/**
	 * Set Push Popup Top Layout ON & OFF
	 * 
	 * @param flag
	 *        ON & OFF (true & false)
	 */
	public void setTopLayoutFlag (Boolean flag) {
		mResData.put(TOP_LAYOUT_FLAG, flag);
	}

	public Boolean getTopLayoutFlag () {
		return (Boolean) mResData.get(TOP_LAYOUT_FLAG);
	}

	/**
	 * Set Push Popup Top Layout Background Color & Alpha
	 * 
	 * @param red
	 *        0 ~ 255 (255 : Max)
	 * @param green
	 *        0 ~ 255 (255 : Max)
	 * @param blue
	 *        0 ~ 255 (255 : Max)
	 * @param alpha
	 *        0 ~ 255 (255 : Max)
	 */
	public void setTopBackColor (int red, int green, int blue, int alpha) {
		String argb = "#" + StringUtil.intToHex(alpha) + StringUtil.intToHex(red) + StringUtil.intToHex(green) + StringUtil.intToHex(blue);
		mResData.put(TOP_BACKGROUND_COLOR, argb);
	}

	public String getTopBackColor () {
		return (String) mResData.get(TOP_BACKGROUND_COLOR);
	}

	/**
	 * Set Push PopUp Top Layout Background Image
	 *
	 *        Image file name
	 */
	public void setTopBackImgResource (String filename) {
		int resid = mContext.getResources().getIdentifier(filename, "drawable", mStrPackageName);
		mResData.put(TOP_BACKGROUND_RES_ID, resid);
	}

	public int getTopBackImgResource () {
		return (Integer) mResData.get(TOP_BACKGROUND_RES_ID);
	}

	/**
	 * Set Push Popup Top Layout Title Name Type
	 * 
	 * @param type
	 *        'text' & 'image'
	 */
	public void setTopTitleType (String type) {
		mResData.put(TOP_TITLE_TYEP, type);
	}

	public String getTopTitleType () {
		return (String) mResData.get(TOP_TITLE_TYEP);
	}

	/**
	 * Set Push PopUp Top Layout Title Name Image
	 *
	 *        Image file name
	 */
	public void setTopTitleImgResource (String filename) {
		int resid = mContext.getResources().getIdentifier(filename, "drawable", mStrPackageName);
		mResData.put(TOP_TITLE_RES_ID, resid);
	}

	public int getTopTitleImgResource () {
		return (Integer) mResData.get(TOP_TITLE_RES_ID);
	}

	/**
	 * Set Push Popup Top Layout Title Name Text
	 *
	 *        Title Name
	 */
	public void setTopTitleName (String name) {
		mResData.put(TOP_TITLE_TEXT_DATA, name);
	}

	public String getTopTitleName () {
		return (String) mResData.get(TOP_TITLE_TEXT_DATA);
	}

	/**
	 * Set Push Popup Top Title Text Color
	 * 
	 * @param red
	 *        0 ~ 255 (255 : Max)
	 * @param green
	 *        0 ~ 255 (255 : Max)
	 * @param blue
	 *        0 ~ 255 (255 : Max)
	 */
	public void setTopTitleTextColor (int red, int green, int blue) {
		String rgb = "#" + StringUtil.intToHex(red) + StringUtil.intToHex(green) + StringUtil.intToHex(blue);
		mResData.put(TOP_TITLE_TEXT_COLOR, rgb);
	}

	public String getTopTitleTextColor () {
		return (String) mResData.get(TOP_TITLE_TEXT_COLOR);
	}

	/**
	 * Set Push Popup Top Title Text Size
	 * 
	 * @param size
	 *        Font Size (Default : 25)
	 */
	public void setTopTitleTextSize (int size) {
		mResData.put(TOP_TITLE_SIZE, size);
	}

	public int getTopTitleTextSize () {
		if (mResData.containsKey(TOP_TITLE_SIZE)) {
			return (Integer) mResData.get(TOP_TITLE_SIZE);
		} else {
			return TITLE_TEXT_DEFAULT_SIZE;
		}
	}

	// ////////////////////////////////////////CONTENT LAYOUT SETTING //////////////////////////////////////////////////////////
	/**
	 * Set Push Popup Content Layout Background Color & Alpha
	 * 
	 * @param red
	 *        0 ~ 255 (255 : Max)
	 * @param green
	 *        0 ~ 255 (255 : Max)
	 * @param blue
	 *        0 ~ 255 (255 : Max)
	 * @param alpha
	 *        0 ~ 255 (255 : Max)
	 */
	public void setContentBackColor (int red, int green, int blue, int alpha) {
		String argb = "#" + StringUtil.intToHex(alpha) + StringUtil.intToHex(red) + StringUtil.intToHex(green) + StringUtil.intToHex(blue);
		mResData.put(CONTENT_BACKGROUND_COLOR, argb);
	}

	public String getContentBackColor () {
		return (String) mResData.get(CONTENT_BACKGROUND_COLOR);
	}

	/**
	 * Set Push PopUp Content Layout Background Image
	 *
	 *        Image file name
	 */
	public void setContentBackImgResource (String filename) {
		int resid = mContext.getResources().getIdentifier(filename, "drawable", mStrPackageName);
		mResData.put(CONTENT_BACKGROUND_RES_ID, resid);
	}

	public int getContentBackImgResource () {
		return (Integer) mResData.get(CONTENT_BACKGROUND_RES_ID);
	}

	/**
	 * Set Push Popup Content Text Color
	 * 
	 * @param red
	 *        0 ~ 255 (255 : Max)
	 * @param green
	 *        0 ~ 255 (255 : Max)
	 * @param blue
	 *        0 ~ 255 (255 : Max)
	 */
	public void setContentTextColor (int red, int green, int blue) {
		String rgb = "#" + StringUtil.intToHex(red) + StringUtil.intToHex(green) + StringUtil.intToHex(blue);
		mResData.put(CONTENT_TEXT_COLOR, rgb);
	}

	public String getContentTextColor () {
		return (String) mResData.get(CONTENT_TEXT_COLOR);
	}

	/**
	 * Set Push Popup Content Text Size
	 * 
	 * @param size
	 *        Font Size (Default : 15)
	 */
	public void setContentTextSize (int size) {
		mResData.put(CONTENT_TEXT_SIZE, size);
	}

	public int getContentTextSize () {
		if (mResData.containsKey(CONTENT_TEXT_SIZE)) {
			return (Integer) mResData.get(CONTENT_TEXT_SIZE);
		} else {
			return TEXT_DEFAULT_SIZE;
		}
	}

	// //////////////////////////////////////// BOTTOM LAYOUT SETTING //////////////////////////////////////////////////////////
	/**
	 * Set Push Popup Bottom Layout Background Image
	 * 
	 * @param filename
	 *        image file name
	 */
	public void setBottomBackImgResource (String filename) {
		int resid = mContext.getResources().getIdentifier(filename, "drawable", mStrPackageName);
		mResData.put(BOTTOM_BACKGROUND_RES_ID, resid);
	}

	public int getBottomBackImgResource () {
		return (Integer) mResData.get(BOTTOM_BACKGROUND_RES_ID);
	}

	/**
	 * Set Push Popup Bottom Layout Background Color & Alpha
	 * 
	 * @param red
	 *        0 ~ 255 (255 : Max)
	 * @param green
	 *        0 ~ 255 (255 : Max)
	 * @param blue
	 *        0 ~ 255 (255 : Max)
	 * @param alpha
	 *        0 ~ 255 (255 : Max)
	 */
	public void setBottomBackColor (int red, int green, int blue, int alpha) {
		String argb = "#" + StringUtil.intToHex(alpha) + StringUtil.intToHex(red) + StringUtil.intToHex(green) + StringUtil.intToHex(blue);
		mResData.put(BOTTOM_BACKGROUND_COLOR, argb);
	}

	public String getBottomBackColor () {
		return (String) mResData.get(BOTTOM_BACKGROUND_COLOR);
	}

	/**
	 * Set Push PopUp Bottom Text Button Count
	 * 
	 * @param count
	 *        Button Count (MAX : 2)
	 */
	public void setBottomTextBtnCount (int count) {
		mResData.put(BOTTOM_TEXT_BTN_COUNT, count);
	}

	public int getBottomTextBtnCount () {
		return (Integer) mResData.get(BOTTOM_TEXT_BTN_COUNT);
	}

	/**
	 * Set Push PopUp Bottom Rich Button Count
	 * 
	 * @param count
	 *        Button Count (MAX : 2)
	 */
	public void setBottomRichBtnCount (int count) {
		mResData.put(BOTTOM_RICH_BTN_COUNT, count);
	}

	public int getBottomRichBtnCount () {
		return (Integer) mResData.get(BOTTOM_RICH_BTN_COUNT);
	}

	/**
	 * Set Push PopUp Bottom Button Image
	 * 
	 * @param args
	 *        array image file name
	 */
	public void setBottomBtnImageResource (String... args) {
		if (args.length != 0) {
			int[] ResId = new int[args.length];
			for (int i = 0; i < args.length; i++) {
				ResId[i] = mContext.getResources().getIdentifier(args[i], "drawable", mStrPackageName);
			}
			mResData.put(BOTTOM_BTN_RES_ID, ResId);
		}
	}

	public int[] getBottomBtnImageResource () {
		return (int[]) mResData.get(BOTTOM_BTN_RES_ID);
	}

	/**
	 * Set Push Popup Bottom Button Name ON & OFF
	 * 
	 * @param state
	 *        ON & OFF (true & false)
	 */
	public void setBottomTextViewFlag (Boolean state) {
		mResData.put(BOTTOM_BTN_TEXT_STATE, state);
	}

	public Boolean getBottomTextViewFlag () {
		return (Boolean) mResData.get(BOTTOM_BTN_TEXT_STATE);
	}

	/**
	 * Set Push Popup Bottom Button Text Name
	 * 
	 * @param name
	 *        array button name
	 */
	public void setBottomBtnTextName (String... name) {
		mResData.put(BOTTOM_BTN_TEXT_NAME, name);
	}

	public String[] getBottomBtnTextName () {
		return (String[]) mResData.get(BOTTOM_BTN_TEXT_NAME);
	}

	/**
	 * Set Push Popup Bottom Button Text Color
	 * 
	 * @param red
	 *        0 ~ 255 (255 : Max)
	 * @param green
	 *        0 ~ 255 (255 : Max)
	 * @param blue
	 *        0 ~ 255 (255 : Max)
	 */
	public void setBottomBtnTextColor (int red, int green, int blue) {
		String rgb = "#" + StringUtil.intToHex(red) + StringUtil.intToHex(green) + StringUtil.intToHex(blue);
		mResData.put(BOTTOM_BTN_TEXT_COLOR, rgb);
	}

	public String getBottomBtnTextColor () {
		return (String) mResData.get(BOTTOM_BTN_TEXT_COLOR);
	}

	/**
	 * Set Push Popup Bottom Button Text Size
	 * 
	 * @param size
	 *        Font Size (Default : 15)
	 */
	public void setBottomBtnTextSize (int size) {
		mResData.put(BOTTOM_BTN_TEXT_SIZE, size);
	}

	public int getBottomBtnTextSize () {
		if (mResData.containsKey(BOTTOM_BTN_TEXT_SIZE)) {
			return (Integer) mResData.get(BOTTOM_BTN_TEXT_SIZE);
		} else {
			return TEXT_DEFAULT_SIZE;
		}
	}

	/**
	 * Set Text Push Popup Bottom Button OnclickListener
	 * 
	 * @param ocl
	 *        set array OnclickListener
	 */
	public void setTextBottomBtnClickListener (Object... ocl) {
		mResData.put(BOTTOM_BTN_TEXT_CLICKLISTENER, ocl);
	}

	public btnEventListener getTextBottomBtnClickListener (int count) {
		btnEventListener[] ocl = null;
		if (mResData.containsKey(BOTTOM_BTN_TEXT_CLICKLISTENER)) {
			ocl = (btnEventListener[]) mResData.get(BOTTOM_BTN_TEXT_CLICKLISTENER);
			return ocl[count];
		} else {
			return null;
		}
	}

	/**
	 * Set Push Popup Bottom Button OnclickListener
	 * 
	 * @param ocl
	 *        set array OnclickListener
	 */
	public void setRichBottomBtnClickListener (Object... ocl) {
		mResData.put(BOTTOM_BTN_RICH_CLICKLISTENER, ocl);
	}

	public btnEventListener getRichBottomBtnClickListener (int count) {
		btnEventListener[] ocl = null;
		if (mResData.containsKey(BOTTOM_BTN_RICH_CLICKLISTENER)) {
			ocl = (btnEventListener[]) mResData.get(BOTTOM_BTN_RICH_CLICKLISTENER);
			return ocl[count];
		} else {
			return null;
		}
	}

	// ////////////////////////////////////////XML LAYOUT SETTING //////////////////////////////////////////////////////////
	/**
	 * Set Push Popup XML Layout
	 * 
	 * @param filename
	 *        XML File Name
	 */
	public void setLayoutXMLTextResId (String filename) {
		int resid = mContext.getResources().getIdentifier(filename, "layout", mStrPackageName);
		mResData.put(XML_LAYOUT_TEXT_RES_ID, resid);
	}

	public int getLayoutXMLTextResId () {
		return (Integer) mResData.get(XML_LAYOUT_TEXT_RES_ID);
	}

	/**
	 * Set Push Popup XML Layout
	 * 
	 * @param filename
	 *        XML File Name
	 */
	public void setLayoutXMLRichResId (String filename) {
		int resid = mContext.getResources().getIdentifier(filename, "layout", mStrPackageName);
		mResData.put(XML_LAYOUT_RICH_RES_ID, resid);
	}

	public int getLayoutXMLRichResId () {
		return (Integer) mResData.get(XML_LAYOUT_RICH_RES_ID);
	}

	/**
	 * Set XML Text Push Button Type
	 * 
	 * @param viewtype
	 *        set array Resource Type
	 */
	public void setXMLTextButtonType (String... viewtype) {
		mResData.put(XML_TEXT_BUTTON_TYPE, viewtype);
	}

	public String[] getXMLTextButtonType () {
		return (String[]) mResData.get(XML_TEXT_BUTTON_TYPE);
	}

	/**
	 * Set XML Text Push Button Tag Name
	 * 
	 * @param name
	 *        set array Resource Tag Name
	 */
	public void setXMLTextButtonTagName (String... name) {
		mResData.put(XML_TEXT_BUTTON_TAG_NAME, name);
	}

	public String[] getXMLTextButtonTagName () {
		return (String[]) mResData.get(XML_TEXT_BUTTON_TAG_NAME);
	}

	/**
	 * Set XML Rich Push Button Type
	 * 
	 * @param viewtype
	 *        set array Resource Type
	 */
	public void setXMLRichButtonType (String... viewtype) {
		mResData.put(XML_RICH_BUTTON_TYPE, viewtype);
	}

	public String[] getXMLRichButtonType () {
		return (String[]) mResData.get(XML_RICH_BUTTON_TYPE);
	}

	/**
	 * Set XML Rich Push Button Tag Name
	 * 
	 * @param name
	 *        set array Resource Tag Name
	 */
	public void setXMLRichButtonTagName (String... name) {
		mResData.put(XML_RICH_BUTTON_TAG_NAME, name);
	}

	public String[] getXMLRichButtonTagName () {
		return (String[]) mResData.get(XML_RICH_BUTTON_TAG_NAME);
	}

	/**
	 * Set XML File ON & OFF
	 * 
	 * @param flag
	 *        ON & OFF (true & false)
	 */
	public void setXmlAndDefaultFlag (Boolean flag) {
		mResData.put(XML_AND_DEFAULT_FLAG, flag);
	}

	public Boolean getXmlAndDefaultFlag () {
		return (Boolean) mResData.get(XML_AND_DEFAULT_FLAG);
	}

	// //////////////////////////////////////// OTHER SETTING //////////////////////////////////////////////////////////
	public void setActivity (Activity a) {
		mResData.put(POPUP_ACTIVITY, a);
	}

	public Activity getActivity () {
		return (Activity) mResData.get(POPUP_ACTIVITY);
	}

	public void setMsgId (String id) {
		mResData.put(PREF_READ_LIST, id);
	}

	public String getMsgId () {
		return (String) mResData.get(PREF_READ_LIST);
	}

	public View getSerachView (View view, int state, String serachName) {
		ArrayList<View> allViewsWithinMyTopView = (ArrayList<View>) getAllChildrenBFS(view);
		boolean found = false;
		int count = 0;

		for (View child : allViewsWithinMyTopView) {
			switch (state) {
				case TEXTVIEW:
					if (child instanceof TextView) {
						found = getMatchString(child, serachName);
					}
					break;
				case LINEARLAYOUT:
					if (child instanceof LinearLayout) {
						found = getMatchString(child, serachName);
					}
					break;
				case WEBVIEW:
					if (child instanceof WebView) {
						found = getMatchString(child, serachName);
					}
					break;
				case PROGRESSBAR:
					if (child instanceof ProgressBar) {
						found = getMatchString(child, serachName);
					}
					break;
				case IMAGEVIEW:
					if (child instanceof ImageView) {
						found = getMatchString(child, serachName);
					}
					break;
				case BUTTON_VIEW:
					if (child instanceof Button) {
						found = getMatchString(child, serachName);
					}
					break;
				case IMAGE_BUTTON:
					if (child instanceof ImageButton) {
						found = getMatchString(child, serachName);
					}
					break;
				case RELATIVELAYOUT:
					if (child instanceof RelativeLayout) {
						found = getMatchString(child, serachName);
					}
					break;
			}

			if (found) {
				break;
			} else {
				count++;
			}
		}

		return allViewsWithinMyTopView.get(count);
	}

	@SuppressLint("DefaultLocale")
	public int getViewType (String type) {
		CLog.e(type);
		int nType = 0;
		if (type.toLowerCase().equals("textview")) {
			nType = TEXTVIEW;
		} else if (type.toLowerCase().equals("linearlayout")) {
			nType = LINEARLAYOUT;
		} else if (type.toLowerCase().equals("webview")) {
			nType = WEBVIEW;
		} else if (type.toLowerCase().equals("progressbar")) {
			nType = PROGRESSBAR;
		} else if (type.toLowerCase().equals("imageview")) {
			nType = IMAGEVIEW;
		} else if (type.toLowerCase().equals("button")) {
			nType = BUTTON_VIEW;
		} else if (type.toLowerCase().equals("imagebutton")) {
			nType = IMAGE_BUTTON;
		} else if (type.toLowerCase().equals("relativelayout")) {
			nType = RELATIVELAYOUT;
		}
		return nType;
	}

	private List<View> getAllChildrenBFS (View v) {
		List<View> visited = new ArrayList<View>();
		List<View> unvisited = new ArrayList<View>();
		unvisited.add(v);

		while (!unvisited.isEmpty()) {
			View child = unvisited.remove(0);
			visited.add(child);
			if (!(child instanceof ViewGroup)) {
				continue;
			}

			ViewGroup group = (ViewGroup) child;
			final int childCount = group.getChildCount();
			for (int i = 0; i < childCount; i++) {
				unvisited.add(group.getChildAt(i));
			}
		}

		return visited;
	}

	private Boolean getMatchString (View view, String searchName) {
		Boolean state = false;
		if (view.getTag() != null) {
			if (view.getTag().toString().equals(searchName)) {
				state = true;
			}
		}

		return state;
	}

	public interface btnEventListener extends Serializable {
		public void response ();
	}
}

class SerializableClass implements Serializable {
	private static final long serialVersionUID = 1L;

	public HashMap<String, Object> mResData = new HashMap<String, Object>();

	// 생성자
	public SerializableClass(HashMap<String, Object> data) {
		mResData = data;
	}
}