package com.pms.sdk.common.util;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.os.Build;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Display;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.UUID;

import androidx.core.content.ContextCompat;

/**
 * @author erzisk get Phonestate
 * @see get3GState<br>
 * getWifiState<br>
 * isAvailablePush<br>
 * getOsVersion<br>
 * getAppVersion<br>
 * getDeviceName<br>
 * getDeviceId<br>
 * getGlobalDeviceToken<br>
 * getLocalIpAddress<br>
 * getPhoneNumber<br>
 * getTabletFlag
 */
public class PhoneState {
    public static final int DEVICE_LIMIT = 21;
    public static final String ANDROID_ID_BUGID = "9774d56d682e549c";
    private static final boolean IS_NEW_UUID = true;
    //20180717 자로 봤을때 이전에 generateUUID를 사용하도록 이미 배포된듯함
    /**
     * return 3g state of device
     *
     * @param context
     * @return
     */
    public static boolean get3GState(final Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting();
    }

    /**
     * return wifi state of device
     *
     * @param context
     * @return
     */
    public static boolean getWifiState(final Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();
    }

    /**
     * is available push
     *
     * @return
     */
    public static boolean isAvailablePush() {
        try {
            int ver = Build.VERSION.SDK_INT;
            if (ver >= Build.VERSION_CODES.ECLAIR_MR1) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            return false;
        }
    }

    /**
     * is notification new style
     *
     * @return
     */
    public static boolean isNotificationNewStyle() {
        try {
            int ver = Build.VERSION.SDK_INT;
            if (ver >= Build.VERSION_CODES.JELLY_BEAN) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * return os version
     *
     * @return
     */
    @SuppressWarnings("static-access")
    public static String getOsVersion() {
        try {
            return new Build.VERSION().RELEASE;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * return app version
     *
     * @param context
     * @return
     */
    public static String getAppVersion(final Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_META_DATA).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "unknown";
        }
    }

    /**
     * return device name
     *
     * @return
     */
    public static String getDeviceName() {
        return android.os.Build.MODEL;
    }

    /**
     * return device id (uuid)
     *
     * @param context
     * @return
     */
    public static String getDeviceId(final Context context) {
        String deviceId = null;
        try {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            deviceId = tm.getDeviceId();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        return deviceId;
    }
    /**
     * return global device token
     *
     * @param context
     * @return
     */
    public static String getGlobalDeviceToken (final Context context) {
//      TODO : 사용안함 없어져야되는 함수
//      UUID 변경여부를 확인 하기 위해 createDeviceToken으로 사용됨
        try {
            String strOldUUID = PMSUtil.getUUID(context);

            if (strOldUUID.trim().length() > 1)
                return strOldUUID;

            String enable_SDK_UUID = PMSUtil.getEnableUUIDFlag(context);
            if (enable_SDK_UUID.equals("Y") || enable_SDK_UUID.isEmpty()) {
                String strNewUUID = generateUUID(context);

                if (!strOldUUID.equals(strNewUUID)) {
                    CLog.i("ChangeNewUUID : " + strNewUUID);
                    PMSUtil.setUUID(context, strNewUUID);
                }

                return strNewUUID;
            }

            return "";

        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }


    public static boolean createDeviceToken(final Context context) {
        String strOldUUID = PMSUtil.getUUID(context);
        String strNewUUID;
        if (strOldUUID.trim().length() > 1) {
            CLog.i("strOldUUID is exists, strOldUUID : " + strOldUUID);
            return false;
        }

        String enable_SDK_UUID = PMSUtil.getEnableUUIDFlag(context);
        CLog.i(String.format("createDeviceToken enable_SDK_UUID : %s / %s", enable_SDK_UUID, "Y"));
        if (enable_SDK_UUID.equals("Y") || enable_SDK_UUID.isEmpty()) {

            if (IS_NEW_UUID) {
                strNewUUID = generateUUID(context);
            }
            else {
                strNewUUID = getStringUUID(context);
            }


            if (strOldUUID.equals(strNewUUID)) {
                return false;
            }
            CLog.i("ChangeNewUUID : " + strNewUUID);
            PMSUtil.setUUID(context, strNewUUID);

            return true;
        }

        return false;
    }


    private static String getStringUUID(Context context) {
        final String tmDevice, tmSerial, androidId;

        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);

        UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());

        int diff = 36 - deviceUuid.toString().length();

        StringBuilder sb = new StringBuilder();
        sb.append(deviceUuid.toString());

        for (int i = 0; i < diff; i++) {
            sb.append("0");
        }

        return sb.toString();
    }

    private static String generateUUID(Context context) {
        final String androidId = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
        StringBuilder sb = new StringBuilder();
        try {
            UUID uuid = null;
            if (!"9774d56d682e549c".equals(androidId)) {
                uuid = UUID.nameUUIDFromBytes(androidId.getBytes("utf8"));
            } else {

                CLog.w("Not Support Devices Create androidID");
                int ver = Build.VERSION.SDK_INT;
                if (ver <= Build.VERSION_CODES.N_MR1) {
                    String deviceName = "" + getDeviceName();
                    String deviceSerial = "" + getDeviceSerialNumber();
                    uuid = new UUID(androidId.hashCode(), ((long) deviceSerial.hashCode() << 32) | deviceName.hashCode());
                } else {
                    int permissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE);
                    if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                        CLog.i("PERMISSION_GRANTED");

                        return getStringUUID(context);
                    }
                    else {
                        CLog.i("PERMISSION_DENIED");
                        CLog.e("Not Support Devices Create UUID");
                    }
                }
            }

            if (uuid == null)
                return "";

            int diff = 36 - uuid.toString().length();
            sb.append(uuid.toString());

            for (int i = 0; i < diff; i++) {
                sb.append("0");
            }
            return sb.toString();
        } catch (UnsupportedEncodingException e) {
            e.getStackTrace();
            return "";
        }
    }

    @SuppressLint("HardwareIds")
    private static String getDeviceSerialNumber() {
        return Build.SERIAL.equals(Build.UNKNOWN) ? "" : Build.SERIAL;
    }

    /**
     * return local(device) ip address
     *
     * @return
     * @throws SocketException
     */
    public static String getLocalIpAddress() throws SocketException {
        Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
        while (en.hasMoreElements()) {
            NetworkInterface interf = en.nextElement();
            Enumeration<InetAddress> ips = interf.getInetAddresses();
            while (ips.hasMoreElements()) {
                InetAddress inetAddress = ips.nextElement();
                if (!inetAddress.isLoopbackAddress()) {
                    return inetAddress.getHostAddress().toString();
                }
            }
        }
        return null;
    }

    /**
     * get phone number
     *
     * @param c
     * @return
     */
    public static String getPhoneNumber(Context c) {
        try {
            TelephonyManager tm = (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);
            return tm.getLine1Number().replace("+82", "0").replace("-", "");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * return tablet flag
     *
     * @param c
     * @return
     */
    public static boolean getTabletFlag(Context c) {
        boolean xlarge = ((c.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
        boolean large = ((c.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return (xlarge || large);

    }

    /**
     * get device size
     *
     * @param a
     * @return
     */
    public static int[] getDeviceSize(Activity a) {
        Display display = a.getWindowManager().getDefaultDisplay();
        return new int[]{display.getWidth(), display.getHeight()};
    }

    /**
     * get device size
     *
     * @param c
     * @return
     */
    public static int[] getDeviceSize(Context c) {
        DisplayMetrics metrics = c.getResources().getDisplayMetrics();
        float width = Float.parseFloat(metrics.widthPixels + "") / metrics.density;
        float height = Float.parseFloat(metrics.heightPixels + "") / metrics.density;
        return new int[]{(int) width, (int) height};
    }
}
