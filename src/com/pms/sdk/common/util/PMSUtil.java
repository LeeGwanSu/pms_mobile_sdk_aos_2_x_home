package com.pms.sdk.common.util;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.PowerManager;
import android.text.TextUtils;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.PMS;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author erzisk
 * @since 2013.06.26
 */
public class PMSUtil implements IPMSConsts {

	/**
	 * set mqtt flag
	 * 
	 * @param context
	 * @param mqttFlag
	 */
	public static void setMQTTFlag (final Context context, boolean mqttFlag) {
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_MQTT_FLAG, mqttFlag ? FLAG_Y : FLAG_N);
	}

	/**
	 * get mqtt flag
	 * 
	 * @param context
	 * @return
	 */
	public static String getMQTTFlag (final Context context) {
		try {
			Prefs prefs = new Prefs(context);
			String mqttFlag = (String) context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
					.get("PMS_MQTT_FLAG").toString().trim();
			return mqttFlag == null || mqttFlag.equals("") ? prefs.getString(PREF_MQTT_FLAG) : mqttFlag;
		} catch (Exception e) {
			e.printStackTrace();
			return FLAG_N;
		}
	}

	/**
	 * get private flag
	 * 
	 * @param context
	 * @return
	 */
	public static String getPrivateFlag (final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_PRIVATE_FLAG);
	}

	/**
	 * get Private Log Flag
	 * 
	 * @param context
	 * @return
	 */
	public static String getPrivateLogFlag (final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_PRIVATE_LOG_FLAG);
	}

	/**
	 * get mqtt server url
	 * 
	 * @param context
	 * @return
	 */
	public static String getMQTTServerUrl (final Context context) {
		try {
			String mqttServerUrl = "";
			if (getPrivateProtocol(context).equals(PROTOCOL_SSL)) {
				mqttServerUrl = (String) context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
						.get("PMS_MQTT_SERVER_URL_SSL").toString().trim();
			} else {
				mqttServerUrl = (String) context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
						.get("PMS_MQTT_SERVER_URL_TCP").toString().trim();
			}
			return mqttServerUrl == null || mqttServerUrl.equals("") ? "" : mqttServerUrl;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String getSignKey(final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_SSL_SIGN_KEY);
	}

	public static String getSignPass (final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_SSL_SIGN_PASS);
	}

	/**
	 * get Private Log Flag
	 * 
	 * @param context
	 * @return
	 */
	public static String getPrivateProtocol (final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_PRIVATE_PROTOCOL);
	}

	/**
	 * get mqtt server url
	 * 
	 * @param context
	 * @return
	 */
	public static String getMQTTServerKeepAlive (final Context context) {
		try {
			String keepalive = String
					.valueOf(context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
							.getInt("PMS_MQTT_SERVER_KEEPALIVE")).toString().trim();
			return keepalive == null || keepalive.equals("") ? "400" : keepalive;
		} catch (Exception e) {
			e.printStackTrace();
			return "400";
		}
	}

	/**
	 * return application key
	 * 
	 * @param context
	 * @return
	 */
	public static String getApplicationKey (final Context context) {
		try {
			return (String) context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
					.get("PMS_APP_KEY").toString().trim();
		} catch (NameNotFoundException e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String getBigNotiContextMsg (final Context context) {
		try {
			return (String) context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
					.get("PMS_NOTI_CONTENT").toString().trim();
		} catch (NameNotFoundException e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String getPushPopupActivity (final Context context) {
		try {
			return (String) context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
					.get("PMS_DEFAULT_POPUP_ACTIVITY").toString().trim();
		} catch (NameNotFoundException e) {
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * return server url <br>
	 * AndroidManifest.xml에 SERVER_URL이 셋팅되어 있지 않거나, <br>
	 * meta data를 불러오는중 exception이 발생하면 <br>
	 * APIVariable.SERVER_URL을 return
	 * 
	 * @param context
	 * @return
	 */
	public static String getServerUrl (final Context context) {
		try {
			Prefs prefs = new Prefs(context);
			String serverUrl = "";
			if (FLAG_Y.equals(prefs.getString("pref_api_server_check"))) {
				serverUrl = prefs.getString(PREF_SERVER_URL);
			} else {
				serverUrl = (String) context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
						.get("PMS_API_SERVER_URL").toString().trim();
			}

			return serverUrl == null || serverUrl.equals("") ? API_SERVER_URL : serverUrl;
		} catch (Exception e) {
			e.printStackTrace();
			return API_SERVER_URL;
		}
	}

	public static int getIconId (final Context context) {
		try {
			int iconResId = (Integer) context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
					.get("PMS_SET_ICON");

			return iconResId;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public static int getLargeIconId (final Context context) {
		try {
			int iconResId = (Integer) context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
					.get("PMS_SET_LARGE_ICON");

			return iconResId;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public static int getChangeLargeIconId (final Context context) {
		try {
			Prefs prefs = new Prefs(context);
			return prefs.getInt(PREF_LARGE_NOTI_ICON);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	/**
	 * set server url
	 * 
	 * @param context
	 * @param serverUrl
	 */
	public static void setServerUrl (final Context context, String serverUrl) {
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_SERVER_URL, serverUrl);
	}

	/**
	 * set gcm project id
	 * 
	 * @param context
	 * @param projectId
	 */
	public static void setGCMProjectId (final Context context, String projectId) {
		Prefs prefs = new Prefs(context);
		prefs.putString(KEY_GCM_PROJECT_ID, projectId);
	}

	/**
	 * get gcm project id
	 * 
	 * @param context
	 * @return
	 */
	public static String getGCMProjectId (final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(KEY_GCM_PROJECT_ID);
	}

	/**
	 * set gcm token
	 * 
	 * @param context
	 * @param gcmToken
	 */
	public static void setGCMToken (final Context context, String gcmToken) {
		Prefs prefs = new Prefs(context);
		prefs.putString(KEY_GCM_TOKEN, gcmToken);
	}

	/**
	 * return gcm token (push)
	 * 
	 * @param context
	 * @return
	 */
	public static String getGCMToken (final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(KEY_GCM_TOKEN);
	}

	/**
	 * get createtoken flag
	 *
	 * @param context
	 * @return
	 */
	public static String getEnableUUIDFlag(final Context context) {
		try {
			String uuidFlag = (String) context.getPackageManager().getApplicationInfo(
					context.getPackageName(), PackageManager.GET_META_DATA).metaData
					.get("PMS_ENABLE_UUID").toString().trim();
			return (uuidFlag == null) ? FLAG_Y : uuidFlag;
		} catch (Exception e) {
			e.printStackTrace();
			return FLAG_N;
		}
	}

	/**
	 * set UUID
	 *
	 * @param context
	 * @param gcmToken
	 */
	public static void setUUID (final Context context, String gcmToken) {
		// 20170811 fixed by hklim
		// UUID 저장 시에 Shared Preference 를 사용하지 않고 DB 사용 하도록 수정됨
		DataKeyUtil.setDBKey(context, DB_UUID, gcmToken);
	}

	/**
	 * get UUID
	 *
	 * @param context
	 * @return
	 */
	public static String getUUID (final Context context) {
		String strReturn = DataKeyUtil.getDBKey(context, DB_UUID);
		return strReturn;
	}


	/**
	 * set encrypt key
	 * 
	 * @param context
	 * @param encKey
	 */
	public static void setEncKey (final Context context, String encKey) {
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_ENC_KEY, encKey);
	}

	/**
	 * get encrypt key
	 * 
	 * @param context
	 * @return
	 */
	public static String getEncKey (final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_ENC_KEY);
	}

	/**
	 * set appUserId
	 * 
	 * @param context
	 * @param appUserId
	 */
	public static void setAppUserId (final Context context, String appUserId) {
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_APP_USER_ID, appUserId);
	}

	/**
	 * get appUserId
	 * 
	 * @param context
	 * @return
	 */
	public static String getAppUserId (final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_APP_USER_ID);
	}

	/**
	 * set cust id
	 * 
	 * @param context
	 * @param custId
	 */
	public static void setCustId (final Context context, String custId) {
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_CUST_ID, custId);
	}

	/**
	 * get cust id
	 * 
	 * @param context
	 * @return
	 */
	public static String getCustId (final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_CUST_ID);
	}

	public static void setPopupActivity (Context context, Boolean state) {
		Prefs prefs = new Prefs(context);
		prefs.putBoolean(PREF_ISPOPUP_ACTIVITY, state);
	}

	public static Boolean getPopupActivity (Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getBoolean(PREF_ISPOPUP_ACTIVITY);
	}

	public static void setNotiOrPopup (Context context, Boolean isNotiPopup) {
		Prefs prefs = new Prefs(context);
		prefs.putBoolean(PREF_NOTIORPOPUP_SETTING, isNotiPopup);
	}

	public static Boolean getNotiOrPopup (Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getBoolean(PREF_NOTIORPOPUP_SETTING);
	}

	/**
	 * set DeviceCert Status
	 * 
	 * @param context
	 * @param status
	 */
	public static void setDeviceCertStatus (final Context context, String status) {
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_DEVICECERT_STATUS, status);
	}

	/**
	 * get DeviceCert Status
	 * 
	 * @param context
	 * @return
	 */
	public static String getDeviceCertStatus (final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_DEVICECERT_STATUS);
	}

	/**
	 * get Link From html
	 * 
	 * @param htmlStr
	 * @return
	 */
	public static Map<Integer, String> getLinkFromHtml (String htmlStr) {
		Map<Integer, String> map = new HashMap<Integer, String>();
		Pattern p = Pattern.compile("<a\\s*href\\s*=\\s*([\"']*)([^\"'>\\s]+)\\1(.*?)>(.*?)</a>");
		Matcher m = p.matcher(htmlStr);
		for (int i = 1; m.find(); i++) {
			map.put(i, m.group(2));
		}
		return map;
	}

	/**
	 * replaceLink
	 * 
	 * @param htmlStr
	 * @return
	 */
	public static String replaceLink (String htmlStr) {
		StringBuffer s = new StringBuffer();
		Pattern p = Pattern.compile("<a\\s*href\\s*=\\s*([\"']*)([^\"'>\\s]+)\\1(.*?)>(.*?)</a>");
		Matcher m = p.matcher(htmlStr);
		for (int i = 1; m.find(); i++) {
			m.appendReplacement(s, m.group(0).replace(m.group(2), "click://" + i));
		}
		m.appendTail(s);
		return s.toString();
	}

	/**
	 * find top activity
	 * 
	 * @return
	 */
	public static String[] findTopActivity (Context c) {
		String[] top = new String[2];
		ActivityManager activityManager = (ActivityManager) c.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> info;
		info = activityManager.getRunningTasks(1);
		for (Iterator<RunningTaskInfo> iterator = info.iterator(); iterator.hasNext();) {
			RunningTaskInfo runningTaskInfo = iterator.next();

			top[0] = runningTaskInfo.topActivity.getPackageName();
			top[1] = runningTaskInfo.topActivity.getClassName();
		}

		return top;
	}

	/**
	 * runnedApp 현재 어플리케이션이 실행중인지 여부
	 * 
	 * @return
	 */
	public static boolean isRunnedApp (Context c) {
		if (findTopActivity(c)[0].equals(c.getPackageName())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * is screen on
	 * 
	 * @param c
	 * @return
	 */
	public static boolean isScreenOn (Context c) {
		PowerManager pm;
		try {
			pm = (PowerManager) c.getSystemService(Context.POWER_SERVICE);
			return pm.isScreenOn();
		} catch (Exception e) {
			return false;
		} finally {
			PMS.clear();
		}
	}

	public static JSONObject getReadParam (String msgId) {
		JSONObject read = new JSONObject();
		try {
			read.put("msgId", msgId);
			read.put("workday", DateUtil.getNowDate());
			return read;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * array to preferences
	 * 
	 * @param obj
	 */
	public static void arrayToPrefs (Context c, String key, Object obj) {
		Prefs prefs = new Prefs(c);
		String arrayString = prefs.getString(key);
		JSONArray array = null;
		try {
			if ("".equals(arrayString)) {
				array = new JSONArray();
			} else {
				array = new JSONArray(arrayString);
			}
			array.put(obj);
			prefs.putString(key, array.toString());
		} catch (Exception e) {
			// e.printStackTrace();
		}
	}

	/**
	 * array from preferences
	 * 
	 * @param c
	 * @param key
	 * @return
	 */
	public static JSONArray arrayFromPrefs (Context c, String key) {
		Prefs prefs = new Prefs(c);
		String arrayString = prefs.getString(key);
		JSONArray array = new JSONArray();
		try {
			array = new JSONArray(arrayString);
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return array;
	}

	public static String getApplicationName(Context context) {
		PackageManager packageManager = context.getPackageManager();
		ApplicationInfo applicationInfo = null;
		try {
			applicationInfo = packageManager.getApplicationInfo(context.getApplicationInfo().packageName, 0);
		}
		catch (final NameNotFoundException e) {
		}
		finally {
			return (String) (applicationInfo != null ? packageManager.getApplicationLabel(applicationInfo) : context.getPackageName());
		}
	}

	public static String getMQTTPollingInterval(final Context context) {
		try {
			Prefs prefs = new Prefs(context);
			String keepalive = prefs.getString(IPMSConsts.PREF_MQTT_POLLING_INTERVAL);
			return keepalive == null || keepalive.equals("") ? "1800" : keepalive;
		} catch (Exception e) {
			e.printStackTrace();
			return "1800";
		}
	}

	public static String getMQTTPollingFlag(final Context context) {
		try {
			Prefs prefs = new Prefs(context);
			String MqttPollingflag = prefs.getString(IPMSConsts.PREF_MQTT_POLLING_FLAG);
			return MqttPollingflag == null || MqttPollingflag.equals("") ? FLAG_N : MqttPollingflag;
		} catch (Exception e) {
			e.printStackTrace();
			return FLAG_N;
		}
	}

	public static String getMQTTAlarmFlag(final Context context) {
		try {
			Prefs prefs = new Prefs(context);
			String MqttAlarmflag = prefs.getString(IPMSConsts.PREF_MQTT_ALARM_FLAG);
			return MqttAlarmflag == null || MqttAlarmflag.equals("") ? FLAG_N : MqttAlarmflag;
		} catch (Exception e) {
			e.printStackTrace();
			return FLAG_N;
		}
	}

	public static void setMQTTPollingInterval(final Context context, final String interval) {
		Prefs prefs = new Prefs(context);
		prefs.putString(IPMSConsts.PREF_MQTT_POLLING_INTERVAL, interval);
	}

	public static void setMQTTPollingFlag(final Context context, final String flag) {
		Prefs prefs = new Prefs(context);
		prefs.putString(IPMSConsts.PREF_MQTT_POLLING_FLAG, flag);
	}
	public static void setMQTTAlarmFlag(final Context context, boolean flag) {
		Prefs prefs = new Prefs(context);
		prefs.putString(IPMSConsts.PREF_MQTT_ALARM_FLAG, flag ? FLAG_Y : FLAG_N);
	}
	public static int getTargetVersion(final Context context) {
		try {
			int numTargetVersion = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).targetSdkVersion;
			return numTargetVersion;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	public static void setMqttWakeupTime(Context context, int time)
	{
		Prefs prefs = new Prefs(context);
		prefs.putInt(IPMSConsts.PREF_MQTT_WAKEUP_TIME, time);
	}
	public static int getMqttWakeupTime(Context context)
	{
		Prefs prefs = new Prefs(context);
		return prefs.getInt(IPMSConsts.PREF_MQTT_WAKEUP_TIME);
	}

	public static void setNotificationToStackable(Context context, boolean value)
	{
		DataKeyUtil.setDBKey(context, DB_NOTIFICATION_STACKABLE, value ? FLAG_Y : FLAG_N);
	}
	public static boolean isNotificationToStackable(Context context)
	{
		String value = DataKeyUtil.getDBKey(context, DB_NOTIFICATION_STACKABLE);
		boolean result = false;
		if(TextUtils.isEmpty(value)) {
			return result;
		}
		result = FLAG_Y.equals(value);
		return result;
	}
}
