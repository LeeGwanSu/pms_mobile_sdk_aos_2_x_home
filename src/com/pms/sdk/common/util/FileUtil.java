package com.pms.sdk.common.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;

public class FileUtil {

	private static final String SD_CARD_ROOT = "/sdcard/";

	/**
	 * write to sd card
	 * 
	 * @param dir
	 * @param fileName
	 * @param data
	 * @throws IOException
	 */
	public void writeToSdCard (String dir, String fileName, String data) throws Exception {
		if (new File(SD_CARD_ROOT + dir).mkdir()) {
		} else {
		}

		File file = new File(SD_CARD_ROOT + dir, fileName);
		if (!file.exists()) {
			file.createNewFile();
		} else {
			long fileSize = file.length();
			if (fileSize >= 10000000) { // 10MB
				file.delete();
				file.createNewFile();
			}
		}

		FileWriter fw = new FileWriter(file, true);
		BufferedWriter bufferWritter = new BufferedWriter(fw);
		bufferWritter.write(new String(data.getBytes("UTF-8"), "UTF-8"));
		bufferWritter.close();
	}

	/**
	 * read from sd card
	 * 
	 * @param dir
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	public String readFromSdCard (String dir, String fileName) throws Exception {
		StringBuilder sb = new StringBuilder();

		BufferedReader br = new BufferedReader(new FileReader(SD_CARD_ROOT + dir + fileName));

		String line;
		while ((line = br.readLine()) != null) {
			sb.append(line);
			sb.append("\n");
		}

		br.close();
		return sb.toString();
	}

	/**
	 * get file name list
	 * 
	 * @param dir
	 * @param search
	 * @return
	 */
	public List<String> getFileNameListFromSdCard (String dir, String search) {
		List<String> filePathList = new ArrayList<String>();
		File directory = new File(SD_CARD_ROOT + dir);
		if (directory.exists()) {
			for (File file : directory.listFiles()) {
				if (search != null && !"".equals(search)) {
					if (file.getName().indexOf(search) > -1) {
						filePathList.add(file.getName());
					}
				} else {
					filePathList.add(file.getName());
				}
			}
		}
		return filePathList;
	}

	@SuppressWarnings("static-access")
	public void getConnectLogUtil (Context context, int connecttype, Object... args) throws Exception {
		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "Tag");

		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
		StringBuilder data = new StringBuilder(1024);
		data.append("[" + DateUtil.convertDate(DateUtil.getNowDate(), "yyyyMMddkkmmss", "yyyy/MM/dd kk:mm:ss") + "]");

		switch (connecttype) {
			case 1: // Connect
				data.append(String.format("[mqttServer::Connect] Succsess Connect ClientID -> %s, KeepAliveTime -> %ss\n", args[0], args[1]));
				break;

			case 2: // ConnectionLost
				data.append(String.format("[mqttServer::ConnectionLost] "
						+ "pm.isScreenOn():%s, wl.isHeld():%s, cm.getActiveNetworkInfo().isAvailable():%s, "
						+ "cm.getActiveNetworkInfo().isConnected():%s, 3g:%s, wifi:%s\n", pm.isScreenOn() + "", wl.isHeld() + "", getNetworkType(cm)
						+ "", getNetworkIsConnected(cm) + "", cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting() + "", cm
						.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting() + ""));
				break;

			case 3: // Reconnect
				data.append(String.format("[mqttServer::Reconnect] " + "Rescheduling connection in %dms, Interval Count -> %d Exception -> %s\n",
						args[0], args[1], args[2]));
				break;

			case 4: // ConnectChange
				data.append(String.format("[mqttServer::ConnectChange] Connectivity changed: connected = %s\n", args[0]));
				break;

			case 5: // RestartReceiver
				data.append(String.format("[mqttServer::RestartReceiver] Intent Action -> %s\n", args[0]));
				break;

			case 6: // Disconnect
				data.append(String.format("[mqttServer::Disconnect] MQTT Client Disconnect\n"));
				break;
			case 7: // KeepAlive
				data.append(String.format("[mqttServer::KeepAlive] MQTT KeepAlive Client ID -> %s, Count ->  %s\n", args[0], args[1]));
				break;
		}
		String logFlag = (String) context.getPackageManager().getApplicationInfo(
				context.getPackageName(), PackageManager.GET_META_DATA).metaData.get("PMS_FILE_WRITE_LOG_FLAG");
		if ("Y".equals(logFlag))
			writeToSdCard("pms_file", "HomeShop_Log.txt", data.toString());

		data.delete(0, data.length());
	}

	private Boolean getNetworkType (ConnectivityManager cm) {
		try {
			return cm.getActiveNetworkInfo().isAvailable();
		} catch (NullPointerException e) {
			return false;
		}
	}

	private Boolean getNetworkIsConnected (ConnectivityManager cm) {
		try {
			return cm.getActiveNetworkInfo().isConnected();
		} catch (NullPointerException e) {
			return false;
		}
	}
}
