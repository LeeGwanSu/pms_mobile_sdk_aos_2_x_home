package com.pms.sdk.common.util;

import android.content.Context;
import android.database.Cursor;

import com.pms.sdk.bean.Data;
import com.pms.sdk.db.PMSDB;

public class DataKeyUtil {

	public static void setDBKey (Context con, String key, String value) {
		PMSDB db = PMSDB.getInstance(con);
		int size = db.selectKeyData(key);
		if (size > 0) {
			Cursor c = db.selectKey(key);
			c.moveToFirst();
			Data data = new Data(c);
			if (value.equals(data.value) == false) {
				db.updateDataValue(key, value);
			}
			if(c!=null && !c.isClosed())
			{
				c.close();
			}
		} else {
			Data data = new Data();
			data.key = key;
			data.value = value;
			db.insertDataValue(data);
		}
	}

	public static String getDBKey (Context con, String key) {
		PMSDB db = PMSDB.getInstance(con);
		Cursor c = db.selectKey(key);
		try {
			c.moveToFirst();
			Data data = new Data(c);
			return data.value;
		} catch (Exception e) {
			return "";
		} finally {
			if(c!=null && !c.isClosed())
			{
				c.close();
			}
		}
	}
}
