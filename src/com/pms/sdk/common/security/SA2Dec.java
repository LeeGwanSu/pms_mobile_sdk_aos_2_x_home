package com.pms.sdk.common.security;

import com.pms.sdk.common.compress.CZip;

import java.lang.ref.WeakReference;

public class SA2Dec {

	@SuppressWarnings("unused")
	private static final String _USER_KEY = "amailRND2009";

	public static final int KEY_LENGTH = 16;
	public static final int KEY_START_INDEX = 17;

	public String decrypt (String str) {
		return decrypt(str, _USER_KEY);
	}

	public String decrypt (String str, String userKey) {

		if (str == null || userKey == null || str.length() < KEY_LENGTH + KEY_START_INDEX)
			return null;
		WeakReference<CZip> cZipWeakReference = new WeakReference<CZip>(new CZip());
		try {

			byte[] decStr = userKey == null ? Seed.seedDecrypt(Base64.decode(str)) : Seed.seedDecrypt(Base64.decode(str), userKey.getBytes());
			if(cZipWeakReference.get() != null)
			{
				CZip cZip = cZipWeakReference.get();
				return cZip.unzipStringFromBytes(decStr);
			}
			else
			{
				return null;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {

		}
	}
}
