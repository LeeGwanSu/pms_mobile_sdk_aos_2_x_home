package com.pms.sdk.common.security;

import com.pms.sdk.common.compress.CZip;

import java.lang.ref.WeakReference;

public class SA2Enc {

	private static final String _USER_KEY = "amailRND2009";

	public String generateKey (String key) {

		String freeKey = String.valueOf(20091022 * (Integer.parseInt(("" + System.currentTimeMillis()).substring(8)))) + "59483286493";
		String genKey = null;

		if (key == null) {
			genKey = freeKey.substring(0, 16);
		} else {

			if (key.length() > 16) {
				genKey = key.substring(0, 16);
			} else {
				genKey = key + (freeKey.substring(0, 16 - key.length()));
			}
		}
		return genKey;
	}

	public String encode (String str, String userKey) {
		return encode(str, userKey, "UTF-8");
	}

	public String encode (String str, String userKey, String charSet) {
		if (str == null || userKey == null || charSet == null)
			return null;

		String key = userKey.getBytes().length < 16 ? userKey + _USER_KEY.substring(0, 16 - userKey.getBytes().length) : userKey;
        WeakReference<CZip> cZipWeakReference = new WeakReference<CZip>(new CZip());

		try {
		    if(cZipWeakReference.get()!=null)
            {
                CZip cZip = cZipWeakReference.get();
                byte[] enc1 = cZip.zipStringToBytes(str, charSet);

                byte[] enc2 = key == null ? Seed.seedEncrypt(enc1) : Seed.seedEncrypt(enc1, key.getBytes());

                return Base64.encodeBytes(enc2);
            }
		    else
            {
                return null;
            }

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
