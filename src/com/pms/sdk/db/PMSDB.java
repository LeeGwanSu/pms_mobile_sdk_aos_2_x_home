package com.pms.sdk.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.pms.sdk.bean.Data;
import com.pms.sdk.bean.Logs;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.MsgGrp;
import com.pms.sdk.common.db.EDBAdapter;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DateUtil;

/**
 * @since 2012.01.15
 * @author erzisk
 * @description pms db (sqlite)
 */
public class PMSDB extends EDBAdapter {

	private static PMSDB instance;

	public static PMSDB getInstance (Context c) {
		if (instance == null) {
			instance = new PMSDB(c);
		}
		return instance;
	}

	private PMSDB(Context c) {
		super(c);
		DATABASE_NAME = "pms_2.0.db";
		DATABASE_VERSION = 22;
	}

	@Override
	protected void onDBCreate (SQLiteDatabase db) {
		CLog.i("onDBCreate");
		db.execSQL(MsgGrp.CREATE_MSG_GRP);
		db.execSQL(Msg.CREATE_MSG);
		db.execSQL(Logs.CREAT_LOGS);
		db.execSQL(Data.CREATE_DATA);
	}

	@Override
	protected void onDBUpgrade (SQLiteDatabase db, int oldVersion, int newVersion) {
		CLog.i("onUpgrade:Upgrading database from version " + oldVersion + " to " + newVersion);

		if (oldVersion == 21)
			db.execSQL(Data.CREATE_DATA);
		else {
			db.execSQL("DROP TABLE IF EXISTS " + MsgGrp.TABLE_NAME);
			db.execSQL("DROP TABLE IF EXISTS " + Msg.TABLE_NAME);
			db.execSQL("DROP TABLE IF EXISTS " + Logs.TABLE_NAME);
			db.execSQL("DROP TABLE IF EXISTS " + Data.TABLE_NAME);
			onDBCreate(db);
		}
	}

	@Override
	protected void printError (Exception e) {
		CLog.e("printError:" + e.getMessage());
		e.printStackTrace();
	}

	// ////////////////////////////////////////////////
	// [start] Handle PMS DB
	// ////////////////////////////////////////////////
	public void deleteAll () {
		this.delete(MsgGrp.TABLE_NAME, null, null);
		this.delete(Msg.TABLE_NAME, null, null);
		this.delete(Logs.TABLE_NAME, null, null);
	}

	// ////////////////////////////////////////////////
	// [start] tbl_msg_grp
	// ////////////////////////////////////////////////

	/**
	 * select msg grp list
	 *
	 * @return
	 */
	public Cursor selectMsgGrpList () {
		String sql = "SELECT * " + " FROM " + MsgGrp.TABLE_NAME + " ORDER BY " + MsgGrp.REG_DATE + " DESC, CAST(" + MsgGrp.USER_MSG_ID
				+ " AS INTEGER) DESC";
		return this.rawQuery(sql, null);
	}

	/**
	 * select msg grp
	 * 
	 * @return
	 */
	public MsgGrp selectMsgGrp (String msgCode) {
		String sql = "SELECT * " + " FROM " + MsgGrp.TABLE_NAME + " WHERE " + MsgGrp.MSG_GRP_CD + "='" + msgCode + "'";
		Cursor c = this.rawQuery(sql, null);

		try {
			return c.moveToFirst() ? new MsgGrp(c) : null;
		}
		catch (Exception e) {
			return null;
		}
		finally {
			if(c!=null && !c.isClosed())
			{
				c.close();
			}
		}
	}

	/**
	 * select new msg cnt
	 * 
	 * @return
	 */
	public int selectNewMsgCnt () {
		String sql = "SELECT COUNT(0) " + " FROM " + Msg.TABLE_NAME + " WHERE " + Msg.READ_YN + "='" + Msg.READ_N + "'";
		Cursor c = this.rawQuery(sql, null);
		try {
			return c.moveToFirst() ? c.getInt(0) : 0;
		}
		catch (Exception e) {
			return 0;
		}
		finally {
			if(c!=null && !c.isClosed())
			{
				c.close();
			}
		}
	}

	/**
	 * insert msg grp
	 * 
	 * @param msg
	 * @return
	 */
	public void insertMsgGrp (Msg msg) {
		String sql = "INSERT INTO " + MsgGrp.TABLE_NAME + "(" + MsgGrp.USER_MSG_ID + "," + MsgGrp.MSG_GRP_NM + "," + MsgGrp.MSG_TEXT + ","
				+ MsgGrp.MSG_GRP_CD + "," + MsgGrp.MSG_ID + "," + MsgGrp.MSG_TYPE + "," + MsgGrp.REG_DATE + "," + MsgGrp.NEW_MSG_CNT + ") "
				+ " VALUES ( " + "'" + msg.userMsgId + "'," + "'" + msg.msgGrpNm + "'," + "'" + msg.msgText + "'," + "'" + msg.msgGrpCd + "'," + "'"
				+ msg.msgId + "'," + "'" + msg.msgType + "'," + "'" + msg.regDate + "'," + " (SELECT COUNT(0) " + " FROM " + Msg.TABLE_NAME
				+ " WHERE " + Msg.MSG_GRP_CD + " = '" + msg.msgGrpCd + "'))";
		this.execSQL(sql, new String[] {});
	}

	/**
	 * insert msg grp
	 * 
	 * @param values
	 * @return
	 */
	public long insertMsgGrp (ContentValues values) {
		return this.insert(MsgGrp.TABLE_NAME, null, values);
	}

	/**
	 * update recent msg grp
	 * 
	 * @param msg
	 *        ex) UPDATE TBL_MSG_GRP
	 * 
	 *        SET TITLE='message title4', MSG='message content4', MSG_CODE='00000', MSG_ID='4', MSG_TYPE='T', REG_DATE='20130115030304', NEW_MSG_CNT =
	 *        ( SELECT COUNT(0) AS NEW_MSG_CNT FROM TBL_MSG A, TBL_MSG_GRP B WHERE A.REG_DATE > B.REG_DATE AND A.MSG_ID > B.MSG_ID AND A.MSG_CODE =
	 *        '00000' AND B.MSG_CODE = '00000' )
	 * 
	 *        WHERE MSG_CODE='00000'
	 */
	public void updateRecentMsgGrp (Msg msg) {
		String sql = "UPDATE " + MsgGrp.TABLE_NAME + " SET " + MsgGrp.USER_MSG_ID + "='" + msg.userMsgId + "', " + MsgGrp.MSG_GRP_NM + "='"
				+ msg.msgGrpNm + "', " + MsgGrp.MSG_TEXT + "='" + msg.msgText + "', " + MsgGrp.MSG_GRP_CD + "='" + msg.msgGrpCd + "', "
				+ MsgGrp.MSG_ID + "='" + msg.msgId + "', " + MsgGrp.MSG_TYPE + "='" + msg.msgType + "', " + MsgGrp.REG_DATE + "='" + msg.regDate
				+ "', " + MsgGrp.NEW_MSG_CNT
				+ " = CAST (("
				+ " SELECT COUNT(0) "
				+
				// " FROM " + Msg.TABLE_NAME + " A, " + MsgGrp.TABLE_NAME + " B " +
				// " WHERE A." + Msg.REG_DATE + " >= B." + MsgGrp.REG_DATE + " AND A." + Msg.MSG_ID + " > B." + MsgGrp.MSG_ID + " AND A." +
				// Msg.MSG_CODE + " = '" + msg.msgCode + "' AND B." + MsgGrp.MSG_CODE + "='" + msg.msgCode + "' " +
				" FROM " + Msg.TABLE_NAME + " WHERE " + Msg.READ_YN + " = '" + Msg.READ_N + "' AND " + Msg.MSG_GRP_CD + " ='" + msg.msgGrpCd + "'"
				+ " ) AS INTEGER)" + " WHERE " + MsgGrp.MSG_GRP_CD + "='" + msg.msgGrpCd + "' ";

		this.execSQL(sql);
	}

	/**
	 * update new msg cnt
	 */
	public void updateNewMsgCnt () {

		Cursor c = this.selectMsgGrpList();
		try {
			c.moveToFirst();

			String msgCode;
			while (!c.isAfterLast()) {
				msgCode = getStringFromCursor(c, MsgGrp.MSG_GRP_CD);
				String sql = "UPDATE " + MsgGrp.TABLE_NAME + " SET " + MsgGrp.NEW_MSG_CNT + " = CAST ((" + " SELECT COUNT(0) " + " FROM "
						+ Msg.TABLE_NAME + " WHERE " + Msg.READ_YN + " = '" + Msg.READ_N + "' AND " + Msg.MSG_GRP_CD + " ='" + msgCode + "'"
						+ " ) AS INTEGER)" + " WHERE " + MsgGrp.MSG_GRP_CD + "='" + msgCode + "'";
				this.execSQL(sql);

				c.moveToNext();
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		finally {
			if(c!=null && !c.isClosed())
			{
				c.close();
			}
		}
	}

	/**
	 * update msg grp
	 * 
	 * @return
	 */
	public long updateMsgGrp (String msgCode, ContentValues values) {
		return this.update(MsgGrp.TABLE_NAME, values, MsgGrp.MSG_GRP_CD + "=?", new String[] { msgCode });
	}

	/**
	 * update msg grp
	 * 
	 * @param msgGrp
	 * @return
	 */
	public long updateMsgGrp (MsgGrp msgGrp) {
		ContentValues values = new ContentValues();
		values.put(MsgGrp.USER_MSG_ID, msgGrp.userMsgId);
		values.put(MsgGrp.MSG_GRP_NM, msgGrp.msgGrpNm);
		values.put(MsgGrp.MSG_TEXT, msgGrp.msgText);
		values.put(MsgGrp.MSG_GRP_CD, msgGrp.msgGrpCd);
		values.put(MsgGrp.MSG_ID, msgGrp.msgId);
		values.put(MsgGrp.MSG_TYPE, msgGrp.msgType);
		values.put(MsgGrp.REG_DATE, msgGrp.regDate);
		values.put(MsgGrp.NEW_MSG_CNT, msgGrp.newMsgCnt);
		return updateMsg(msgGrp.msgGrpCd, values);
	}

	/**
	 * delete msg grp
	 * 
	 * @param msgCode
	 * @return
	 */
	public long deleteMsgGrp (String msgCode) {
		long result = 0;
		result += this.delete(MsgGrp.TABLE_NAME, MsgGrp.MSG_GRP_CD + "=?", new String[] { msgCode });
		result += this.delete(Msg.TABLE_NAME, Msg.MSG_GRP_CD + "=?", new String[] { msgCode });
		return result;
	}

	/**
	 * delete empty msg grp
	 * 
	 * @return
	 */
	public void deleteEmptyMsgGrp () {
		Cursor c = this.selectMsgGrpList();
		c.moveToFirst();
		while (!c.isAfterLast()) {
			String msgCode = getStringFromCursor(c, MsgGrp.MSG_GRP_CD);
			String sql = "DELETE FROM " + MsgGrp.TABLE_NAME + " WHERE (SELECT COUNT(0) FROM " + Msg.TABLE_NAME + " WHERE " + Msg.MSG_GRP_CD + " = '"
					+ msgCode + "') <= 0 " + " AND " + MsgGrp.MSG_GRP_CD + " = '" + msgCode + "'";
			this.execSQL(sql);
			c.moveToNext();
		}
		if(c!=null && !c.isClosed())
		{
			c.close();
		}
	}

	// ////////////////////////////////////////////////
	// [end] tbl_msg_grp
	// ////////////////////////////////////////////////

	// ////////////////////////////////////////////////
	// [start] tbl_msg
	// ////////////////////////////////////////////////
	/**
	 * select MsgList
	 * 
	 * @param page
	 * @param row
	 * @return
	 */
	public Cursor selectMsgList (int page, int row) {
		String sql = "SELECT * " + " FROM " + Msg.TABLE_NAME + " ORDER BY " + Msg.REG_DATE + " DESC, CAST(" + Msg.USER_MSG_ID + " AS INTEGER) DESC "
				+ " LIMIT " + row + " OFFSET " + ((page - 1) * row);
		return this.rawQuery(sql, null);
	}

	/**
	 * select msg list
	 * 
	 * @return
	 */
	public Cursor selectMsgList (String msgCode) {
		String sql = "SELECT * " + " FROM " + Msg.TABLE_NAME + " WHERE " + Msg.MSG_GRP_CD + "='" + msgCode + "'" + " ORDER BY " + Msg.REG_DATE
				+ " DESC, CAST(" + Msg.USER_MSG_ID + " AS INTEGER) DESC";
		return this.rawQuery(sql, null);
	}

	/**
	 * select msg
	 * 
	 * @param msgId
	 * @return
	 */
	public Msg selectMsgWhereMsgId (String msgId) {
		String sql = "SELECT * " + " FROM " + Msg.TABLE_NAME + " WHERE " + Msg.MSG_ID + "='" + msgId + "'";
		Cursor c = this.rawQuery(sql, null);

		try {
			return c.moveToFirst() ? new Msg(c) : null;
		} catch (Exception e) {
			return null;
		} finally {
			if(c!=null && !c.isClosed())
			{
				c.close();
			}
		}
	}

	/**
	 * select msg
	 * 
	 * @param userMsgId
	 * @return
	 */
	public Msg selectMsgWhereUserMsgId (String userMsgId) {
		String sql = "SELECT * " + " FROM " + Msg.TABLE_NAME + " WHERE " + Msg.USER_MSG_ID + "='" + userMsgId + "'";
		Cursor c = this.rawQuery(sql, null);

		try {
			return c.moveToFirst() ? new Msg(c) : null;
		} catch (Exception e) {
			return null;
		} finally {
			if(c!=null && !c.isClosed())
			{
				c.close();
			}
		}
	}

	/**
	 * select min message code
	 * 
	 * @return
	 */
	public int selectMinMsgCode () {
		Cursor c;
		String sql = "SELECT IFNULL(MIN(CAST(" + MsgGrp.MSG_GRP_CD + " AS INTEGER)), -1) " + " FROM " + MsgGrp.TABLE_NAME;
		c = this.rawQuery(sql, null);
		c.moveToFirst();
		int temp = c.getInt(0);
		if(c!=null && !c.isClosed())
		{
			c.close();
		}
		return temp;
	}

	public Cursor selectQuery (String sql) {
		return this.rawQuery(sql, null);
	}

	public void insertQuery (String sql) {
		this.execSQL(sql);
	}

	/**
	 * insert msg
	 * 
	 * @param values
	 * @return
	 */
	public long insertMsg (ContentValues values) {
		return this.insert(Msg.TABLE_NAME, null, values);
	}

	/**
	 * insert msg
	 * 
	 * @param msg
	 * @return
	 */
	public long insertMsg (Msg msg) {
		ContentValues values = new ContentValues();
		values.put(Msg.USER_MSG_ID, msg.userMsgId);
		values.put(Msg.MSG_GRP_NM, msg.msgGrpNm);
		values.put(Msg.APP_LINK, msg.appLink);
		values.put(Msg.ICON_NAME, msg.iconName);
		values.put(Msg.MSG_ID, msg.msgId);
		values.put(Msg.PUSH_TITLE, msg.pushTitle);
		values.put(Msg.PUSH_MSG, msg.pushMsg);
		values.put(Msg.PUSH_IMG, msg.pushImg);
		values.put(Msg.MSG_TEXT, msg.msgText);
		values.put(Msg.MAP1, msg.map1);
		values.put(Msg.MAP2, msg.map2);
		values.put(Msg.MAP3, msg.map3);
		values.put(Msg.MSG_TYPE, msg.msgType);
		values.put(Msg.READ_YN, msg.readYn);
		values.put(Msg.EXPIRE_DATE, msg.expireDate);
		values.put(Msg.REG_DATE, msg.regDate);
		values.put(Msg.MSG_GRP_CD, msg.msgGrpCd);
		return insertMsg(values);
	}

	/**
	 * update msg
	 * 
	 * @param msg
	 * @return
	 */
	public long updateMsg (Msg msg) {
		ContentValues values = new ContentValues();
		values.put(Msg.USER_MSG_ID, msg.userMsgId);
		values.put(Msg.MSG_GRP_NM, msg.msgGrpNm);
		values.put(Msg.APP_LINK, msg.appLink);
		values.put(Msg.ICON_NAME, msg.iconName);
		values.put(Msg.PUSH_TITLE, msg.pushTitle);
		values.put(Msg.PUSH_MSG, msg.pushMsg);
		values.put(Msg.PUSH_IMG, msg.pushImg);
		values.put(Msg.MSG_TEXT, msg.msgText);
		values.put(Msg.MAP1, msg.map1);
		values.put(Msg.MAP2, msg.map2);
		values.put(Msg.MAP3, msg.map3);
		if (Integer.parseInt(msg.msgGrpCd) > -1) {
			values.put(Msg.MSG_GRP_CD, msg.msgGrpCd);
		}
		values.put(Msg.MSG_TYPE, msg.msgType);
		values.put(Msg.READ_YN, msg.readYn);
		values.put(Msg.EXPIRE_DATE, msg.expireDate);
		values.put(Msg.REG_DATE, msg.regDate);
		return updateMsg(msg.userMsgId, values);
	}

	/**
	 * update read msg
	 * 
	 * @param msgGrpCd
	 * @return
	 */
	public long updateReadMsg (String msgGrpCd, String firstUserMsgId, String lastUserMsgId) {
		ContentValues values = new ContentValues();
		values.put(Msg.READ_YN, Msg.READ_Y);
		String whereClause = Msg.MSG_GRP_CD + "=? AND " + Msg.USER_MSG_ID + ">=? AND " + Msg.USER_MSG_ID + "<=?";
		String[] whereArgs = new String[] { msgGrpCd, firstUserMsgId, lastUserMsgId };
		return this.update(Msg.TABLE_NAME, values, whereClause, whereArgs);
	}

	/**
	 * update read msg where user msgId
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long updateReadMsgWhereUserMsgId (String userMsgId) {
		ContentValues values = new ContentValues();
		values.put(Msg.READ_YN, Msg.READ_Y);
		String whereClause = Msg.USER_MSG_ID + "=?";
		String[] whereArgs = new String[] { userMsgId };
		return this.update(Msg.TABLE_NAME, values, whereClause, whereArgs);
	}

	/**
	 * update read msg where msgId
	 * 
	 * @param msgId
	 * @return
	 */
	public long updateReadMsgWhereMsgId (String msgId) {
		ContentValues values = new ContentValues();
		values.put(Msg.READ_YN, Msg.READ_Y);
		String whereClause = Msg.MSG_ID + "=?";
		String[] whereArgs = new String[] { msgId };
		return this.update(Msg.TABLE_NAME, values, whereClause, whereArgs);
	}

	/**
	 * update msg
	 * 
	 * @return
	 */
	public long updateMsg (String userMsgId, ContentValues values) {
		return this.update(Msg.TABLE_NAME, values, Msg.USER_MSG_ID + "=?", new String[] { userMsgId });
	}

	/**
	 * delete expire msg
	 *
	 * @return
	 */
	public long deleteExpireMsg () {
		Long nowDate = Long.parseLong(DateUtil.getNowDate()) / 100;
		return this.delete(Msg.TABLE_NAME, "CAST(" + Msg.EXPIRE_DATE + " AS LONG) / 100 < CAST(? AS LONG)", new String[] { nowDate + "" });
	}

	/**
	 * delete msg
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long deleteUserMsgId (String userMsgId) {
		return this.delete(Msg.TABLE_NAME, Msg.USER_MSG_ID + "=?", new String[] { userMsgId });
	}

	/**
	 * delete msg
	 * 
	 * @param MsgId
	 * @return
	 */
	public long deleteMsgId (String MsgId) {
		return this.delete(Msg.TABLE_NAME, Msg.MSG_ID + "=?", new String[] { MsgId });
	}

	// ////////////////////////////////////////////////
	// [end] tbl_msg
	// ////////////////////////////////////////////////

	// ////////////////////////////////////////////////
	// [start] tbl_logs
	// ////////////////////////////////////////////////
	/**
	 * select Logs
	 * 
	 * @param type
	 * @param date
	 * @return
	 */
	public Cursor selectLog (String type, String date) {
		String sql = "SELECT * " + " FROM " + Logs.TABLE_NAME + " WHERE " + Logs.LOG_TYPE_FLAG + "='" + type + "'" + " AND " + Logs._DATE + "='"
				+ date + "'" + " ORDER BY " + Logs._ID + " ASC ";

		return this.rawQuery(sql, null);
	}

	/**
	 * insert Logs
	 * 
	 * @param logs
	 * @return
	 */
	public long insertLog (Logs logs) {
		ContentValues values = new ContentValues();
		values.put(Logs._DATE, logs.date);
		values.put(Logs._TIME, logs.time);
		values.put(Logs.LOG_TYPE_FLAG, logs.logFlag);

		if (Logs.TYPE_A.equals(logs.logFlag)) {
			values.put(Logs.API, logs.api);
			values.put(Logs.PARAM, logs.param);
			values.put(Logs.RESULT, logs.result);
		} else if (Logs.TYPE_P.equals(logs.logFlag)) {
			values.put(Logs.PRIVATELOG, logs.privateLog);
		}

		return this.insert(Logs.TABLE_NAME, null, values);
	}

	/**
	 * delete Logs
	 * 
	 * @param date
	 * @return
	 */
	public long deleteLog (String date) {
		return this.delete(Logs.TABLE_NAME, Logs._DATE + "=?", new String[] { date });
	}

	// ////////////////////////////////////////////////
	// [end] tbl_logs
	// ////////////////////////////////////////////////

	// ////////////////////////////////////////////////
	// [end] Handle PMS DB
	// ////////////////////////////////////////////////

	/**
	 * Select Key
	 *
	 * @param key
	 * @return
	 */
	public Cursor selectKey(String key) {
		String sql = "SELECT * " + " FROM " + Data.TABLE_NAME + " WHERE " + Data.KEY + "='" + key + "'";
		return this.rawQuery(sql, null);
	}

	public int selectKeyData(String key) {
		String sql = "SELECT COUNT(0) FROM " + Data.TABLE_NAME + " WHERE " + Data.KEY + "='" + key + "'";
		Cursor c = this.rawQuery(sql, null);
		if (c == null) {
			return 0;
		}
		try {
			return c.moveToFirst() ? c.getInt(0) : 0;
		} catch (Exception e) {
			return 0;
		} finally {
			if(c!=null && !c.isClosed())
			{
				c.close();
			}
		}
	}

	/**
	 * insert Data Value
	 *
	 * @param data
	 * @return
	 */
	public long insertDataValue(Data data) {
		ContentValues values = new ContentValues();
		values.put(Data.KEY, data.key);
		values.put(Data.VALUE, data.value);
		return insertData(values);
	}

	/**
	 * insert msg
	 *
	 * @param values
	 * @return
	 */
	public long insertData(ContentValues values) {
		return this.insert(Data.TABLE_NAME, null, values);
	}

	public long updateDataValue(String key, String value) {
		ContentValues values = new ContentValues();
		values.put(Data.VALUE, value);
		String whereClause = Data.KEY + "=?";
		String[] whereArgs = new String[]{key};
		return this.update(Data.TABLE_NAME, values, whereClause, whereArgs);
	}

	/**
	 * columnname을 통해 data를 가져옴
	 * 
	 * @param c
	 * @param columnName
	 * @return
	 */
	public static String getStringFromCursor (Cursor c, String columnName) {
		return c.getString(c.getColumnIndexOrThrow(columnName));
	}

	/**
	 * 테이블 전체 데이터 보기 (테스트용)
	 */
	public void showAllTable (String... tables) {
		StringBuilder columnList;
		StringBuilder dataRow;
		Cursor c;

		for (String tableName : tables) {
			CLog.d("-----------------------------------table name:" + tableName + "-----------------------------------");
			c = this.query(tableName, null, null, null, null, null, null);

			CLog.d("======== column ========");
			columnList = new StringBuilder();
			for (String columnName : c.getColumnNames()) {
				columnList.append(columnName + "|");
			}
			CLog.d(columnList.toString());

			CLog.d("========  data  ========");
			while (c.moveToNext()) {
				dataRow = new StringBuilder();
				for (String columnName : c.getColumnNames()) {
					dataRow.append(getStringFromCursor(c, columnName) + "|");
				}
				CLog.d(dataRow.toString() + "\n");
			}
			CLog.d("----------------------------------------------------------------------");
			CLog.d(" ");
			if(c!=null && !c.isClosed())
			{
				c.close();
			}
		}
	}

	public void testInsertMsgGrp (String msgCode, int cnt) {
		Msg msg = null;
		for (int i = 0; i < cnt; i++) {
			msg = new Msg();
			msg.userMsgId = String.valueOf(i);
			msg.msgGrpNm = "테스트";
			msg.pushTitle = "push title";
			msg.pushMsg = "push message" + i;
			msg.msgText = "message text";
			msg.msgGrpCd = msgCode;
			msg.msgId = "1000" + i;
			msg.msgType = Msg.TYPE_A;
			int rnd = (int) (Math.random() * 2);
			if (rnd > 0) {
				msg.readYn = Msg.READ_Y;
			} else {
				msg.readYn = Msg.READ_N;
			}
			msg.expireDate = "20140224000000";
			msg.regDate = "20140211215500";
			this.insertMsg(msg);
		}

		MsgGrp msgGrp = new MsgGrp();
		msgGrp.msgGrpNm = msg.msgGrpNm;
		msgGrp.msgText = msg.msgText;
		msgGrp.msgGrpCd = msg.msgGrpCd;
		msgGrp.msgId = msg.msgId;
		msgGrp.msgType = msg.msgType;
		msgGrp.newMsgCnt = cnt + "";
		msgGrp.regDate = msg.regDate;
		this.insertMsgGrp(msg);
	}

	public void testInsertLogs (String type, int cnt) {
		Logs logs = null;
		for (int i = 0; i < cnt; i++) {
			logs = new Logs();
			logs.date = "20140204";
			logs.time = "14122" + i;
			logs.logFlag = type;
			if (Logs.TYPE_A.equals(type)) {
				logs.api = "deviceCert";
				logs.param = "{\"asdasdasdasdasd\"}";
				logs.result = "{\"asdasd\":\"asdasdasd\"}";
			} else if (Logs.TYPE_P.equals(type)) {
				logs.privateLog = Logs.SUCCSESS;
			}

			this.insertLog(logs);
		}
	}

	public void testSelectLogs (String type, String date) {
		StringBuilder dataList;
		StringBuilder dataRow;

		Cursor c = this.selectLog(type, date);
		dataList = new StringBuilder();
		while (c.moveToNext()) {
			dataRow = new StringBuilder();
			for (String columnName : c.getColumnNames()) {
				dataRow.append(getStringFromCursor(c, columnName) + "|");
			}
			dataList.append(dataRow.toString() + "\n");
		}

		CLog.d("========  data  ========");
		CLog.d(dataList.toString());
	}

	public void testDelectLogs (String date) {
		this.deleteLog(date);
	}

	public void testUpdateLogs () {
		String sql = "SELECT * " + " FROM " + Logs.TABLE_NAME;

		Cursor c = this.rawQuery(sql, null);

		CLog.e(c.getCount() + "");
		while (c.moveToNext()) {
			Logs logs = new Logs(c);
			CLog.e(logs.id + " " + logs.time);
			int index = logs.time.indexOf("24");
			if ((index != -1) && (index == 0)) {
				String temp = logs.time.substring(2, logs.time.length());
				logs.time = "00" + temp;
				CLog.e(logs.id + " " + logs.time);
				CLog.e(testUpdate(logs.id, logs.time) + "");
			}
		}
		if(c!=null && !c.isClosed())
		{
			c.close();
		}
	}

	public long testUpdate (String id, String time) {
		ContentValues values = new ContentValues();
		values.put(Logs._TIME, time);
		return this.update(Logs.TABLE_NAME, values, Logs._ID + "=?", new String[] { id });
	}
}
