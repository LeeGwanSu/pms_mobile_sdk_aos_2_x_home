package com.pms.sdk.bean;

import android.os.Bundle;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.common.util.StringUtil;

public class PushMsg implements IPMSConsts {

    public String msgId;
    public String notiTitle;
    public String notiMsg;
    public String notiImg;
    public String message;
    public String sound;
    public String msgType;
    public String data;
    public String notPopup;
    public String silentMsg;

    public PushMsg(Bundle extras) {
        msgId = extras.getString(KEY_MSG_ID);
        notiTitle = extras.getString(KEY_NOTI_TITLE);
        notiMsg = extras.getString(KEY_NOTI_MSG);
        notiImg = extras.getString(KEY_NOTI_IMG);
        message = extras.getString(KEY_MSG);
        sound = extras.getString(KEY_SOUND);
        msgType = extras.getString(KEY_MSG_TYPE);
        data = extras.getString(KEY_DATA);
        notPopup = extras.getString(KEY_NOT_POPUP);
        silentMsg = extras.getString(KEY_SILENT_MSG);
    }

    public boolean isEmptyMsg() {
        return (StringUtil.isEmpty(msgId) ||
                StringUtil.isEmpty(notiTitle) ||
                StringUtil.isEmpty(notiMsg) ||
                StringUtil.isEmpty(msgType));
    }

    public boolean isSilentMsg() {
        return !(silentMsg == null || silentMsg.isEmpty() || silentMsg.equals(FLAG_N));
    }

    public boolean isShowPopUp() {
        return notPopup.equals(FLAG_N);
    }

    @Override
    public String toString() {
        return "PushMsg{" +
                "msgId='" + msgId + '\'' +
                ", notiTitle='" + notiTitle + '\'' +
                ", notiMsg='" + notiMsg + '\'' +
                ", notiImg='" + notiImg + '\'' +
                ", message='" + message + '\'' +
                ", sound='" + sound + '\'' +
                ", msgType='" + msgType + '\'' +
                ", data='" + data + '\'' +
                ", notPopup='" + notPopup + '\'' +
                ", silentMsg='" + silentMsg + '\'' +
                '}';
    }
}
