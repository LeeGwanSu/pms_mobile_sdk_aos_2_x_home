package com.pms.sdk.bean;

import org.json.JSONObject;

import android.database.Cursor;

/**
 * @since 2012.12.26
 * @author erzisk
 * @description message bean
 */
public class Msg {

	public static final String TABLE_NAME = "TBL_MSG";
	public static final String _ID = "_id";
	public static final String USER_MSG_ID = "USER_MSG_ID";
	public static final String MSG_GRP_NM = "MSG_GRP_NM";
	public static final String APP_LINK = "APP_LINK";
	public static final String ICON_NAME = "ICON_NAME";
	public static final String MSG_ID = "MSG_ID";
	public static final String PUSH_TITLE = "PUSH_TITLE";
	public static final String PUSH_MSG = "PUSH_MSG";
	public static final String PUSH_IMG = "PUSH_IMG";
	public static final String MSG_TEXT = "MSG_TEXT";
	public static final String MAP1 = "MAP1";
	public static final String MAP2 = "MAP2";
	public static final String MAP3 = "MAP3";
	public static final String MSG_TYPE = "MSG_TYPE";
	public static final String READ_YN = "READ_YN";
	public static final String EXPIRE_DATE = "EXPIRE_DATE";
	public static final String REG_DATE = "REG_DATE";
	public static final String MSG_GRP_CD = "MSG_GRP_CD";

	// [code flag]
	// default
	public static final String CODE_DEFAULT = "00000";

	// [type flag]
	// text
	public static final String TYPE_T = "T";
	// attach
	public static final String TYPE_A = "A";
	// html
	public static final String TYPE_H = "H";
	// link
	public static final String TYPE_L = "L";

	// [read falg]
	// read
	public static final String READ_Y = "Y";
	// unread
	public static final String READ_N = "N";

	public static final int ROW_COUNT = 50;

	public static final String CREATE_MSG = "CREATE TABLE " + TABLE_NAME + "( " + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + USER_MSG_ID
			+ " INTEGER, " + MSG_GRP_NM + " TEXT, " + APP_LINK + " TEXT, " + ICON_NAME + " TEXT, " + MSG_ID + " INTEGER, " + PUSH_TITLE + " TEXT, "
			+ PUSH_MSG + " TEXT, " + PUSH_IMG + " TEXT, " + MSG_TEXT + " TEXT, " + MAP1 + " TEXT, " + MAP2 + " TEXT, " + MAP3 + " TEXT, " + MSG_TYPE
			+ " TEXT, " + READ_YN + " TEXT, " + EXPIRE_DATE + " TEXT, " + REG_DATE + " TEXT, " + MSG_GRP_CD + " TEXT " + ");";

	public String id = "-1";
	public String userMsgId = "";
	public String msgGrpNm = "";
	public String appLink = "";
	public String iconName = "";
	public String msgId = "";
	public String pushTitle = "";
	public String pushMsg = "";
	public String pushImg = "";
	public String msgText = "";
	public String map1 = "";
	public String map2 = "";
	public String map3 = "";
	public String msgType = "";
	public String readYn = "";
	public String expireDate = "";
	public String regDate = "";
	public String msgGrpCd = "";

	public Msg() {
	}

	public Msg(Cursor c) {
		// Convert.cursorToBean(c, this);

		id = c.getString(c.getColumnIndexOrThrow(_ID));
		userMsgId = c.getString(c.getColumnIndexOrThrow(USER_MSG_ID));
		msgGrpNm = c.getString(c.getColumnIndexOrThrow(MSG_GRP_NM));
		appLink = c.getString(c.getColumnIndexOrThrow(APP_LINK));
		iconName = c.getString(c.getColumnIndexOrThrow(ICON_NAME));
		msgId = c.getString(c.getColumnIndexOrThrow(MSG_ID));
		pushTitle = c.getString(c.getColumnIndexOrThrow(PUSH_TITLE));
		pushMsg = c.getString(c.getColumnIndexOrThrow(PUSH_MSG));
		pushImg = c.getString(c.getColumnIndexOrThrow(PUSH_IMG));
		msgText = c.getString(c.getColumnIndexOrThrow(MSG_TEXT));
		map1 = c.getString(c.getColumnIndexOrThrow(MAP1));
		map2 = c.getString(c.getColumnIndexOrThrow(MAP2));
		map3 = c.getString(c.getColumnIndexOrThrow(MAP3));
		msgType = c.getString(c.getColumnIndexOrThrow(MSG_TYPE));
		readYn = c.getString(c.getColumnIndexOrThrow(READ_YN));
		expireDate = c.getString(c.getColumnIndexOrThrow(EXPIRE_DATE));
		regDate = c.getString(c.getColumnIndexOrThrow(REG_DATE));
		msgGrpCd = c.getString(c.getColumnIndexOrThrow(MSG_GRP_CD));
	}

	public Msg(JSONObject jo) {
		// Convert.jsonToBeanCamel(jo, this);

		// try {id = jo.getString("id");} catch (Exception e) {e.printStackTrace();}
		try {
			userMsgId = jo.getString("userMsgId");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			msgGrpNm = jo.getString("msgGrpNm");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			appLink = jo.getString("appLink");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			iconName = jo.getString("iconName");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			msgId = jo.getString("msgId");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			pushTitle = jo.getString("pushTitle");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			pushMsg = jo.getString("pushMsg");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			pushImg = jo.getString("pushImg");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			msgText = jo.getString("msgText");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			map1 = jo.getString("map1");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			map2 = jo.getString("map2");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			map3 = jo.getString("map3");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			msgType = jo.getString("msgType");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			readYn = jo.getString("readYn");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			expireDate = jo.getString("expireDate");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			regDate = jo.getString("regDate");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			msgGrpCd = jo.getString("msgGrpCd");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}