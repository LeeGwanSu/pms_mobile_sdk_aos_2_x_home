package com.pms.sdk.push;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager;
import com.pms.sdk.api.request.DeviceCert;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.push.mqtt.MQTTBinder;
import com.pms.sdk.push.mqtt.MQTTService;

import org.json.JSONObject;

/**
 * Created by ydmner on 7/27/16.
 */
public class FCMPushService extends FirebaseMessagingService implements IPMSConsts {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            CLog.i("FCM OnMessageReceived From : " + remoteMessage.getFrom()+"\npayload : "+remoteMessage.getData());

            if (PMSUtil.getGCMProjectId(this).equals(remoteMessage.getFrom())) {
                Intent broadcastIntent = new Intent(getApplicationContext(), PushReceiver.class);
                broadcastIntent.setAction(ACTION_RECEIVE);
                broadcastIntent.addCategory(getApplication().getPackageName());

                broadcastIntent.putExtra(MQTTBinder.KEY_MSG, new JSONObject(remoteMessage.getData()).toString());
                broadcastIntent.putExtra("message_id", remoteMessage.getMessageId());
                sendBroadcast(broadcastIntent);
            } else {
                CLog.e("No Match SenderID");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMessageSent(String msgId) {
        CLog.e("onMessageSent() " + msgId);
        super.onMessageSent(msgId);
    }

    @Override
    public void onSendError(String s, Exception e) {
        CLog.e("onSendError() " + s + ", " + e.toString());
        super.onSendError(s, e);
    }

    @Override
    public void onNewToken(String token)
    {
        super.onNewToken(token);
        PMSUtil.setGCMToken(getApplicationContext(), token);
        CLog.i("onNewToken, registration ID = " + token);
    }
}
