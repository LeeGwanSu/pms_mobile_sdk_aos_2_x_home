package com.pms.sdk.push;

import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.PMSUtil;

/**
 * PMSWebViewBridge rich click 관련 클래스
 * 
 * @author erzisk
 * @since 2013.11.26
 */
public class PMSWebViewBridge implements IPMSConsts {

	private Context mContext;

	public PMSWebViewBridge(Context context) {
		this.mContext = context;
	}

	public boolean isReplaceLink () {
		CLog.i("call isReplaceLink");
		return true;
	}

	public void addRichClick (String msgId, String linkSeq, String pushMsgId) {
		CLog.i("call onRichClick");
		JSONObject richClick = new JSONObject();
		try {
			richClick.put("msgId", msgId);
			richClick.put("linkSeq", linkSeq);
			richClick.put("msgPushType", pushMsgId);
			richClick.put("workday", DateUtil.getNowDate());
		} catch (Exception e) {
			e.printStackTrace();
		}
		CLog.d(String.format("msgId:%s,linkSeq:%s,pushMsgId:%s", msgId, linkSeq, pushMsgId));

		PMSUtil.arrayToPrefs(mContext, PREF_CLICK_LIST, richClick);
		CLog.d("clickList:" + PMSUtil.arrayFromPrefs(mContext, PREF_CLICK_LIST));
	}
}
