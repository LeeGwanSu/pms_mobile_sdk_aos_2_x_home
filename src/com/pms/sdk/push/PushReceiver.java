package com.pms.sdk.push;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.pms.sdk.IPMSConsts;
import com.pms.sdk.PMS;
import com.pms.sdk.api.QueueManager;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.PushMsg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.mqtt.MQTTBinder;
import com.pms.sdk.push.mqtt.RestartReceiver;
import com.pms.sdk.view.BitmapLruCache;

import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.List;

import androidx.core.app.NotificationCompat;

/**
 * push receiver
 *
 * @author erzisk
 * @since 2013.06.07
 */
public class PushReceiver extends BroadcastReceiver implements IPMSConsts {

    // notification id
    public final static int NOTIFICATION_ID = 0x253470;
    private static final int NOTIFICATION_GROUP_SUMMARY_ID = 1;
    private static final String NOTIFICATION_GROUP = "com.pms.sdk.notification_type";
    private static int sNotificationId = NOTIFICATION_GROUP_SUMMARY_ID + 1;
    private static final int START_TASK_TO_FRONT = 2;
    private static final int DEFAULT_SHOWING_TIME = 30000;
    private final Handler mFinishHandler = new Handler();
    private Prefs mPrefs;
    private PowerManager pm;
    private PowerManager.WakeLock wl;
//    private QueueManager mQueueManager;
    /**
     * finish runnable
     */
    private final Runnable finishRunnable = new Runnable() {
        @Override
        public void run() {
            if (wl != null && wl.isHeld()) {
                wl.release();
            }
        }
    };
    private Bitmap mPushImage;

    @Override
    public synchronized void onReceive(final Context context, final Intent intent) {
        CLog.i("onReceive() -> " + intent.toString());
        mPrefs = new Prefs(context);

        if (!intent.getAction().equals(ACTION_REGISTRATION)) {

            // receive push message
            if (intent.getAction().equals(MQTTBinder.ACTION_RECEIVED_MSG) || intent.getAction().equals(ACTION_MQTT_RECEIVE)) {
                // private server

                CLog.i("onReceive:receive from private server");

            } else if (intent.getAction().equals(ACTION_RECEIVE) || intent.getAction().equals(ACTION_GCM_RECEIVE)) {
                // other push send

//                context.sendBroadcast(new Intent(ACTION_FORCE_START).putExtras(intent));
//                context.sendBroadcast(new Intent(context,RestartReceiver.class).setAction(ACTION_FORCE_START).putExtras(intent));
                // gcm
                // key: title, msgType, message, sound, data
                CLog.i("onReceive:receive from GCM");
            }
            String message = intent.getStringExtra(MQTTBinder.KEY_MSG);
            // set push info
            try {
                JSONObject msgObj = new JSONObject(message);
                if (msgObj.has(KEY_MSG_ID)) {
                    intent.putExtra(KEY_MSG_ID, msgObj.getString(KEY_MSG_ID));
                }
                if (msgObj.has(KEY_NOTI_TITLE)) {
                    intent.putExtra(KEY_NOTI_TITLE, msgObj.getString(KEY_NOTI_TITLE));
                }
                if (msgObj.has(KEY_MSG_TYPE)) {
                    intent.putExtra(KEY_MSG_TYPE, msgObj.getString(KEY_MSG_TYPE));
                }
                if (msgObj.has(KEY_NOTI_MSG)) {
                    intent.putExtra(KEY_NOTI_MSG, msgObj.getString(KEY_NOTI_MSG));
                }
                if (msgObj.has(KEY_MSG)) {
                    intent.putExtra(KEY_MSG, msgObj.getString(KEY_MSG));
                }
                if (msgObj.has(KEY_SOUND)) {
                    intent.putExtra(KEY_SOUND, msgObj.getString(KEY_SOUND));
                }
                if (msgObj.has(KEY_NOTI_IMG)) {
                    intent.putExtra(KEY_NOTI_IMG, msgObj.getString(KEY_NOTI_IMG));
                }
                if (msgObj.has(KEY_NOT_POPUP)) {
                    intent.putExtra(KEY_NOT_POPUP, msgObj.getString(KEY_NOT_POPUP));
                }
                if (msgObj.has(KEY_SILENT_MSG)) {
                    intent.putExtra(KEY_SILENT_MSG, msgObj.getString(KEY_SILENT_MSG));
                }
                if (msgObj.has(KEY_DATA)) {
                    intent.putExtra(KEY_DATA, msgObj.getString(KEY_DATA));
                }

                onImgDown(context, intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private synchronized void onImgDown(final Context context, final Intent intent) {

        if (isImagePush(intent.getExtras())) {

            // image push
            QueueManager queueManager = QueueManager.getInstance();
            RequestQueue queue = queueManager.getRequestQueue();
            queue.getCache().clear();
            ImageLoader imageLoader = new ImageLoader(queue, new BitmapLruCache());

            imageLoader.get(intent.getStringExtra(KEY_NOTI_IMG), new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    if (response == null) {
                        CLog.e("response is null");
                        return;
                    }
                    if (response.getBitmap() == null) {
                        CLog.e("bitmap is null");
                        return;
                    }
                    mPushImage = response.getBitmap();
                    CLog.i("imageWidth:" + mPushImage.getWidth());
                    onMessage(context, intent);
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    CLog.e("onErrorResponse:" + error.getMessage());
                    // wrong img url (or exception)
                    onMessage(context, intent);
                }
            });
        } else {
            // default push
            onMessage(context, intent);
        }
    }

    /**
     * on message (gcm, private msg receiver)
     *
     * @param context
     * @param intent
     */
    private synchronized void onMessage(final Context context, Intent intent) {

        try {
            final Bundle extras = intent.getExtras();

//            CLog.e("Push Message Payload : " + extras.toString());

            PMS pms = PMS.getInstance(context);

            PushMsg pushMsg = new PushMsg(extras);

            if (pushMsg.isSilentMsg())
            {
                CLog.i("SilentPush");
                //push silent receiver class
                Intent innerIntent;
                String stringPushSilentReceiverClass = mPrefs.getString(IPMSConsts.PREF_PUSH_SILENT_RECEIVER_CLASS);
                if(stringPushSilentReceiverClass != null)
                {
                    try
                    {
                        Class<?> cls = Class.forName(stringPushSilentReceiverClass);
                        innerIntent = new Intent(context, cls).putExtras(extras);
                        innerIntent.setAction(RECEIVER_PUSH_SILENT);
                        context.sendBroadcast(innerIntent);
                        return;
                    } catch (ClassNotFoundException e)
                    {
                        CLog.e(e.getMessage());
                    }
                }
            }

            if (pushMsg.isEmptyMsg()) {
//                pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
//                wl = pm.newWakeLock(PowerManager.PROXIMITY_SCREEN_OFF_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "Tag");
//                wl.acquire();
                if (FLAG_Y.equals(PMSUtil.getMQTTFlag(context)) && FLAG_Y.equals(PMSUtil.getPrivateFlag(context)))
                {
                    CLog.i("msgId or notiTitle or notiMsg or msgType is null");
                    if (FLAG_Y.equals(mPrefs.getString(IPMSConsts.PREF_SCREEN_WAKEUP_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG))) {
                        // screen on
                        pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                        wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "myapp:mywakelocktag");
                        if (!pm.isScreenOn()) {
                            wl.acquire();
                            mFinishHandler.postDelayed(finishRunnable, DEFAULT_SHOWING_TIME);
                        }
                    }
                    Intent sendIntent = new Intent(context, RestartReceiver.class);
                    sendIntent.setAction(ACTION_FORCE_START);
                    context.sendBroadcast(sendIntent);
                }
                return;
            }

            CLog.i(pushMsg + "");

            PMSDB db = PMSDB.getInstance(context);

            // check already exist msg
            Msg existMsg = db.selectMsgWhereMsgId(pushMsg.msgId);
            if (existMsg != null && existMsg.msgId.equals(pushMsg.msgId)) {
                CLog.i("already exist msg");
                return;
            }

            // insert (temp) new msg
            Msg newMsg = new Msg();
            newMsg.readYn = Msg.READ_N;
            newMsg.msgGrpCd = "999999";
            newMsg.expireDate = "0";
            newMsg.msgId = pushMsg.msgId;

            db.insertMsg(newMsg);


            Intent innerIntent = null;
            // refresh list and badge
            String strPushReciverClass = mPrefs.getString(PREF_PUSH_RECEIVER_CLASS);
            
            if (strPushReciverClass != null) {
                try {
                    Class<?> cls = Class.forName(strPushReciverClass);
                    innerIntent = new Intent(context, cls).putExtras(extras);
                    innerIntent.setAction(RECEIVER_PUSH);
                } catch (ClassNotFoundException e) {
                    CLog.e(e.getMessage());
                }
            }

            if (innerIntent == null) {
                innerIntent = new Intent(RECEIVER_PUSH).putExtras(extras);
            }
            
            if (innerIntent != null) {
                innerIntent.addCategory(context.getPackageName());
                context.sendBroadcast(innerIntent);
            }

            // show noti
            // ** 수정 ** 기존 코드는 PREF_MSG_FLAG로 되어 있어 NOTI_FLAG로 변경 함
            CLog.i("NOTI FLAG : " + mPrefs.getString(PREF_NOTI_FLAG));

            if (FLAG_Y.equals(mPrefs.getString(PREF_NOTI_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG))) {
                // check push flag

                if (FLAG_Y.equals(mPrefs.getString(IPMSConsts.PREF_SCREEN_WAKEUP_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG))) {
                    // screen on
                    pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                    wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "myapp:mywakelocktag");
                    if (!pm.isScreenOn()) {
                        wl.acquire();
                        mFinishHandler.postDelayed(finishRunnable, DEFAULT_SHOWING_TIME);
                    }
                }

                CLog.i("version code :" + Build.VERSION.SDK_INT);

                // execute push noti listenerP
                if (pms.getOnReceivePushListener() != null) {
                    if (pms.getOnReceivePushListener().onReceive(context, extras)) {
                        showNotification(context, extras);
                    }
                } else {
                    showNotification(context, extras);
                }

                CLog.i("ALERT FLAG : " + mPrefs.getString(PREF_ALERT_FLAG));
                CLog.i("NOT POPUP FLAG : " + pushMsg.notPopup);

                if (pushMsg.isShowPopUp() && (FLAG_Y.equals(mPrefs.getString(PREF_ALERT_FLAG)))) {
                    showPopup(context, extras);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * show notification
     *
     * @param context
     * @param extras
     */
    private synchronized void showNotification(Context context, Bundle extras) {

        // push intent sample
        // String msgId = extras.getString(KEY_MSG_ID);
        // String title = extras.getString(KEY_TITLE);
        // String msg = extras.getString(KEY_MSG);
        // String msgType = extras.getString(KEY_MSG_TYPE);
        // String sound = extras.getString(KEY_SOUND);
        // String data = extras.getString(KEY_DATA);
        CLog.i("showNotification");
        if (isImagePush(extras)) {
            showNotificationImageStyle(context, extras);
        } else {
            showNotificationTextStyle(context, extras);
        }
    }

    /**
     * show notification text style
     *
     * @param context
     * @param extras
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private synchronized void showNotificationTextStyle(final Context context, Bundle extras) {
        CLog.i("showNotificationTextStyle");
        // push info
        PushMsg pushMsg = new PushMsg(extras);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder;
        
        // notification
        int iconId = PMSUtil.getIconId(context);
        int largeIconId = PMSUtil.getChangeLargeIconId(context);

        int notificationId = getNotificationId();
        String strNotiChannel = DataKeyUtil.getDBKey(context, DB_NOTI_CHANNEL_ID);
        if (StringUtil.isEmpty(strNotiChannel)) {
            strNotiChannel = "0";
        }

        // Notification channel added
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1 && PMSUtil.getTargetVersion(context) > Build.VERSION_CODES.N_MR1) {
            strNotiChannel = createNotiChannel(context, notificationManager, strNotiChannel);
            builder = new NotificationCompat.Builder(context, strNotiChannel);
            builder.setNumber(0);
        }
        else {
            builder = new NotificationCompat.Builder(context);
//            builder.setNumber(PMSDB.getInstance(context).selectNewMsgCnt());
        }
        // setting ring mode
        final int noti_mode = ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).getRingerMode();

        // checek ring mode
        if (noti_mode == AudioManager.RINGER_MODE_NORMAL) {
            if (FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
                try {
                    int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
                    CLog.d("notiSound : "+notiSound);
                    if (notiSound > 0) {
                        Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
                        builder.setSound(uri);
                    } else {
                        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        builder.setSound(uri);
                    }
                } catch (Exception e) {
                    CLog.e(e.getMessage());
                }
            }
        }

        builder.setContentIntent(makePendingIntent(context, extras, notificationId));
        builder.setAutoCancel(true);
        builder.setContentText(pushMsg.notiMsg);
        builder.setContentTitle(pushMsg.notiTitle);
        builder.setTicker(pushMsg.notiMsg);
        // setting lights color
        builder.setLights(Notification.FLAG_SHOW_LIGHTS, 1000, 2000);
        int ver = Build.VERSION.SDK_INT;
        if (ver >= Build.VERSION_CODES.JELLY_BEAN) {
            builder.setPriority(NotificationCompat.PRIORITY_MAX);
        }

        // set small icon
        CLog.i("small icon :" + iconId);
        if (iconId > 0) {
            builder.setSmallIcon(iconId);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try
            {

                if(!StringUtil.isEmpty(mPrefs.getString(IPMSConsts.PREF_NOTI_BACK_COLOR)))
                {
                    builder.setColor(Color.parseColor(mPrefs.getString(IPMSConsts.PREF_NOTI_BACK_COLOR)));
                }
                // set large icon
                CLog.i("large icon :" + largeIconId);
                if (largeIconId > 0) {
                    builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIconId));
                } else {
                    builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), PMSUtil.getLargeIconId(context)));
                }
            }
            catch (Exception e)
            {
                CLog.e(e.getMessage());
            }
        }

        // check vibe mode
        if (FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
            builder.setDefaults(Notification.DEFAULT_VIBRATE);
        }

        Notification notification = null;
        if (ver >= Build.VERSION_CODES.JELLY_BEAN && FLAG_Y.equals(mPrefs.getString(PREF_USE_BIGTEXT))) {
            notification = new NotificationCompat.BigTextStyle(builder).setBigContentTitle(pushMsg.notiTitle).bigText(pushMsg.notiMsg).build();
        } else {
            notification = builder.build();
        }
        if(PMSUtil.isNotificationToStackable(context)){
            //Stackable
        }else{
            notificationManager.cancelAll();
        }
        notificationManager.notify(notificationId, notification);
    }

    /**
     * show notification image style
     *
     * @param context
     * @param extras
     */
    @SuppressLint("NewApi")
    private synchronized void showNotificationImageStyle(final Context context, Bundle extras) {
        CLog.i("showNotificationImageStyle");
        // push info
        PushMsg pushMsg = new PushMsg(extras);

        // notification
        int iconId = PMSUtil.getIconId(context);
        int largeIconId = PMSUtil.getChangeLargeIconId(context);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder;

        int notificationId = getNotificationId();
        String strNotiChannel = DataKeyUtil.getDBKey(context, DB_NOTI_CHANNEL_ID);
        if (StringUtil.isEmpty(strNotiChannel)) {
            strNotiChannel = "0";
        }

        // Notification channel added
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            strNotiChannel = createNotiChannel(context, notificationManager, strNotiChannel);
            builder = new NotificationCompat.Builder(context, strNotiChannel);
            builder.setNumber(0);
        }
        else {
            builder = new NotificationCompat.Builder(context);
            // builder.setNumber(PMSDB.getInstance(context).selectNewMsgCnt());
        }
        // setting ring mode
        int noti_mode = ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).getRingerMode();

        // checek ring mode
        if (noti_mode == AudioManager.RINGER_MODE_NORMAL) {
            if (FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
                try {
                    int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
                    CLog.d("notiSound : "+notiSound);
                    if (notiSound > 0) {
                        Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
                        builder.setSound(uri);
                    } else {
                        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        builder.setSound(uri);
                    }
                } catch (Exception e) {
                    CLog.e(e.getMessage());
                }
            }
        }
        builder.setContentIntent(makePendingIntent(context, extras, notificationId));
        builder.setAutoCancel(true);
        builder.setContentText(PMSUtil.getBigNotiContextMsg(context));
        builder.setContentTitle(pushMsg.notiTitle);
        builder.setTicker(pushMsg.notiMsg);
        // setting lights color
        builder.setLights(0xFF00FF00, 1000, 2000);
        int ver = Build.VERSION.SDK_INT;
        if (ver >= Build.VERSION_CODES.JELLY_BEAN) {
            builder.setPriority(NotificationCompat.PRIORITY_MAX);
        }

        // set small icon
        CLog.i("small icon :" + iconId);
        if (iconId > 0) {
            builder.setSmallIcon(iconId);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try
            {

                if(!StringUtil.isEmpty(mPrefs.getString(IPMSConsts.PREF_NOTI_BACK_COLOR)))
                {
                    builder.setColor(Color.parseColor(mPrefs.getString(IPMSConsts.PREF_NOTI_BACK_COLOR)));
                }
                // set large icon
                CLog.i("large icon :" + largeIconId);
                if (largeIconId > 0) {
                    builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIconId));
                } else {
                    builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), PMSUtil.getLargeIconId(context)));
                }
            }
            catch (Exception e)
            {
                CLog.e(e.getMessage());
            }
        }

        // check vibe mode
        if (FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
            builder.setDefaults(Notification.DEFAULT_VIBRATE);
        }

        if (mPushImage == null) {
            CLog.e("mPushImage is null");
        }
        // show notification
        Notification notification = new NotificationCompat.BigPictureStyle(builder).bigPicture(mPushImage).setBigContentTitle(pushMsg.notiTitle)
                .setSummaryText(pushMsg.notiMsg).build();

        if(PMSUtil.isNotificationToStackable(context)){
            //Stackable
        }else{
            notificationManager.cancelAll();
        }
        notificationManager.notify(notificationId, notification);
    }
    
    /**
     * make pending intent
     *
     * @param context
     * @param extras
     * @return
     */
    private PendingIntent makePendingIntent(Context context, Bundle extras, int requestCode) {
        
        // notification
        Intent innerIntent = null;
        //		String receiverClass = ProPertiesFileUtil.getString(context, PRO_NOTI_RECEIVER);
        String receiverClass = mPrefs.getString(PREF_NOTI_RECEIVER_CLASS);
        String receiverAction = mPrefs.getString(PREF_NOTI_RECEIVER);
        CLog.i("makePendingIntent receiverClass : " + receiverClass);
        CLog.i("makePendingIntent receiverAction : " + receiverAction);
        if (receiverClass != null) {
            try {
                Class<?> cls = Class.forName(receiverClass);
                innerIntent = new Intent(context, cls).putExtras(extras);
                innerIntent.setAction(receiverAction);
                if (receiverAction != null)
                    innerIntent.setAction(receiverAction);
            } catch (ClassNotFoundException e) {
                CLog.e(e.getMessage());
            }
        }

        if (innerIntent == null) {
            CLog.d("innerIntent == null");
            receiverAction = receiverAction != null ? receiverAction : "com.pms.sdk.notification";
            // setting push info to intent
            innerIntent = new Intent(receiverAction).putExtras(extras);
        }

        if (innerIntent == null) return null;
        
        return PendingIntent.getBroadcast(context, requestCode, innerIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    /**
     * show popup (activity)
     *
     * @param context
     * @param extras
     */
    private synchronized void showPopup(Context context, Bundle extras) {
        try {
            PushMsg pushMsg = new PushMsg(extras);

            if (PMSUtil.getNotiOrPopup(context) && pushMsg.msgType.equals("T")) {
                Toast.makeText(context, pushMsg.notiMsg, Toast.LENGTH_SHORT).show();
            } else {
                Class<?> pushPopupActivity;

                String pushPopupActivityName = mPrefs.getString(PREF_PUSH_POPUP_ACTIVITY);

                if (StringUtil.isEmpty(pushPopupActivityName)) {
                    pushPopupActivityName = DEFAULT_PUSH_POPUP_ACTIVITY;
                }

                try {
                    pushPopupActivity = Class.forName(pushPopupActivityName);
                } catch (ClassNotFoundException e) {
                    CLog.e(e.getMessage());
                    pushPopupActivity = PushPopupActivity.class;
                }
                CLog.i("pushPopupActivity :" + pushPopupActivityName);

                Intent pushIntent = new Intent(context, pushPopupActivity);
                pushIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                pushIntent.putExtras(extras);

                Boolean isPopupActive = PMSUtil.getPopupActivity(context);
                if (isPopupActive) {
                    if (isOtherApp(context)) {
                        context.startActivity(pushIntent);
                    }
                } else {
                    context.startActivity(pushIntent);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings({ "deprecation", "static-access" })
    @SuppressLint("DefaultLocale")
    private boolean isOtherApp (Context context) {
        CLog.i("isOtherApp!!!!");

        //잠금화면인지 확인 - Android 4.0 잠금화면일때 표시가됨 2018.09.05
        boolean isScreenLocked;
        KeyguardManager km = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        if(Build.VERSION.SDK_INT >= 28)
        {
            if(!km.isKeyguardLocked())
            {
                CLog.d("screen is unlocked");
                isScreenLocked = false;
            }
            else
            {
                CLog.d("screen is locked");
                isScreenLocked = true;
            }
        }
        else
        {
            if(!km.inKeyguardRestrictedInputMode())
            {
                CLog.d("screen is unlocked");
                isScreenLocked = false;
            }
            else
            {
                CLog.d("screen is locked");
                isScreenLocked = true;
            }
        }
        if(!isScreenLocked)
        {
            ActivityManager am = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
            String topActivity = "";

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
                ActivityManager.RunningAppProcessInfo currentInfo = null;
                Field field = null;
                try {
                    field = ActivityManager.RunningAppProcessInfo.class.getDeclaredField("processState");
                } catch (NoSuchFieldException e) {
                    CLog.e(e.getMessage());
                }

                List<ActivityManager.RunningAppProcessInfo> appList = am.getRunningAppProcesses();
                for (ActivityManager.RunningAppProcessInfo app : appList) {
                    if (app.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                        Integer state = null;
                        try {
                            state = field.getInt(app);
                        } catch (IllegalAccessException e) {
                            CLog.e(e.getMessage());
                        } catch (IllegalArgumentException e) {
                            CLog.e(e.getMessage());
                        }
                        if (state != null && state == START_TASK_TO_FRONT) {
                            currentInfo = app;
                            break;
                        }
                    }
                }

                // 20180124 hklim null check 추가
                // info를 받아오지 못하면 topActivity는 값이 없기 때문에 false 가 리턴되어 팝업을 띄우지 않는다.
                if (currentInfo != null) {
                    topActivity = currentInfo.processName;
                }
            } else {
                List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(10);
                topActivity = taskInfo.get(0).topActivity.getPackageName();
            }

            CLog.e("TOP Activity : " + topActivity);
            if (FLAG_Y.equals(PMSUtil.isRunnedApp(context))) {
                if ((topActivity.toLowerCase().indexOf("launcher") != -1) // 런처
                        || (topActivity.toLowerCase().indexOf("locker") != -1) // 락커
                        || topActivity.equals("com.google.android.googlequicksearchbox") // 넥서스 5
                        || topActivity.equals("com.cashslide") // 캐시 슬라이드
                        || topActivity.equals("com.kakao.home") // 카카오런처
                        || topActivity.equals(context.getPackageName())) {
                    return true;
                }
            } else {
                if (topActivity.equals(context.getPackageName())) {
                    return true;
                }
            }

        }

        return false;
    }

    /**
     * is image push
     *
     * @param extras
     * @return
     */
    private boolean isImagePush(Bundle extras) {
        try {
            if (!PhoneState.isNotificationNewStyle()) {
                throw new Exception("wrong os version");
            }
            String notiImg = extras.getString(KEY_NOTI_IMG);
            CLog.i("notiImg:" + notiImg);
            if (notiImg == null || "".equals(notiImg)) {
                throw new Exception("no image type");
            }
            return true;
        } catch (Exception e) {
            CLog.e("isImagePush:" + e.getMessage());
            return false;
        }
    }
    @TargetApi(Build.VERSION_CODES.O)
    private String createNotiChannel(Context context, NotificationManager notificationManager, String strNotiChannel)
    {
        NotificationChannel notiChannel = notificationManager.getNotificationChannel(strNotiChannel);
        boolean isShowBadge = false;
        boolean isPlaySound;
        boolean isPlaySoundChanged = false;
        boolean isEnableVibe;
        boolean isShowBadgeOnChannel;
        boolean isPlaySoundOnChannel;
        boolean isEnableVibeOnChannel;

        try
        {
            if(IPMSConsts.FLAG_Y.equals((String) context.getPackageManager().getApplicationInfo(context.getPackageName(),
                    PackageManager.GET_META_DATA).metaData.get(IPMSConsts.META_DATA_NOTI_O_BADGE)))
            {
                isShowBadge = true;
            }
            else
            {
                isShowBadge = false;
            }
        }
        catch (PackageManager.NameNotFoundException e)
        {
            CLog.e(e.getMessage());
        }

        if(IPMSConsts.FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG)))
        {
            isPlaySound = true;
        }
        else
        {
            isPlaySound = false;
        }
        if(IPMSConsts.FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)))
        {
            isEnableVibe = true;
        }
        else
        {
            isEnableVibe = false;
        }
        try
        {
            CLog.d("AppSetting isShowBadge "+ context.getPackageManager().getApplicationInfo(context.getPackageName(),
                    PackageManager.GET_META_DATA).metaData.get(IPMSConsts.META_DATA_NOTI_O_BADGE)+
                    " isPlaySound "+mPrefs.getString(PREF_RING_FLAG)+
                    " isEnableVibe "+mPrefs.getString(PREF_VIBE_FLAG));
        }
        catch (PackageManager.NameNotFoundException e)
        {
            CLog.e(e.getMessage());
        }

        if (notiChannel == null)
        {    //if notichannel is not initialized
            CLog.d("notification initialized");
            notiChannel = new NotificationChannel(strNotiChannel, PMSUtil.getApplicationName(context), NotificationManager.IMPORTANCE_HIGH);
            notiChannel.setShowBadge(isShowBadge);
            notiChannel.enableVibration(isEnableVibe);
            notiChannel.setLightColor(Notification.FLAG_SHOW_LIGHTS);
            notiChannel.setImportance(NotificationManager.IMPORTANCE_HIGH);
            notiChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            if (isEnableVibe)
            {
                notiChannel.setVibrationPattern(new long[]{1000, 1000});
            }

            if (isPlaySound)
            {
                Uri uri;
                try
                {
                    int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
                    if (notiSound > 0)
                    {
                        uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
                        CLog.d("notiSound " + notiSound + " uri " + uri.toString());
                    } else
                    {
                        throw new Exception("default ringtone is set");
                    }
                }
                catch (Exception e)
                {
                    uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    CLog.e(e.getMessage());
                }
                AudioAttributes audioAttributes = new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                        .build();
                notiChannel.setSound(uri, audioAttributes);
                CLog.d("setChannelSound ring with initialize notichannel");
            } else
            {
                notiChannel.setSound(null, null);
                CLog.d("setChannelSound muted with initialize notichannel");
            }
            notificationManager.createNotificationChannel(notiChannel);
            return strNotiChannel;
        } else
        {
            CLog.d("notification is exist");
            return strNotiChannel;
        }
    }
    public int getNotificationId () {
        int notificationId = (int)(System.currentTimeMillis() % Integer.MAX_VALUE);
        return notificationId;
    }


}
