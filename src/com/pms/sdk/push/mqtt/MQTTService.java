package com.pms.sdk.push.mqtt;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.text.TextUtils;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;

import java.net.URI;
import java.net.URISyntaxException;

import androidx.annotation.Nullable;

public class MQTTService extends Service implements IPMSConsts
{
    public static final String INTENT_ACTION = "intentAction";

    private PowerManager.WakeLock wakeLock;
    private final int WAKEUP_HANDLER_ID = 34590;
    private final long WAKEUP_INTERVAL = 1000 * 30;

    private URI serverUri;
    private Intent serviceIntent;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) { return null; }

    @Override
    public void onCreate()
    {
        CLog.d("onCreate()");
        super.onCreate();
        try
        {
            serverUri = new URI(PMSUtil.getMQTTServerUrl(getApplicationContext()));
        }
        catch (URISyntaxException e)
        {
            CLog.e(e.getMessage());
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        CLog.d("onStartCommand()");
        CLog.d("startId: " + startId);

        try
        {
            if(serverUri == null)
            {
                serverUri = new URI(PMSUtil.getMQTTServerUrl(getApplicationContext()));
            }

            String serverProtocol = serverUri.getScheme();
            String serverHost = serverUri.getHost();
            int serverPort = serverUri.getPort();

            //Wakeup Screen
            if(intent!=null)
            {
                serviceIntent = intent;
                String action = intent.getStringExtra(INTENT_ACTION);
                CLog.i("receivedAction : "+action);
                if (ACTION_START.equals(action) || ACTION_FORCE_START.equals(action))
                {
                    PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                    wakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "myapp:mywakelocktag");
                    if (!pm.isScreenOn())
                    {
                        wakeLock.acquire();
                        wakeUpHandler.sendEmptyMessageDelayed(WAKEUP_HANDLER_ID, WAKEUP_INTERVAL);
                    }
                }
            }

            //MQTT Bind
            String clientId = PMSUtil.getAppUserId(getApplicationContext());
            if(!TextUtils.isEmpty(clientId))
            {
//                MQTTBinder.newInstance(getApplicationContext()).withInfo(clientId, serverProtocol, serverHost, serverPort).start(new MQTTBinder.IMQTTServiceCallback()
//                {
//                    // connection complete
//                    @Override
//                    public void onConnect(MQTTBinder.ConnectInfo child)
//                    {
//                        child.closeToAfterMillisecond(MQTTScheduler.DEFAULT_KEEP_ALIVE_TIME);
//                    }
//
//                    // connection close
//                    @Override
//                    public void onFinish()
//                    {
//                        try
//                        {
//                            CLog.w("[[ service stop ]]");
//                            stopService(serviceIntent);
//                        }
//                        catch (Exception e) { }
//                    }
//                });
            }
            else
            {
                try
                {
                    CLog.w("AppUserId is null, Please DeviceCert first");
                    stopService(serviceIntent);
                }
                catch (Exception e) { }
            }
        }
        catch (Exception e)
        {
            CLog.e(e.getMessage());
        }

        return START_NOT_STICKY;
    }

    private Handler wakeUpHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            switch (msg.what)
            {
                case WAKEUP_HANDLER_ID:
                {
                    this.post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            if (wakeLock != null && wakeLock.isHeld())
                            {
                                wakeLock.release();
                            }
                        }
                    });

                    removeMessages(WAKEUP_HANDLER_ID);
                    break;
                }
            }

        }
    };

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        try
        {
            if (MQTTBinder.getInstance().getChild() != null)
            {
                MQTTBinder.getInstance().getChild().cancelListener();
            }
        }
        catch (Exception e) { }

        CLog.i("onDestroy()");
    }
}
