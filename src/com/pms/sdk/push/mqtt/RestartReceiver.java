package com.pms.sdk.push.mqtt;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.StringUtil;

import java.net.URI;

import static android.content.Context.MODE_PRIVATE;

/**
 * Private PUSH Restart Receiver
 * @author haewon
 *
 */
public class RestartReceiver extends BroadcastReceiver implements IPMSConsts
{
	public static final long DEFAULT_SCHEDULE_TIME = 1000 * 60 * 1;

	@Override
	public synchronized void onReceive (final Context context, Intent intent)
	{
		if(intent != null)
		{
			String action = intent.getAction();
//			MQTTScheduler scheduler = MQTTScheduler.getInstance();

			if(!TextUtils.isEmpty(action))
			{
				int device = Build.VERSION.SDK_INT;
				Log.d("TAG", "Device build version: " + device + ", Release version: " + Build.VERSION.RELEASE);

				if(ACTION_FORCE_START.equals(action))
				{
					try
					{
						String url = PMSUtil.getMQTTServerUrl(context);
						if (TextUtils.isEmpty(url))
						{
							CLog.w("url is empty");
							return;
						}

						URI serverUri = new URI(url);

						String serverProtocol = serverUri.getScheme();
						String serverHost = serverUri.getHost();
						int serverPort = serverUri.getPort();

						String clientId = PMSUtil.getAppUserId(context);
						if(!TextUtils.isEmpty(clientId))
						{
							int keepAlive = (int) DEFAULT_SCHEDULE_TIME / 1000;
							MQTTBinder.newInstance(context).withInfo(clientId, serverProtocol, serverHost, serverPort, keepAlive);
							MQTTBinder.ConnectInfo service = MQTTBinder.getInstance().getChild();
							if (service != null)
							{
								service.start(new MQTTBinder.IMQTTServiceCallback()
								{
									// connection complete
									@Override
									public void onConnect(MQTTBinder.ConnectInfo child)
									{
										child.closeToAfterMillisecond(DEFAULT_SCHEDULE_TIME);
									}

									// connection close
									@Override
									public void onFinish()
									{
										CLog.d("[ Binder finish ]");
									}
								});
							}
						}
					}
					catch (Exception e)
					{
						CLog.e("Receiver error(mqtt) " + e.getMessage());
					}
				}
				else
				{
					CLog.w("[ RestartReceiver ] not support action: " + action);
				}
			}
			else
			{
				CLog.w("[ RestartReceiver ] intent action is empty");
			}
		}
		else
		{
			CLog.w("[ RestartReceiver ] intent is null");
		}
	}
}