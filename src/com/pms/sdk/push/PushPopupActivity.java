package com.pms.sdk.push;

import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONObject;

import android.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.PushMsg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.PMSPopupUtil;
import com.pms.sdk.common.util.PMSPopupUtil.btnEventListener;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;

/**
 * push popup activity
 * 
 * @author Yang
 * @since 2013.12.11
 */
public class PushPopupActivity extends Activity implements IPMSConsts {

	private static Activity PushPopupActivity = null;

	private LayoutInflater inflate = null;

	private Context mContext = null;

	private String[] mStrBtnName = null;

	private RelativeLayout mRmainLayout = null;

	private LinearLayout.LayoutParams mlilp = null;

	private WebView mWv = null;

	private TextView mContextView = null;

	private PushMsg pushMsg = null;

	private Timer mPopupTimer = null;

	private int[] mnResId = null;

	private int mnBottomTextBtnCount = 0;
	private int mnBottomRichBtnCount = 0;
	private int mnMaxWidth = 0;
	private int pushPopupShowingTime = 0;
	private int mShowingTime = 0;

	private final static int FILL_PARENT = -1;
	private final static int WRAP_CONTENT = -2;

	private PMSPopupUtil pMSPopupUtil = null;

	@SuppressLint("ResourceAsColor")
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		inflate = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		mContext = this;
		pMSPopupUtil = new PMSPopupUtil();
		pMSPopupUtil.setParmSetting(mContext);
		pMSPopupUtil.mResData = pMSPopupUtil.getSaveMapData();

		PushPopupActivity = PushPopupActivity.this;
		pMSPopupUtil.setActivity(PushPopupActivity);

		int[] deviceSize = PhoneState.getDeviceSize(this);
		mnMaxWidth = (int) Math.round((deviceSize[0] < deviceSize[1] ? deviceSize[0] : deviceSize[1]) * 0.8);

		mRmainLayout = new RelativeLayout(this);
		mRmainLayout.setLayoutParams(new RelativeLayout.LayoutParams(WRAP_CONTENT, FILL_PARENT));
		mRmainLayout.setGravity(Gravity.CENTER);
		mRmainLayout.setPadding(50, 50, 50, 50);

		mPopupTimer = new Timer();

		Prefs pref = new Prefs(this);
		pushPopupShowingTime = pref.getInt(PREF_PUSH_POPUP_SHOWING_TIME);

		mContext.registerReceiver(changeContentReceiver, new IntentFilter(RECEIVER_CHANGE_POPUP));

		settingPopupUI(getIntent());

		setContentView(mRmainLayout);
	}

	@Override
	protected void onResume () {
		try {
			WebView.class.getMethod("onResume").invoke(mWv);
			mWv.resumeTimers();
		} catch (Exception e) {
		}
		super.onResume();
		this.overridePendingTransition(0, 0);
	}

	@Override
	public void onDestroy () {
		super.onDestroy();
		if (mWv != null) {
			mWv.loadData("", "Text/html", "UTF8");
		}
		mContext.unregisterReceiver(changeContentReceiver);
	}

	@Override
	protected void onNewIntent (Intent intent) {
		super.onNewIntent(intent);
		settingPopupUI(intent);
	}

	private void dataSetting () {
		mnBottomTextBtnCount = pMSPopupUtil.getBottomTextBtnCount();
		mnBottomRichBtnCount = pMSPopupUtil.getBottomRichBtnCount();
		mnResId = pMSPopupUtil.getBottomBtnImageResource();
		mStrBtnName = pMSPopupUtil.getBottomBtnTextName();
	}

	public void settingPopupUI (Intent intent) {
		pushMsg = new PushMsg(intent.getExtras());

		CLog.i(pushMsg + "");

		pMSPopupUtil.setMsgId(pushMsg.msgId);
		if (pMSPopupUtil.getXmlAndDefaultFlag()) {
			View view = null;
			if (Msg.TYPE_H.equals(pushMsg.msgType) || Msg.TYPE_L.equals(pushMsg.msgType)) {
				view = inflate.inflate(pMSPopupUtil.getLayoutXMLRichResId(), null);
				getXMLRichPopup(view, pushMsg.message);

			} else {
				view = inflate.inflate(pMSPopupUtil.getLayoutXMLTextResId(), null);
				getXMLTextPopup(view, pushMsg.notiMsg);
			}

			mRmainLayout.addView(view);
		} else {
			dataSetting();
			LinearLayout backGroundll = new LinearLayout(mContext);
			mlilp = new LinearLayout.LayoutParams(mnMaxWidth, WRAP_CONTENT);
			mlilp.gravity = Gravity.CENTER;
			backGroundll.setLayoutParams(mlilp);
			backGroundll.setOrientation(LinearLayout.VERTICAL);

			if (pMSPopupUtil.mResData.containsKey(POPUP_BACKGROUND_COLOR)) {
				backGroundll.setBackgroundColor(Color.parseColor(pMSPopupUtil.getPopUpBackColor()));
			} else if (pMSPopupUtil.mResData.containsKey(POPUP_BACKGROUND_RES_ID)) {
				backGroundll.setBackgroundResource(pMSPopupUtil.getPopupBackImgResource());
			}

			if (Msg.TYPE_H.equals(pushMsg.msgType) || Msg.TYPE_L.equals(pushMsg.msgType)) {
				backGroundll.addView(getRichPopup(pushMsg.message));
			} else {
				backGroundll.addView(getTextPopup(pushMsg.notiMsg));
			}

			mRmainLayout.addView(backGroundll);
		}

		mShowingTime = pushPopupShowingTime;
		mPopupTimer.schedule(new TimerTask() {
			@Override
			public void run () {
				if (mShowingTime > -1) {
					mShowingTime -= 1000;
				} else {
					mPopupTimer.cancel();
					finish();
				}
			}
		}, 0, 1000);
	}

	private View getTopLayoutSetting () {
		LinearLayout topMainll = new LinearLayout(mContext);
		mlilp = new LinearLayout.LayoutParams(FILL_PARENT, WRAP_CONTENT);
		mlilp.gravity = Gravity.CENTER;

		topMainll.setOrientation(LinearLayout.VERTICAL);
		topMainll.setLayoutParams(mlilp);
		topMainll.setPadding(15, 15, 15, 15);

		if (pMSPopupUtil.mResData.containsKey(TOP_BACKGROUND_COLOR)) {
			topMainll.setBackgroundColor(Color.parseColor(pMSPopupUtil.getTopBackColor()));
		} else if (pMSPopupUtil.mResData.containsKey(TOP_BACKGROUND_RES_ID)) {
			topMainll.setBackgroundResource(pMSPopupUtil.getTopBackImgResource());
		}

		mlilp = new LinearLayout.LayoutParams(FILL_PARENT, WRAP_CONTENT);
		mlilp.gravity = Gravity.CENTER;
		if (TOP_TITLE_TEXT.equals(pMSPopupUtil.getTopTitleType())) {
			TextView txt = new TextView(mContext);
			txt.setLayoutParams(mlilp);
			txt.setGravity(Gravity.CENTER);
			txt.setTextColor(Color.parseColor(pMSPopupUtil.getTopTitleTextColor()));
			txt.setTextSize(pMSPopupUtil.getTopTitleTextSize());
			txt.setText(pMSPopupUtil.getTopTitleName());

			topMainll.addView(txt);
		} else if (TOP_TITLE_IMAGE.equals(pMSPopupUtil.getTopTitleType())) {
			ImageView iv = new ImageView(mContext);
			iv.setLayoutParams(mlilp);
			iv.setScaleType(ImageView.ScaleType.FIT_CENTER);
			iv.setImageResource(pMSPopupUtil.getTopTitleImgResource());

			topMainll.addView(iv);
		}

		return topMainll;
	}

	private View getTextPopup (String msg) {
		CLog.i("getTextPopup");
		LinearLayout contentMainll = new LinearLayout(mContext);
		mlilp = new LinearLayout.LayoutParams(0, WRAP_CONTENT);
		mlilp.weight = 1;

		contentMainll.setOrientation(LinearLayout.VERTICAL);
		contentMainll.setLayoutParams(new LayoutParams(FILL_PARENT, FILL_PARENT));

		if (pMSPopupUtil.mResData.containsKey(CONTENT_BACKGROUND_COLOR)) {
			contentMainll.setBackgroundColor(Color.parseColor(pMSPopupUtil.getContentBackColor()));
		} else if (pMSPopupUtil.mResData.containsKey(CONTENT_BACKGROUND_RES_ID)) {
			contentMainll.setBackgroundResource(pMSPopupUtil.getContentBackImgResource());
		}

		mlilp = new LinearLayout.LayoutParams(FILL_PARENT, WRAP_CONTENT);
		mlilp.setMargins(15, 15, 15, 15);
		mContextView = new TextView(mContext);
		mContextView.setLayoutParams(mlilp);
		mContextView.setTextColor(Color.parseColor(pMSPopupUtil.getContentTextColor()));
		mContextView.setTextSize(pMSPopupUtil.getContentTextSize());
		mContextView.setText(msg);

		if (pMSPopupUtil.getTopLayoutFlag()) {
			contentMainll.addView(getTopLayoutSetting());
		}
		contentMainll.addView(mContextView);
		contentMainll.addView(bottomLayoutSetting());

		return contentMainll;
	}

	private void getXMLTextPopup (View view, String msg) {
		CLog.i("getXMLTextPopup");

		mContextView = ((TextView) pMSPopupUtil.getSerachView(view, TEXTVIEW, "ContentTextView"));
		mContextView.setText(msg);

		String[] viewtype = pMSPopupUtil.getXMLTextButtonType();
		String[] viewTagName = pMSPopupUtil.getXMLTextButtonTagName();
		for (int i = 0; i < viewtype.length; i++) {
			final int count = i;
			View buttonView = pMSPopupUtil.getSerachView(view, pMSPopupUtil.getViewType(viewtype[i]), viewTagName[i]);
			buttonView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick (View v) {
					btnEventListener btnEvent = pMSPopupUtil.getTextBottomBtnClickListener(count);
					btnEvent.response();
				}
			});
		}
	}

	private View getRichPopup (String url) {
		CLog.i("getRichPopup");
		mlilp = new LinearLayout.LayoutParams(0, WRAP_CONTENT);
		mlilp.weight = 1;

		LinearLayout contentMainll = new LinearLayout(mContext);
		contentMainll.setOrientation(LinearLayout.VERTICAL);
		contentMainll.setLayoutParams(new LayoutParams(FILL_PARENT, FILL_PARENT));

		if (pMSPopupUtil.mResData.containsKey(CONTENT_BACKGROUND_COLOR)) {
			contentMainll.setBackgroundColor(Color.parseColor(pMSPopupUtil.getContentBackColor()));
		} else if (pMSPopupUtil.mResData.containsKey(CONTENT_BACKGROUND_RES_ID)) {
			contentMainll.setBackgroundResource(pMSPopupUtil.getContentBackImgResource());
		}

		mlilp = new LinearLayout.LayoutParams(FILL_PARENT, WRAP_CONTENT);
		mlilp.weight = 1;
		mWv = new WebView(mContext);
		mWv.setLayoutParams(mlilp);

		final ProgressBar prg = new ProgressBar(mContext, null, android.R.attr.progressBarStyleHorizontal);
		prg.setProgress(50);
		prg.setLayoutParams(mlilp);

		webViewSetting(prg, url);

		if (pMSPopupUtil.getTopLayoutFlag()) {
			contentMainll.addView(getTopLayoutSetting());
		}
		contentMainll.addView(mWv);
		contentMainll.addView(prg);
		contentMainll.addView(bottomLayoutSetting());

		return contentMainll;
	}

	private void getXMLRichPopup (View view, String url) {
		CLog.i("getXMLRichPopup");

		ProgressBar prg = (ProgressBar) pMSPopupUtil.getSerachView(view, PROGRESSBAR, "ContentProgressBar");
		mWv = (WebView) pMSPopupUtil.getSerachView(view, WEBVIEW, "ContentWebView");
		webViewSetting(prg, url);

		String[] viewtype = pMSPopupUtil.getXMLRichButtonType();
		String[] viewTagName = pMSPopupUtil.getXMLRichButtonTagName();
		for (int i = 0; i < viewtype.length; i++) {
			final int count = i;
			View buttonView = pMSPopupUtil.getSerachView(view, pMSPopupUtil.getViewType(viewtype[i]), viewTagName[i]);
			buttonView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick (View v) {
					btnEventListener btnEvent = pMSPopupUtil.getTextBottomBtnClickListener(count);
					btnEvent.response();
				}
			});
		}
	}

	@SuppressLint("SetJavaScriptEnabled")
	private void webViewSetting (final ProgressBar prg, String url) {
		mWv.clearCache(true);
		mWv.clearHistory();
		mWv.clearFormData();
		mWv.clearView();

		mWv.setInitialScale(1);
		mWv.setBackgroundColor(R.style.Theme_Translucent);
		mWv.setHorizontalScrollBarEnabled(false);
		mWv.setVerticalScrollBarEnabled(false);

		WebSettings settings = mWv.getSettings();
		settings.setJavaScriptEnabled(true);
		settings.setJavaScriptCanOpenWindowsAutomatically(true);
		settings.setSupportMultipleWindows(true);
		settings.setUseWideViewPort(true);
		settings.setLoadWithOverviewMode(true);
		settings.setPluginState(PluginState.ON);
		// settings.setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		settings.setRenderPriority(RenderPriority.HIGH);
		settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
		settings.setGeolocationEnabled(true);
		settings.setDomStorageEnabled(true);
		settings.setUseWideViewPort(true);

		mWv.setWebChromeClient(new WebChromeClient() {
			@Override
			public void onProgressChanged (WebView view, int newProgress) {
				super.onProgressChanged(view, newProgress);
				prg.setProgress(newProgress);
				if (newProgress >= 100) {
					prg.setVisibility(View.GONE);
				}
			}
		});

		PMSWebViewBridge pmsBridge = new PMSWebViewBridge(mContext);
		mWv.addJavascriptInterface(pmsBridge, "pms");

		// setting html to webview
		if (Msg.TYPE_L.equals(pushMsg.msgType) && url.startsWith("http")) {
			mWv.loadUrl(url);
		} else {
			mWv.loadData(url, "text/html; charset=utf-8", null);
		}

		mWv.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch (View v, MotionEvent event) {
				if (mShowingTime < pushPopupShowingTime) {
					mShowingTime = pushPopupShowingTime;
				}
				if (event.getAction() != MotionEvent.ACTION_MOVE) {
					((WebView) v).setWebViewClient(new WebViewClient() {
						@Override
						public boolean shouldOverrideUrlLoading (WebView view, String url) {
							JSONObject read = new JSONObject();
							try {
								read.put("msgId", pushMsg.msgId);
								read.put("workday", DateUtil.getNowDate());
							} catch (Exception e) {
								e.printStackTrace();
							}
							CLog.d(String.format("msgId:%s", pushMsg.msgId));

							PMSUtil.arrayToPrefs(mContext, PREF_READ_LIST, read);
							CLog.i("webViewClient:url=" + url);
							Intent intent = new Intent(Intent.ACTION_VIEW);
							Uri uri = Uri.parse(url);
							intent.setData(uri);
							intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
							startActivity(intent);
							return true;
						}
					});
				}
				return false;
			}
		});

		mWv.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading (WebView view, String url) {
				CLog.i("webViewClient:url=" + url);
				return super.shouldOverrideUrlLoading(view, url);
			}
		});
	}

	private View bottomLayoutSetting () {
		LinearLayout bottomMainll = new LinearLayout(mContext);
		mlilp = new LinearLayout.LayoutParams(FILL_PARENT, WRAP_CONTENT);
		mlilp.gravity = Gravity.CENTER;
		bottomMainll.setLayoutParams(mlilp);
		bottomMainll.setOrientation(LinearLayout.HORIZONTAL);
		bottomMainll.setGravity(Gravity.CENTER);

		if (pMSPopupUtil.mResData.containsKey(BOTTOM_BACKGROUND_COLOR)) {
			bottomMainll.setBackgroundColor(Color.parseColor(pMSPopupUtil.getBottomBackColor()));
		} else if (pMSPopupUtil.mResData.containsKey(BOTTOM_BACKGROUND_RES_ID)) {
			bottomMainll.setBackgroundResource(pMSPopupUtil.getBottomBackImgResource());
		}

		int count = 0;
		if (Msg.TYPE_H.equals(pushMsg.msgType) || Msg.TYPE_L.equals(pushMsg.msgType)) {
			count = mnBottomRichBtnCount;
		} else {
			count = mnBottomTextBtnCount;
		}

		LinearLayout[] bottomSubll = new LinearLayout[count];
		if (count != 0) {
			for (int i = 0; i < count; i++) {
				bottomSubll[i] = (LinearLayout) bottomLinearLayoutSetting(mStrBtnName[i], i, count);
				bottomMainll.addView(bottomSubll[i]);
			}
		}

		return bottomMainll;
	}

	private View bottomLinearLayoutSetting (String btnName, final int count, int totalcount) {
		LinearLayout ll = new LinearLayout(mContext);
		if (totalcount == 2) {
			mlilp = new LinearLayout.LayoutParams(0, WRAP_CONTENT);
			mlilp.weight = 50;
		} else {
			mlilp = new LinearLayout.LayoutParams(mnMaxWidth / 2, WRAP_CONTENT);
		}

		mlilp.setMargins(15, 15, 15, 15);
		ll.setLayoutParams(mlilp);

		View view = null;
		if (pMSPopupUtil.mResData.containsKey(BOTTOM_BTN_RES_ID)) {
			ll.setBackgroundResource(mnResId[count]);
			if (pMSPopupUtil.getBottomTextViewFlag()) {
				ll.addView(bottomImageBackToText(btnName));
			}
			view = ll;
		} else {
			Button btn = new Button(mContext);
			btn.setText(btnName);
			ll.addView(btn, new LinearLayout.LayoutParams(FILL_PARENT, WRAP_CONTENT));
			view = btn;
		}

		if (Msg.TYPE_H.equals(pushMsg.msgType) || Msg.TYPE_L.equals(pushMsg.msgType)) {
			view.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick (View v) {
					btnEventListener btnEvent = pMSPopupUtil.getRichBottomBtnClickListener(count);
					btnEvent.response();
				}
			});
		} else {
			view.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick (View v) {
					btnEventListener btnEvent = pMSPopupUtil.getTextBottomBtnClickListener(count);
					btnEvent.response();
				}
			});
		}
		return ll;
	}

	private View bottomImageBackToText (String imgText) {
		TextView text = new TextView(mContext);
		text.setLayoutParams(new LayoutParams(FILL_PARENT, WRAP_CONTENT));
		text.setText(imgText);
		text.setTextColor(Color.parseColor(pMSPopupUtil.getBottomBtnTextColor()));
		text.setGravity(Gravity.CENTER);
		text.setPadding(10, 10, 10, 10);
		text.setTextSize(pMSPopupUtil.getBottomBtnTextSize());

		return text;
	}

	BroadcastReceiver changeContentReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive (Context context, Intent intent) {
			pushMsg = new PushMsg(intent.getExtras());

			new Handler().post(new Runnable() {
				@Override
				public void run () {
					CLog.e("Push Popup Receiver!!!");
					if (Msg.TYPE_T.equals(pushMsg.msgType)) {
						mContextView.setText(pushMsg.notiMsg);
					} else if (Msg.TYPE_H.equals(pushMsg.msgType) || Msg.TYPE_L.equals(pushMsg.msgType)) {
						mWv.loadData("", "Text/html", "UTF8");

						if (Msg.TYPE_L.equals(pushMsg.msgType) && pushMsg.message.startsWith("http")) {
							mWv.loadUrl(pushMsg.message);
						} else {
							mWv.loadDataWithBaseURL(null, pushMsg.message, "text/html", "utf-8", null);
						}
					}
				}
			});
		}
	};
}