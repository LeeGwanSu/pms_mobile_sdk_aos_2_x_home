package com.pms.sdk;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.MsgGrp;
import com.pms.sdk.common.security.TlsSocketFactory;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.mqtt.MQTTService;
import com.pms.sdk.push.mqtt.RestartReceiver;

import java.io.Serializable;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * @since 2012.12.28
 * @author erzisk
 * @description pms (solution main class)     home&shopping
 * @version [2013.10.07] pushMsg.java 생성 - push 데이터 key 변경<br>
 *          [2013.10.11] android-query -> volley 로 library 변경<br>
 *          [2013.10.14] push notification 출력 변경<br>
 *          [2013.10.16 15:09] logout시에 저장된 cust_id삭제<br>
 *          [2013.10.21 10:30] api call시에, appUserId 또는 encKey가 없을 시 에러 추가<br>
 *          [2013.10.21 11:22] api call시에, paging이 존재하는 api(newMsg.m)일 경우, 가장 마지막 호출에서만 apiCallback이 이뤄지게끔 수정<br>
 *          [2013.10.21 18:43] push 데이터 key 변경 (IPMSConsts)<br>
 *          [2013.10.25 14:15] deviceCert시 token이 없는경우 'noToken'으로 넘김<br>
 *          [2013.10.29 17:03] deviceCert시 token이 "noToken"일 경우도 loop<br>
 *          [2013.10.30 11:26] push 수신 시 notImg exception 처리 추가<br>
 *          [2013.11.01 11:15] CLog 새로운 버전으로 적용 및 소스에서 TAG 삭제!!!!<br>
 *          [2013.11.06 15:02] deviceCert parameter에 sessCnt추가<br>
 *          [2013.11.06 16:07] logined_cust_id 추가, deviceCert 및 loginPms시에 cust_id와 logined_cust_id를 비교하여 db delete 결정<br>
 *          [2013.11.15 10:38] APIManager에서 error발생 시에도 apiCalback수행<br>
 *          [2013.11.19 15:36] push popup finish thread 추가<br>
 *          [2013.11.26 09:22] click 관련 추가<br>
 *          [2013.11.26 16:42] MQTT Client 변경<br>
 *          [2013.11.26 21:13] deviceCert시 read 및 click api 호출<br>
 *          [2013.11.27 17:51] MQTT Client 사용시 로그 저장 루틴 추가 <br>
 *          [2013.12.04 17:47] push 수신 시 해당 msgId가 존재하면 noti하지 않게 변경<br>
 *          [2013.12.05 15:35] push popup 유지 시간 설정 추가 PMS.setPushPopupShowingTime(int)<br>
 *          [2013.12.10 18:21] MQTT KeepAlive Time mata-data에서 설정하도록 수정함.<br>
 *          [2013.12.11 16:07] PMSDB.deleteExpireMsg 로직 변경<br>
 *          [2013.12.12 18:34] Private Flag 추가<br>
 *          [2013.12.16 10:33] PushReceiver.onMessage에 synchronized추가<br>
 *          [2013.12.16 16:36] Push Popup 사용자가 설정할수 있도록 변경함. <br>
 *          [2013.12.17 13:19] MQTT Client 중복 실행 수정함. <br>
 *          [2014.01.13 09:31] MQTT Client 안정화 & Connect Check 부분 수정함. <br>
 *          [2014.02.07 17:12] Collect 부분 수정함. <br>
 *          [2014.02.11 10:18] CollectLog getParam 부분 수정함. <br>
 *          [2014.02.21 17:23] MQTT Client 안정화 및 Prvate 서버 Protocol 적용 가능하게 수정함.<br>
 *          [2014.02.25 10:12] 안쓰는 method 삭제함.<br>
 *          [2014.03.03 10:36] 팝업 셋팅 default 설정 가능하게 변경.<br>
 *          [2014.03.05 15:31] MQTT Client 관련 Service 추가.<br>
 *          [2014.03.05 20:37] MQTT Client STOP 재시작하는 현상 수정.<br>
 *          [2014.03.13 10:47] MQTT Client keepAlive 수정함. sleep mode 삭제.<br>
 *          [2014.03.28 09:29] DisusePms.m API 추가함.<br>
 *          [2014.05.14 20:06] DisusePms.m API 삭제 & Popup 설정값 추가.<br>
 *          [2014.06.02 20:39] Popup WebView background 수정함. <br>
 *          [2014.06.05 18:24] 다른앱 실행시 팝업창 미 노출. <br>
 *          [2014.06.09 11:05] 팝업창에 대한 Flag 값 추가함. <br>
 *          [2014.06.11 17:32] 텍스트 푸쉬일때 toast 메세지로 전환할수 있도록 수정. <br>
 *          [2014.06.16 20:44] 다른앱 사용시 안뜨는 플래그 추가함. <br>
 *          [2014.06.24 09:10] ReadMSg UserMsgId로 가능하게 수정함. <br>
 *          [2014.07.02 15:28] 연속 발송시 1건씩 빠지는 문제 수정함. <br>
 *          [2014.07.03 12:28] MQTT Wake Lock 쪽 예외처리 추가함. <br>
 *          [2014.07.08 14:28] DeviceCert시 Config 값이 틀리면 SetConfig 호출 <br>
 *          [2014.07.31 16:26] Push 왔을 경우 null or "" 공백시 리턴처리추가함. <br>
 *          [2014.08.01 15:07] Collect Log 버그 수정함. <br>
 *          [2014.08.25 09:18] MQTT Client 버그 수정. <br>
 *          [2014.09.16 11:36] Notification Priority Level 2로 수정. <br>
 *          [2014.10.21 14:39] Android OS 5.0에서 상태창 이미지 표시 안되는 버그 수정함. <br>
 *          [2015.03.12 09:11] 엔진에서 오는 값으로 팝업창을 뛰울것인지 안뛰울것인지 결정. <br>
 *          [2015.03.13 16:21] 구글 정책에 따른 앱 실행중일떄만 팝업창 노출. <br>
 *          [2015.03.24 20:05] 03월12일 버전으로 교체함. <br>
 *          [2015.06.16 15:40] setConfig Flag 추가함. <br>
 *          [2016.04.06 18:12] Notification Big Text모드를 추가함. <br>
 *          [2016.04.18 11:00] setConfig Flag 삭제함.<br>
 *          [2016.04.29 09:47] X509TrustManager 코드 삭제하고 대응 코드 삽입함.<br>
 *          [2016.07.26 15:52] Notification BigText시 오류나는 OS 버전별로 뛰우도록 수정함.<br>
 *          [2016.08.03 13:43] PushMsg 파싱중 오류나는 부분 예외처리함.<br>
 *          [2016.08.04 13:10] PushMessage 받을 때 Payload값 보이도록 수정함.<br>
 *          [2016.08.08 13:50] PushReceive 중 Action값 추가함.<br>
 *          [2016.09.08 14:41] Notification 중 안읽은 메세지 표시 기능 삭제함.<br>
 *          [2016.11.29 09:45] doze mode broadcast 패치1 적용 <br>
 *          [2016.12.14 13:21] Icon에 대한 버그 사항 수정함.<br>
 *          [2017.03.16 09:41] newMsg중 response PushImg 추가함.<br>
 *          [2017.03.31 16:54] DB 업그레이드 OnCreate문 삭제함.<br>
 *          [2017.05.12 19:38] *UUID 로직변경 *silentMsg 처리 및 팝업처리 수정
 *          [2017.05.15 19:25] *LargeIcon에 대한 수정 (setLargeNotiIcon )
 *          [2017.05.23 16:57] *private PushMsg parser 수정(silentMsg bug수정)
 *          [2017.05.30 13:49] *Popup Flag bug fixed
 *          [2017.05.30 22:16] *UUID 관련 수정 (ADID add)
 *          [2017.07.13 16:37] *Popup Flag bug fixed (API 22이상은 앱구분없이 팝업이 뜸)
 *          [2017.07.14 17:42] *Popup Flag bug fixed (API 22이상은 팝업 안뜸)
 *          [2017.07.19 14:06] *Rich Push MsgGrpCode 관련 수정
 *          [2017.09.19 11:06] *Volley 관련 수정
 *          [2017.10.16 17:26] *DB Exception 발생 문제 수정, QueueManager 에서 기존에 유지되던
 *                              queue 를 가져와 ImageLoader 에서 생성하지 않고 사용토록 수정
 *          [2017.10.26 10:35] * Queue Manager 추가 Single tone 으로 관리 (Image Loader 및 APIManager)
 *                              DB 중복 Open으로 인해 강제종료 문제 수정
 *                              PushPopup 발생 시 강제 종료 문제 수정
 *                              UUID 생성 로직 변경, Device정보를 이용하여 UUID를 작성하는 경로 이전에 Device 정보 없이 발급하는 방법으로
 *                              UUID 발급, 발급 실패 시 Device정보를 필요로 하는 UUID 발급 방법으로 발급 시도함 <br>
 *          [2018.01.15 13:43] *LOLLIPOP(21) 버전 이상일 경우에 JobService를 이용하여 MQTTService 실행.
 *                              JobService 에서 WakeLock을 관리하기 때문에 JobService에서는 WakeLock 삭제.
 *          [2018.04.26 17:16] *MQTTJobService로 동작하지 않도록 수정, wakelock 관련 강제종료 이슈 수정
 *          [2018.06.08 13:29] *Android 8.0 대응 (Intent 실행 대응)
 *          [2018.06.11 10:10] *SilentPushReceiver 추가
 *          [2018.07.17 12:51] Android 8.0 대응 (JobScheduler, NotificationChannel)
 *          [2018.08.09 14:32] 암시적 인텐트 제한 대응, WRITE_STORAGE_PERMISSION 안쓰도록 수정, PopupUtil Preference에 저장하도록 수정, MQTT 라이브러리 교체, 그에따라 MQTT 서비스 변경
 *          [2018.08.10 11:37] MQTTService 예외 처리 추가, 로그 추가
 *          [2018.08.14 09:47] MQTT 버그 수정
 *          [2018.08.24 18:32] MQTT 안정화
 *          [2018.08.27 11:35] MQTT 안정화 (setusername 예외처리)
 *          [2018.08.28 13:29] MQTT isOnline 조건 제거, setNotiBackColor 추가
 *          [2018.08.30 16:44] MQTT 간소화 적용
 *          [2018.09.11 11:09] MQTT 간소화 오류 수정, 잠금화면 체크 추가
 *          [2018.09.12 10:21] 벨소리 볼륨 대응, MQTT 백그라운드로 전환(WakeUpPush 쓰므로)
 *          [2018.09.12 16:47] startForeground 전부 제거
 *          [2018.09.17 10:58] NotificationId 원복, AlarmPingSender 방어로직 추가, 암시적 Intent 대응
 *          [2018.09.17 13:29] MQTT 재시도 횟수 제한, Thread 제한 추가
 *          [2018.09.17 14:33] Doze 모드시에 푸시 못받아서 startForeground 원복
 *          [2018.09.17 17:27] MQTT 안정화 FIX, Doze 모드 시에도 받음 support library 26.0.0
 *          [2018.09.20 13:21] MQTT WakeupTime 버전 적용
 *          [2018.10.05 18:44] MQTT WakeupTime 30초 버전
 *          [2018.10.11 12:48] MQTT Channel 미리 생성 버전
 *          [2018.10.11 14:00] MQTT Channel 미리 생성 롤백
 *          [2018.10.11 17:42] startForegroundService 전 stopService 추가
 *          [2018.10.15 16:34] MQTT 채널 수정
 *          [2018.10.16 10:09] MQTT 포그라운드 부분 수정
 *          [2018.10.16 14:37] MQTT 하이브리드 적용
 *          [2018.10.18 11:42] MQTT 최종 버전 적용
 *          [2018.10.22 10:57] 8.0 이상에만 Foreground 적용
 *          [2018.10.24 17:19] 8.0도 startService, QOS 2 적용
 *          [2018.10.29 13:08] MQTT 로직 보완
 *          [2018.10.31 17:28] MQTT 포그라운드 팝업 버그 수정
 *          [2018.11.01 12:06] MQTT 8.0일 경우 별도로 종료하는 스레드 추가
 *          [2018.11.19 18:25] MQTT RCV_TIME 이슈 대응
 *          [2018.11.26 18:16] MQTT Starter 적용
 *          [2018.11.28 13:05] startActivity 버그 수정
 *          [2018.11.28 16:26] startActivity 버그 수정
 *          [2018.11:30 12:10] 재시도 회수 조정, 화면 활성화 개선, MQTT ALARM 플래그 추가
 *          [2018.12.10 09:33] mqttTask 일정 시간 지난뒤 종료, PopupActivity Flag 변경
 *          [2018.12.10 19:24] MQTTBinder 적용
 *          [2018.12.11 10:00] 버그 수정
 *          [2018.12.24 09:11] MQTTStarter onResume에서 실행하도록 변경
 *          [2019.01.03 18:12] MQTT 최적화, GCM InstanceId 적용
 *          [2019.01.07 08:45] MQTTBinder 제거, Android 6.0 이상에서 Receiver에서 푸시 받도록 변경
 *          [2019.01.07 11:30] MQTT 동작 로직 수정
 *          [2019.01.30 16:20] FCM 적용
 *          [2019.04.08 16:20] NO_TOKEN 대응
 *          [2019.04.16 17:38] FCM 토큰 발급 로직 개선, 벨소리 음량 조절 제거, setColor 버전 분기 적용
 *          [2019.04.29 11:37] DeviceCert 퍼미션 체크부분 제거
 *          [2019.07.17 17:25] GZIP java.lang.OutOfMemoryError 대응, DateUtil kk->HH
 *          [2019.08.28 14:00] 통신 Volley -> OkHttp로 교체, 이미지 Volley -> Glide로 교체
 *          [2019.09.06 11:16] AndroidX 대응, 알림 채널 재생성 로직 제거, Badge 동작 제거
 *          [2019.09.23 11:19] APIManager 싱글톤으로 동작하도록 변경, TLS Support 코드가 AsyncTask 써서 OOM 발생시키길래 제거
 *          [2019.09.23 15:37] 업체 요청으로 Android X 대응 안한 버전 만듬
 *          [2019.12.09 15:37] OkHttpClient OOM 이슈 대응, makePendingIntent 대응
 *          [2020.01.16 15:27] 업체 요청으로 통신 모듈 Volley로 롤백
 *          [2020.03.05 17:14] OOM 상황 발생을 대비하기 위한 Czip 메모리 할당 전환, 이미지 로더 Volley로 롤백
 *          [2020.04.27 14:11] Czip WeakReference 적용, OutOfMemory 예외처리, encrypt outStream 버퍼 적용
 *          [2020.06.08 14:19] CursorWindowAllocationException 이슈 대응
 */
public class PMS implements IPMSConsts, Serializable {

    private static final long serialVersionUID = 1L;
    private static PMS instancePms;
    private static PMSPopup instancePmsPopup;
    private Context mContext;
    private final PMSDB mDB;
    private final Prefs mPrefs;

    private OnReceivePushListener onReceivePushListener;

    /**
     * constructor
     *
     * @param context
     */
    private PMS(Context context) {
        this.mDB = PMSDB.getInstance(context);
        this.mPrefs = new Prefs(context);
        CLog.i("Version:" + PMS_VERSION + ",UpdateDate:"+PMS_VERSION_UPDATE_DATE);
        initOption(context);
    }

    /**
     * initialize option
     */
    private void initOption(Context context) {
        if (StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG))) {
            mPrefs.putString(PREF_NOTI_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_MSG_FLAG))) {
            mPrefs.putString(PREF_MSG_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
            mPrefs.putString(PREF_RING_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
            mPrefs.putString(PREF_VIBE_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_ALERT_FLAG))) {
            mPrefs.putString(PREF_ALERT_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_MAX_USER_MSG_ID))) {
            mPrefs.putString(PREF_MAX_USER_MSG_ID, "-1");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG))) {
            mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, "Y");
        }
        if (StringUtil.isEmpty(PMSUtil.getServerUrl(context))) {
            PMSUtil.setServerUrl(context, API_SERVER_URL);
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_PUSH_POPUP_ACTIVITY))) {
            mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, DEFAULT_PUSH_POPUP_ACTIVITY);
        }
        if (mPrefs.getInt(PREF_PUSH_POPUP_SHOWING_TIME) < 1) {
            mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, 10000);
        }
    }

    /**
     * getInstance
     *
     * @param context
     * @param projectId
     * @return
     */
    public static PMS getInstance(Context context, String projectId) {
        PMSUtil.setGCMProjectId(context, projectId);
        return getInstance(context);
    }

    /**
     * getInstance
     *
     * @param context
     * @return
     */
    public static PMS getInstance(final Context context) {
        CLog.d("getInstance:projectId=" + PMSUtil.getGCMProjectId(context));
        if (instancePms == null) {
            instancePms = new PMS(context);
//            if (PhoneState.isAvailablePush()) {
//                PushReceiver.gcmRegister(context, PMSUtil.getGCMProjectId(context));
//				PushReceiver.requestADIDInBackground(context);
//            }
        }
        instancePms.setmContext(context);

        String token = PMSUtil.getGCMToken(context);
        if (TextUtils.isEmpty(token) || NO_TOKEN.equals(token))
        {
            // get token
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>()
            {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult)
                {
                    String newToken = instanceIdResult.getToken();
                    PMSUtil.setGCMToken(context, newToken);
                    CLog.d("FirebaseInstanceId "+newToken);
                }
            });

        }
        return instancePms;
    }

    public static PMSPopup getPopUpInstance() {
        return instancePmsPopup;
    }

    @Deprecated
    public static void stopApi()
    {
//        QueueManager.getInstance().stop();
    }

    /**
     * start mqtt service
     */
    public void startMQTTService(Context context) {
        if (FLAG_Y.equals(PMSUtil.getMQTTFlag(context)))
        {
            Intent serviceIntent = new Intent(context, RestartReceiver.class);
            serviceIntent.setAction(ACTION_START);
            context.sendBroadcast(serviceIntent);
        }
    }

    public void stopMQTTService(Context context) {
        context.stopService(new Intent(context, MQTTService.class));
    }

    /**
     * clear
     *
     * @return
     */
    public static boolean clear() {
        try {
            PMSUtil.setDeviceCertStatus(instancePms.mContext, DEVICECERT_PENDING);
            instancePms.unregisterReceiver();
            instancePms = null;

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void setmContext(Context context) {
        this.mContext = context;
    }

    @Deprecated
    private void unregisterReceiver() {
//        Badge.getInstance(mContext).unregisterReceiver();
    }

    // /**
    // * push token 세팅
    // * @param pushToken
    // */
    // public void setPushToken(String pushToken) {
    // CLog.i(TAG + "setPushToken");
    // CLog.d(TAG + "setPushToken:pushToken=" + pushToken);
    // PMSUtil.setGCMToken(mContext, pushToken);
    // }

    public void setUseBigText(Boolean state) {
        mPrefs.putString(PREF_USE_BIGTEXT, state ? "Y" : "N");
    }

    public void setPopupSetting(Boolean state, String title) {
        instancePmsPopup = PMSPopup.getInstance(mContext, mContext.getPackageName(), state, title);
    }

    /**
     * set ServerUrl
     *
     * @param serverUrl
     */
    public void setServerUrl(String serverUrl) {
        CLog.i("setServerUrl");
        CLog.d("setServerUrl:serverUrl=" + serverUrl);
        PMSUtil.setServerUrl(mContext, serverUrl);
    }

    /**
     * cust id 세팅
     *
     * @param custId
     */
    public void setCustId(String custId) {
        CLog.i("setCustId");
        CLog.d("setCustId:custId=" + custId);
        PMSUtil.setCustId(mContext, custId);
    }

    /**
     * get cust id
     *
     * @return
     */
    public String getCustId() {
        CLog.i("getCustId");
        return PMSUtil.getCustId(mContext);
    }

    public void setIsPopupActivity(Boolean ispopup) {
        PMSUtil.setPopupActivity(mContext, ispopup);
    }

    public void setNotiOrPopup(Boolean isnotiorpopup) {
        PMSUtil.setNotiOrPopup(mContext, isnotiorpopup);
    }

    /**
     * set inbox activity
     *
     * @param inboxActivity
     */
    public void setInboxActivity(String inboxActivity) {
        mPrefs.putString(PREF_INBOX_ACTIVITY, inboxActivity);
    }

    /**
     * set push popup activity
     *
     * @param classPath
     */
    public void setPushPopupActivity(String classPath) {
        mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, classPath);
    }

    /**
     * set pushPopup showing time
     *
     * @param pushPopupShowingTime
     */
    public void setPushPopupShowingTime(int pushPopupShowingTime) {
        mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, pushPopupShowingTime);
    }

    /**
     * get msg flag
     *
     * @return
     */
    public String getMsgFlag() {
        return mPrefs.getString(PREF_MSG_FLAG);
    }

    /**
     * get noti flag
     *
     * @return
     */
    public String getNotiFlag() {
        return mPrefs.getString(PREF_NOTI_FLAG);
    }

    /**
     * get order flag
     *
     * @return
     */
    public String getOrderFlag() {
        return mPrefs.getString(PREF_ORDER_FLAG);
    }

    /**
     * set noti icon
     *
     * @param resId
     */
    public void setNotiIcon(int resId) {
        mPrefs.putInt(PREF_NOTI_ICON, resId);
    }

    /**
     * set large noti icon
     *
     * @param resId
     */
    public void setLargeNotiIcon(int resId) {
        mPrefs.putInt(PREF_LARGE_NOTI_ICON, resId);
    }

    /**
     * set noti cound
     *
     * @param resId
     */
    public void setNotiSound(int resId) {
        mPrefs.putInt(PREF_NOTI_SOUND, resId);
    }

    public void setNotiBackColor(String colorHexString)
    {
        mPrefs.putString(IPMSConsts.PREF_NOTI_BACK_COLOR, colorHexString);
    }
    public String getNotiBackColor()
    {
        return mPrefs.getString(IPMSConsts.PREF_NOTI_BACK_COLOR);
    }

    /**
     * set noti receiver
     *
     * @param intentAction
     */
    public void setNotiReceiver(String intentAction) {
        mPrefs.putString(PREF_NOTI_RECEIVER, intentAction);
    }

    public void setNotiReceiverClass(String notiReceiverClass) {
        mPrefs.putString(PREF_NOTI_RECEIVER_CLASS, notiReceiverClass);
    }

    public void setPushReceiverClass(String pushReceiver) {
        mPrefs.putString(PREF_PUSH_RECEIVER_CLASS, pushReceiver);
    }

    public void setPushSilentReceiverClass(String pushSilentReceiverClass)
    {
        mPrefs.putString(PREF_PUSH_SILENT_RECEIVER_CLASS, pushSilentReceiverClass);
    }
    
    /**
     * set ring mode
     *
     * @param isRingMode
     */
    public void setRingMode(boolean isRingMode) {
        mPrefs.putString(PREF_RING_FLAG, isRingMode ? "Y" : "N");
    }

    /**
     * set vibe mode
     *
     * @param isVibeMode
     */
    public void setVibeMode(boolean isVibeMode) {
        mPrefs.putString(PREF_VIBE_FLAG, isVibeMode ? "Y" : "N");
    }

    /**
     * set popup noti
     *
     * @param isShowPopup
     */
    public void setPopupNoti(boolean isShowPopup) {
        mPrefs.putString(PREF_ALERT_FLAG, isShowPopup ? "Y" : "N");
    }

    /**
     * set screen wakeup
     *
     * @param isScreenWakeup
     */
    public void setScreenWakeup(boolean isScreenWakeup) {
        mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, isScreenWakeup ? "Y" : "N");
    }

    public OnReceivePushListener getOnReceivePushListener() {
        return onReceivePushListener;
    }

    public void setOnReceivePushListener(OnReceivePushListener onReceivePushListener) {
        this.onReceivePushListener = onReceivePushListener;
    }

    /**
     * set debugTag
     *
     * @param tagName
     */
    public void setDebugTAG(String tagName) {
        CLog.setTagName(tagName);
    }

    public void setLogToFile(boolean debugMode) {
        CLog.setDebugFile(debugMode);
    }
    
    /**
     * set debug mode
     *
     * @param debugMode
     */
    public void setDebugMode(boolean debugMode) {
        CLog.setDebugMode(debugMode);
    }

    @Deprecated
    public void bindBadge(Context c, int id) {
//        Badge.getInstance(c).addBadge(c, id);
    }

    @Deprecated
    public void bindBadge(TextView badge)
    {

//        Badge.getInstance(badge.getContext()).addBadge(badge);
    }

    /**
     * show Message Box
     *
     * @param c
     */
    public void showMsgBox(Context c) {
        showMsgBox(c, null);
    }

    public void showMsgBox(Context c, Bundle extras) {
        CLog.i("showMsgBox");
        if (PhoneState.isAvailablePush()) {
            try {
                Class<?> inboxActivity = Class.forName(mPrefs.getString(PREF_INBOX_ACTIVITY));

                Intent i = new Intent(mContext, inboxActivity);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                if (extras != null) {
                    i.putExtras(extras);
                }
                c.startActivity(i);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            // Intent i = new Intent(INBOX_ACTIVITY);
            // i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // if (extras != null) {
            // i.putExtras(extras);
            // }
            // c.startActivity(i);
        }
    }

    @Deprecated
    public void closeMsgBox(Context c) {
//        Intent i = new Intent(c, Badge.class).setAction(RECEIVER_CLOSE_INBOX);
//        c.sendBroadcast(i);
    }

	/*
     * ===================================================== [start] database =====================================================
	 */

    /**
     * select MsgGrp list
     *
     * @return
     */
    public Cursor selectMsgGrpList() {
        return mDB.selectMsgGrpList();
    }

    /**
     * select MsgGrp
     *
     * @param msgCode
     * @return
     */
    public MsgGrp selectMsGrp(String msgCode) {
        return mDB.selectMsgGrp(msgCode);
    }

    /**
     * select new msg Cnt
     *
     * @return
     */
    public int selectNewMsgCnt() {
        return mDB.selectNewMsgCnt();
    }

    /**
     * select Msg List
     *
     * @param page
     * @param row
     * @return
     */
    public Cursor selectMsgList(int page, int row) {
        return mDB.selectMsgList(page, row);
    }

    /**
     * select Msg List
     *
     * @param msgCode
     * @return
     */
    public Cursor selectMsgList(String msgCode) {
        return mDB.selectMsgList(msgCode);
    }

    /**
     * select Msg
     *
     * @param msgId
     * @return
     */
    public Msg selectMsgWhereMsgId(String msgId) {
        return mDB.selectMsgWhereMsgId(msgId);
    }

    /**
     * select Msg
     *
     * @param userMsgID
     * @return
     */
    public Msg selectMsgWhereUserMsgId(String userMsgID) {
        return mDB.selectMsgWhereUserMsgId(userMsgID);
    }

    /**
     * select query
     *
     * @param sql
     * @return
     */
    public Cursor selectQuery(String sql) {
        return mDB.selectQuery(sql);
    }

    /**
     * update MsgGrp
     *
     * @param msgCode
     * @param values
     * @return
     */
    public long updateMsgGrp(String msgCode, ContentValues values) {
        return mDB.updateMsgGrp(msgCode, values);
    }

    /**
     * update read msg
     *
     * @param msgGrpCd
     * @param firstUserMsgId
     * @param lastUserMsgId
     * @return
     */
    public long updateReadMsg(String msgGrpCd, String firstUserMsgId, String lastUserMsgId) {
        return mDB.updateReadMsg(msgGrpCd, firstUserMsgId, lastUserMsgId);
    }

    /**
     * update read msg where userMsgId
     *
     * @param userMsgId
     * @return
     */
    public long updateReadMsgWhereUserMsgId(String userMsgId) {
        return mDB.updateReadMsgWhereUserMsgId(userMsgId);
    }

    /**
     * update read msg where msgId
     *
     * @param msgId
     * @return
     */
    public long updateReadMsgWhereMsgId(String msgId) {
        return mDB.updateReadMsgWhereMsgId(msgId);
    }

    /**
     * delete Msg
     *
     * @param userMsgId
     * @return
     */
    public long deleteUserMsgId(String userMsgId) {
        return mDB.deleteUserMsgId(userMsgId);
    }

    /**
     * delete Msg
     *
     * @param MsgId
     * @return
     */
    public long deleteMsgId(String MsgId) {
        return mDB.deleteMsgId(MsgId);
    }

    /**
     * delete MsgGrp
     *
     * @param msgCode
     * @return
     */
    public long deleteMsgGrp(String msgCode) {
        return mDB.deleteMsgGrp(msgCode);
    }

    /**
     * delete expire Msg
     *
     * @return
     */
    public long deleteExpireMsg() {
        return mDB.deleteExpireMsg();
    }

    /**
     * delete empty MsgGrp
     *
     * @return
     */
    public void deleteEmptyMsgGrp() {
        mDB.deleteEmptyMsgGrp();
    }

    /**
     * delete all
     */
    public void deleteAll() {
        mDB.deleteAll();
    }

    public SSLSocketFactory getSSLSocketFactoryForLollipop()
    {
        SSLSocketFactory sslSocketFactory = null;
        final TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException
                    {
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new java.security.cert.X509Certificate[]{};
                    }
                }
        };
        try
        {
            sslSocketFactory = new TlsSocketFactory(trustAllCerts, new java.security.SecureRandom());
        }
        catch (KeyManagementException e)
        {
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return sslSocketFactory;
    }
    public X509TrustManager getX509TrustManagerForTrustAllOfCertifications()
    {
        final TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException
                    {
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new java.security.cert.X509Certificate[]{};
                    }
                }
        };
        return (X509TrustManager)trustAllCerts[0];
    }

	/*
	 * ===================================================== [end] database =====================================================
	 */

    public void setNotificationStackable(boolean isStackable){
        PMSUtil.setNotificationToStackable(mContext, isStackable);
    }
}
